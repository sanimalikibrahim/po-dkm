<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Setting extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model(['SettingAppModel', 'SettingSmtpModel', 'SettingAccountModel']);
  }

  public function index()
  {
    redirect(base_url('setting/application'));
  }

  public function application()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('setting'),
      'card_title' => 'Pengaturan › Applikasi'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('application', $data, TRUE);
    $this->template->render();
  }

  public function smtp()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('setting'),
      'card_title' => 'Pengaturan › SMTP'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('smtp', $data, TRUE);
    $this->template->render();
  }

  public function account()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('setting'),
      'card_title' => 'Pengaturan › Account',
      'data' => $this->SettingAccountModel->getDetail(['id' => $this->session->userdata('user')['id']])
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('account', $data, TRUE);
    $this->template->render();
  }

  public function ajax_save_application()
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SettingAppModel->rules());

    if ($this->form_validation->run() === true) {
      echo json_encode($this->SettingAppModel->update());
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_save_smtp()
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SettingSmtpModel->rules());

    if ($this->form_validation->run() === true) {
      echo json_encode($this->SettingSmtpModel->update());
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_save_account()
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SettingAccountModel->rules());

    if ($this->form_validation->run() === true) {
      echo json_encode($this->SettingAccountModel->update());
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }
}
