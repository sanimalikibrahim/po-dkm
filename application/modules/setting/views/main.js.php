<script type="text/javascript">
  $(document).ready(function() {

    var _form_application = "form-setting-application";
    var _form_smtp = "form-setting-smtp";
    var _form_account = "form-setting-account";

    // Handle data submit Application
    $("#" + _form_application + " .page-action-save-application").on("click", function(e) {
      e.preventDefault();
      tinyMCE.triggerSave();

      var form = $("#" + _form_application)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('setting/ajax_save_application/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            notify(response.data, "success");
            window.location = "<?php echo base_url('setting/application') ?>";
          } else {
            notify(response.data, "danger");
          };
        }
      });
      return false;
    });

    // Handle data submit SMTP
    $("#" + _form_smtp + " .page-action-save-smtp").on("click", function(e) {
      e.preventDefault();
      tinyMCE.triggerSave();

      var form = $("#" + _form_smtp)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('setting/ajax_save_smtp/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            notify(response.data, "success");
            window.location = "<?php echo base_url('setting/smtp') ?>";
          } else {
            notify(response.data, "danger");
          };
        }
      });
      return false;
    });

    // Handle data submit Account
    $("#" + _form_account + " .page-action-save-account").on("click", function(e) {
      e.preventDefault();
      tinyMCE.triggerSave();

      var form = $("#" + _form_account)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('setting/ajax_save_account/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            notify(response.data, "success");
            window.location = "<?php echo base_url('logout') ?>";
          } else {
            notify(response.data, "danger");
          };
        }
      });
      return false;
    });

  });
</script>