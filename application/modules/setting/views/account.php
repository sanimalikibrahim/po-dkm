<section id="setting">
    <div class="card">
        <div class="card-body">

            <form id="form-setting-account" enctype="multipart/form-data">

                <div class="row">
                    <div class="col-xs-10 col-md-10">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                        <div class="clear-card"></div>
                    </div>
                </div>
                <div class="clear-card"></div>

                <div class="row">
                    <div class="col-xs-10 col-md-4">
                        <div class="form-group">
                            <label>NIK</label>
                            <input type="text" name="nik" class="form-control mask-number user-nik" placeholder="NIK" readonly value="<?php echo $data->nik ?>" />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10 col-md-4">
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control user-password" placeholder="(Optional) Type new password for change" />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10 col-md-4">
                        <div class="form-group">
                            <label required>Email</label>
                            <input type="email" name="email" class="form-control user-email" placeholder="Email" required value="<?php echo $data->email ?>" />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10 col-md-4">
                        <div class="form-group">
                            <label required>Nama Lengkap</label>
                            <input type="text" name="nama_lengkap" class="form-control user-nama_lengkap" placeholder="Nama Lengkap" required value="<?php echo $data->nama_lengkap ?>"/>
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10 col-md-4">
                        <div class="form-group">
                            <label>Jabatan</label>
                            <input type="text" name="jabatan" class="form-control user-jabatan" placeholder="Jabatan" readonly value="<?php echo $data->jabatan ?>"/>
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10 col-md-4">
                        <div class="form-group">
                            <label>Cabang</label>
                            <input type="text" name="cabang" class="form-control user-cabang" placeholder="Cabang" readonly value="<?php echo $data->cabang ?>"/>
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10 col-md-4">
                        <div class="form-group">
                            <label>Departemen</label>
                            <input type="text" name="departemen" class="form-control user-departemen" placeholder="Departemen" readonly value="<?php echo $data->departemen ?>"/>
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>

                <small class="form-text text-muted">
                    Fields with red stars (<label required></label>) are required.
                </small>

                <div class="row" style="margin-top: 2rem;">
                    <div class="col-xs-10 col-md-2">
                        <button class="btn btn--raised btn-primary btn--icon-text btn-block page-action-save-account spinner-action-button">
                            Save Changes
                            <div class="spinner-action"></div>
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>
</section>