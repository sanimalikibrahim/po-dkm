<div class="table-responsive">
  <table class="table table-bordered table-condensed">
    <thead>
      <tr>
        <th style="text-align: center;" width="50">No</th>
        <th style="text-align: center;" width="300">User PIC</th>
        <th style="text-align: center;" width="200">Role</th>
        <th style="text-align: center;">Description</th>
        <th style="text-align: center;" width="180">Date</th>
      </tr>
    </thead>
    <tbody>
      <?php if (count($data_approval_history) > 0) : ?>
        <?php $no = 1; ?>
        <?php foreach ($data_approval_history as $key => $item) : ?>
          <tr <?php echo ($item->action == 'Reject') ? 'style="background: #ff6b68; color: #f9f9f9;"' : '' ?>>
            <td valign="top" align="center"><?php echo $no++ ?></td>
            <td valign="top"><?php echo $item->nama_lengkap ?></td>
            <td valign="top"><?php echo $item->role ?></td>
            <td valign="top"><?php echo $item->description ?></td>
            <td valign="top" align="center"><?php echo $item->created_at ?></td>
          </tr>
        <?php endforeach; ?>
      <?php else : ?>
        <tr>
          <td colspan="5" style="padding: 15px;">
            No data available in table
          </td>
        </tr>
      <?php endif; ?>
    </tbody>
  </table>
</div>