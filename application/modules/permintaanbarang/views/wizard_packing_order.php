<?php require_once("main.css.php") ?>
<?php include_once('form_input_order_item.php') ?>

<?php $isAdmin = (strtolower($this->session->userdata('user')['role']) == 'administrasi') ? true : false ?>
<?php $flagLabel = ($isAdmin && isset($data_permintaan_barang->status) && in_array((int) $data_permintaan_barang->status, [0])) ? 'EDIT' : 'VIEW' ?>
<?php $isReadonly = ($isAdmin && isset($data_permintaan_barang->status) && !in_array((int) $data_permintaan_barang->status, [0])) ? 'disabled="true"' : '' ?>
<?php $isReadonly_opacity = ($isAdmin && isset($data_permintaan_barang->status) && !in_array((int) $data_permintaan_barang->status, [0])) ? '' : 'opacity: 1;' ?>

<section id="permintaanbarang">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute;">
                        <?php echo (isset($data_permintaan_barang->id)) ? $flagLabel : 'NEW' ?>
                    </span>
                    <div style="margin-left: 60px; margin-top: 5px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <div class="row">
                <?php require_once("tab_wizard.php") ?>
                <div class="col">
                    <?php if (isset($data_permintaan_barang->status)) : ?>
                        <div class="alert alert-secondary">
                            <i class="zmdi zmdi-info-outline"></i> &nbsp;
                            <?php echo (isset($data_permintaan_barang->status)) ? $controller->get_status($data_permintaan_barang->status) : '' ?>
                        </div>
                        <div class="clear-sm"></div>
                    <?php endif; ?>

                    <form id="form-permintaanbarang" enctype="multipart/form-data">

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label required>Dari</label>
                                    <input type="text" name="dari" class="form-control po-dari" placeholder="Dari" value="<?php echo (isset($data_permintaan_barang->dari)) ? $data_permintaan_barang->dari : '' ?>" <?php echo $isReadonly ?> />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label required>Penerima</label>
                                    <input type="text" name="penerima" class="form-control po-penerima" placeholder="Penerima" value="<?php echo (isset($data_permintaan_barang->penerima)) ? $data_permintaan_barang->penerima : '' ?>" <?php echo $isReadonly ?> />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label required>Tanggal</label>
                                    <input type="text" name="tanggal" class="form-control flatpickr-date po-tanggal" placeholder="Tanggal" value="<?php echo (isset($data_permintaan_barang->tanggal)) ? $data_permintaan_barang->tanggal : '' ?>" style="<?php echo $isReadonly_opacity ?>" <?php echo $isReadonly ?> />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>

                        <small class="form-text text-muted">
                            Fields with red stars (<label required></label>) are required.
                        </small>

                        <div class="row">
                            <div class="col">
                                <div class="buttons-container">
                                    <div class="row">
                                        <div class="col text-right">
                                            <?php if ($isAdmin && (!isset($data_permintaan_barang->status) || (in_array((int) $data_permintaan_barang->status, [0])))) : ?>
                                                <button class="btn btn--raised btn-primary btn--icon-text btn-custom permintaanbarang-action-save spinner-action-button">
                                                    Save & Next
                                                    <i class="zmdi zmdi-long-arrow-right"></i>
                                                </button>
                                            <?php else : ?>
                                                <a href="<?php echo base_url('permintaanbarang/wizard/1/' . $data_id) ?>" class="btn btn--raised btn-primary btn--icon-text btn-custom spinner-action-button">
                                                    Next
                                                    <i class="zmdi zmdi-long-arrow-right"></i>
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</section>