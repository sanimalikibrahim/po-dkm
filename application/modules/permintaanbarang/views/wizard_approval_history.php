<?php require_once('main.css.php') ?>

<?php $isAdmin = (strtolower($this->session->userdata('user')['role']) == 'administrasi') ? true : false ?>
<?php $flagLabel = ($isAdmin && isset($data_permintaan_barang->status) && in_array((int) $data_permintaan_barang->status, [0])) ? 'EDIT' : 'VIEW' ?>
<?php $isReadonly = ($isAdmin && isset($data_permintaan_barang->status) && !in_array((int) $data_permintaan_barang->status, [0])) ? 'disabled="true"' : '' ?>
<?php $isReadonly_opacity = ($isAdmin && isset($data_permintaan_barang->status) && !in_array((int) $data_permintaan_barang->status, [0])) ? '' : 'opacity: 1;' ?>

<section id="permintaanbarang">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute;">
                        <?php echo (isset($data_permintaan_barang->id)) ? $flagLabel : 'NEW' ?>
                    </span>
                    <div style="margin-left: 60px; margin-top: 5px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <div class="row">
                <?php require_once("tab_wizard.php") ?>
                <div class="col">
                    <?php if (isset($data_permintaan_barang->status)) : ?>
                        <div class="alert alert-secondary">
                            <i class="zmdi zmdi-info-outline"></i> &nbsp;
                            <?php echo (isset($data_permintaan_barang->status)) ? $controller->get_status($data_permintaan_barang->status) : '' ?>
                        </div>
                        <div class="clear-sm"></div>
                    <?php endif; ?>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row">
                                <?php if (!$is_mobile) : ?>
                                    <div class="col-2 col-2-custom"><b>Dari</b></div>
                                    <div class="col">: <?php echo (isset($data_permintaan_barang->dari)) ? $data_permintaan_barang->dari : '' ?></div>
                                <?php else : ?>
                                    <div class="col-12"><b>Dari :</b></div>
                                    <div class="col"><?php echo (isset($data_permintaan_barang->dari)) ? $data_permintaan_barang->dari : '' ?></div>
                                <?php endif; ?>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <?php if (!$is_mobile) : ?>
                                    <div class="col-2 col-2-custom"><b>Tanggal</b></div>
                                    <div class="col">: <?php echo $controller->format_date((isset($data_permintaan_barang->tanggal)) ? $data_permintaan_barang->tanggal : '') ?></div>
                                <?php else : ?>
                                    <div class="col-12"><b>Tanggal :</b></div>
                                    <div class="col"><?php echo $controller->format_date((isset($data_permintaan_barang->tanggal)) ? $data_permintaan_barang->tanggal : '') ?></div>
                                <?php endif; ?>
                            </div>
                        </li>
                    </ul>
                    <div class="clear"></div>

                    <div class="approval-history">
                        <?php
                        if (!$is_mobile) {
                            include_once('approval_history.php');
                        } else {
                            include_once('approval_history_mobile.php');
                        };
                        ?>
                    </div>
                    <div class="buttons-container">
                        <div class="row">
                            <div class="col-auto">
                                <a href="<?php echo base_url('permintaanbarang/wizard/2/' . $data_id) ?>" class="btn btn--raised btn-light btn--icon-text page-action-back spinner-action-button <?php echo (!$is_mobile) ? 'btn-custom' : '' ?>">
                                    <?php echo (!$is_mobile) ? 'Back' : '<i class="zmdi zmdi-arrow-left"></i>' ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>