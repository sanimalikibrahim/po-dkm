<div class="modal fade" id="modal-form-penawaran" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-xl modal-dialog-centered" style="<?php echo ($is_mobile) ? 'max-width: 98%' : '' ?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Penawaran</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-penawaran">
          <div class="approval-preview">Please wait...</div>
          <hr />
          <div class="approval-form">
            <input type="hidden" name="packing_order_id" class="approval-packing_order_id" />
            <div class="form-group">
              <label>Note</label>
              <textarea name="note" class="form-control textarea-autosize text-counter approval-note" rows="1" data-max-length="300" placeholder="(optional)" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"></textarea>
              <i class="form-group__bar"></i>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="btn-group" role="group" aria-label="Basic example">
          <button class="btn btn-success btn--icon-text penawaran-action-send">
            <i class="zmdi zmdi-check-circle"></i> Send To Supervisor
          </button>
        </div>
        <button type="button" class="btn btn-light btn--icon-text approval-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>