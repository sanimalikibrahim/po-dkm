<?php require_once('main.css.php') ?>
<?php include_once('form_penawaran.php') ?>
<?php include_once('form_penawaran_approval.php') ?>

<section id="permintaanbarang">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <!-- <div class="table-responsive"> -->
            <table id="table-penawaran" class="table table-bordered display nowrap" style="width: 100%;">
                <thead class="thead-default">
                    <tr>
                        <th width="100">No</th>
                        <th>Dari</th>
                        <th>Penerima</th>
                        <th>Tanggal</th>
                        <th>Jumlah Barang</th>
                        <th>Created At</th>
                        <th width="120" style="text-align: center;">Action</th>
                    </tr>
                </thead>
            </table>
            <!-- </div> -->
        </div>
    </div>
</section>