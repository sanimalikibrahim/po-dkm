<div class="table-responsive">
  <table id="table-order_item" class="table table-bordered">
    <thead class="thead-default">
      <tr>
        <th class="text-center" rowspan="2" width="80">No</th>
        <th class="text-center" rowspan="2">Uraian</th>
        <th class="text-center" rowspan="2">Qty</th>
        <th class="text-center" colspan="2">Harga Beli</th>
        <th class="text-center" colspan="2">Harga Jual</th>
        <th class="text-center" rowspan="2">Selisih</th>
        <th class="text-center" rowspan="2">%</th>
      </tr>
      <tr>
        <th class="text-center">Satuan</th>
        <th class="text-center">Total</th>
        <th class="text-center">Satuan</th>
        <th class="text-center">Total</th>
      </tr>
    </thead>
    <tbody>
      <?php if (count($data_order_item) > 0) : ?>
        <?php $no = 1; ?>
        <?php foreach ($data_order_item as $index => $item) : ?>
          <tr>
            <td><?= $no++ ?></td>
            <td><?= $item->nama_barang ?></td>
            <td>
              <?= number_format($item->quantity) . ' ' . $item->quantity_unit ?>
              <input type="hidden" name="permintaan_barang_item[<?= $item->id ?>][quantity]" class="penawaran-<?= $item->id ?>-quantity" value="<?= $item->quantity ?>" readonly />
            </td>
            <td>
              <input type="text" name="permintaan_barang_item[<?= $item->id ?>][harga_beli]" class="form-control p-0 mask-money penawaran-<?= $item->id ?>-harga_beli" placeholder="0" onkeyup="penawaranCalculateHargaBeli('<?= $item->id ?>')" value="0" style="border-bottom: 1px solid #b2b9bf;" />
            </td>
            <td>
              <input type="text" name="permintaan_barang_item[<?= $item->id ?>][harga_beli_total]" class="form-control p-0 mask-money penawaran-<?= $item->id ?>-harga_beli_total" placeholder="-" style="opacity: 1;" value="0" readonly />
            </td>
            <td>
              <div style="border-bottom: 1px solid #eceff1; padding: .375rem 0; color: #495057;">
                <?= number_format($item->unit_price) ?>
              </div>
            </td>
            <td>
              <div style="border-bottom: 1px solid #eceff1; padding: .375rem 0; color: #495057;">
                <?= number_format($item->amount) ?>
              </div>
              <input type="hidden" name="permintaan_barang_item[<?= $item->id ?>][amount]" class="penawaran-<?= $item->id ?>-amount" value="<?= $item->amount ?>" readonly />
            </td>
            <td>
              <input type="text" name="permintaan_barang_item[<?= $item->id ?>][selisih]" class="form-control p-0 mask-money penawaran-<?= $item->id ?>-selisih" placeholder="-" style="opacity: 1;" value="0" readonly />
            </td>
            <td>
              <input type="text" name="permintaan_barang_item[<?= $item->id ?>][selisih_persen]" class="form-control p-0 penawaran-<?= $item->id ?>-selisih_persen" placeholder="-" style="opacity: 1;" value="0" readonly />
            </td>
          </tr>
        <?php endforeach ?>
      <?php else : ?>
        <tr>
          <td colspan="7">Tidak ditemukan data</td>
        </tr>
      <?php endif ?>
    </tbody>
  </table>
</div>