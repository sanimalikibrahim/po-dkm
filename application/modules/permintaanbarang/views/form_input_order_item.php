<style type="text/css">
  .list-detail {
    background: #fdfdfd;
    padding: 8px 30px;
    margin-left: -30px;
    margin-right: -30px;
    border-top: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
  }
</style>

<div class="modal fade" id="modal-form-order_item" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Item</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-order_item">

          <input type="hidden" name="permintaan_barang_id" class="order_item-packing_order_id" />
          <input type="hidden" name="supplier_barang_id" class="order_item-supplier_barang_id" />
          <input type="hidden" name="kode_barang" class="order_item-kode_barang" />
          <input type="hidden" name="nama_barang" class="order_item-nama_barang" />
          <input type="hidden" name="quantity_unit" class="order_item-quantity_unit" />
          <input type="hidden" name="unit_price" class="order_item-unit_price" />
          <input type="hidden" name="amount" class="order_item-amount" />

          <div class="row">
            <div class="col">
              <div class="form-group">
                <label required>Item</label>
                <div class="select">
                  <select name="supplier_barang" class="form-control select2-desc order_item-supplier_barang" data-placeholder="Select a item" search-placeholder="Search by code part or departemen">
                    <?php
                    if (count($data_supplier_barang) > 0) {
                      echo '<option value="">Select a item</option>';
                      foreach ($data_supplier_barang as $key => $item) {
                        $data_desc  = "<div><strong style='color: #32c787;'>" . $item->code_part . "</strong></div>";
                        $data_desc .= "<div>" . $item->nama_part . "</div>";
                        echo '<option value="' . $item->id . '" data-desc="' . $data_desc . '">' . $item->departemen . ' | ' . $item->code_part . '</option>';
                      };
                    };
                    ?>
                  </select>
                  <i class="form-group__bar"></i>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="list-detail">
              <ul class="list-group list-group-flush">
                <li class="list-group-item" style="padding-left: 0px; padding-right: 0px;">
                  <div class="row">
                    <div class="col-3">Department</div>
                    <div class="col-auto" style="padding: 0px;">:</div>
                    <div class="col order_item-data-departemen">-</div>
                  </div>
                </li>
                <li class="list-group-item" style="padding-left: 0px; padding-right: 0px;">
                  <div class="row">
                    <div class="col-3">Part Code</div>
                    <div class="col-auto" style="padding: 0px;">:</div>
                    <div class="col order_item-data-code_part">-</div>
                  </div>
                </li>
                <li class="list-group-item" style="padding-left: 0px; padding-right: 0px;">
                  <div class="row">
                    <div class="col-3">Part Name</div>
                    <div class="col-auto" style="padding: 0px;">:</div>
                    <div class="col order_item-data-nama_part">-</div>
                  </div>
                </li>
                <li class="list-group-item" style="padding-left: 0px; padding-right: 0px;">
                  <div class="row">
                    <div class="col-3">Unit Price</div>
                    <div class="col-auto" style="padding: 0px;">:</div>
                    <div class="col"><label class="order_item-data-harga">-</label></div>
                  </div>
                </li>
                <li class="list-group-item" style="padding-left: 0px; padding-right: 0px;">
                  <div class="row">
                    <div class="col-3">Quantity Unit</div>
                    <div class="col-auto" style="padding: 0px;">:</div>
                    <div class="col order_item-data-satuan">-</div>
                  </div>
                </li>
              </ul>
            </div>
          </div>

          <div class="form-group">
            <label required>Quantity</label>
            <input type="number" name="quantity" class="form-control mask-number order_item-quantity" placeholder="Quantity">
            <i class="form-group__bar"></i>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text order_item-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text order_item-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>