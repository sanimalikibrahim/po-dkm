<?php require_once('main.css.php') ?>

<div class="preview-po">
  <div class="preview-header">
    <table class="table-header">
      <tr>
        <td style="width: 80px;">
          <img src="<?php echo base_url('themes/_public/img/logo/dkm-black.png') ?>" class="logo" />
        </td>
        <td style="padding-left: 20px; padding-right: 20px;">
          <div class="kop-surat">
            <h5>PT. DHARMA KARYATAMA MULIA</h5>
            <span>Jl. Raya Bogor Km 29, Gandaria, Ps. Rebo - Jakarta 13710, Indonesia</span> <br />
            <span>Phone : +62 21 8719964-5, Fax : +62 21 8727018</span> <br />
            <span>Email : pt_dkm@dkm.co.id, Web : www.dkm.co.id</span>
          </div>
        </td>
        <td style="width: 80px; text-align: right;">
          <!-- <img src="<?php echo base_url('themes/_public/img/logo/sics.png') ?>" class="logo" /> -->
        </td>
      </tr>
    </table>
    <hr class="double-line">
  </div>
  <div class="preview-body">
    <div style="text-align: center; margin-bottom: 20px;">
      <h5>PERMINTAAN / PEMESANAN BARANG</h5>
    </div>
    <table style="width: 100%; margin-bottom: 20px;">
      <tr>
        <td valign="top">
          <table class="table-body">
            <tr>
              <td valign="top" class="th">Kepada</td>
              <td valign="top" class="td">Departemen Purchasing</td>
            </tr>
            <tr>
              <td valign="top" class="th">Dari</td>
              <td valign="top" class="td"><?php echo (isset($data_permintaan_barang->dari)) ? $data_permintaan_barang->dari : '' ?></td>
            </tr>
            <tr>
              <td valign="top" class="th">Tanggal</td>
              <td valign="top" class="td"><?php echo (isset($data_permintaan_barang->tanggal)) ? $data_permintaan_barang->tanggal : '' ?></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table class="table-order-item">
      <thead>
        <tr>
          <th width="50">NO</th>
          <th>NAMA BARANG</th>
          <th>SATUAN</th>
          <th>JUMLAH</th>
        </tr>
      </thead>
      <tbody>
        <?php if (isset($data_order_item) && count($data_order_item) > 0) : ?>
          <?php $no = 1 ?>
          <?php foreach ($data_order_item as $key => $item) : ?>
            <tr>
              <td valign="top" align="center"><?php echo $no++ ?></td>
              <td valign="top">
                <?php echo $item->nama_barang ?>
              </td>
              <td valign="top" align="center" width="180">
                <?php echo $item->quantity_unit ?>
              </td>
              <td valign="top" align="center" width="180">
                <?php echo $item->quantity ?>
              </td>
            </tr>
          <?php endforeach; ?>
        <?php else : ?>
          <tr>
            <td colspan="5" style="padding: 15px;">
              No data available in table
            </td>
          </tr>
        <?php endif; ?>
      </tbody>
    </table>
    <table class="table-order-item mb-0">
      <thead>
        <tr>
          <th>Diajukan</th>
          <th colspan="2">Disetujui</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td valign="center" align="center" class="ttd">
            <?php echo (isset($data_ttd->administrasi)) ? $data_ttd->administrasi : '-' ?>
          </td>
          <td valign="center" align="center" class="ttd">
            <?php echo (isset($data_ttd->g_affair)) ? $data_ttd->g_affair : '-' ?>
          </td>
          <td valign="center" align="center" class="ttd">
            <?php echo (isset($data_ttd->admin_gudang)) ? $data_ttd->admin_gudang : '-' ?>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <th align="center">Administrasi</th>
          <th align="center">G. Affair</th>
          <th align="center">Admin Gudang</th>
        </tr>
      </tfoot>
    </table>
    <p style="margin: 0; margin-top: 20px; font-size: 11px;">
      Lembar ke-1 : Yang Menyerahkan
    </p>
  </div>
</div>