<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "permintaanbarang";
    var _table = "table-order_item";
    var _table_po = "table-permintaanbarang";
    var _table_approvalresult = "table-approvalresult";
    var _table_approved = "table-approved";
    var _table_approval = "table-approval";
    var _table_penawaran = "table-penawaran";
    var _modal = "modal-form-order_item";
    var _modal_approval = "modal-form-approval";
    var _modal_penawaran = "modal-form-penawaran";
    var _modal_penawaran_approval = "modal-form-penawaran-approval";
    var _form_po = "form-permintaanbarang";
    var _form = "form-order_item";
    var _form_approval = "form-approval";
    var _form_penawaran = "form-penawaran";
    var _form_penawaran_approval = "form-penawaran-approval";
    var _dataId = $("#data-id").val();
    var _headStatus = "<?= isset($permintaan_barang_status) ? $permintaan_barang_status : null ?>";

    // Permintaan Barang
    // Initialize DataTables : Draft
    if ($("#" + _table_po)[0]) {
      var table_packingorder = $("#" + _table_po).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('permintaanbarang/ajax_get_draft/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "dari"
          },
          {
            data: "penerima"
          },
          {
            data: "tanggal"
          },
          {
            data: "item_count",
            render: function(data, type, row, meta) {
              return meta.settings.fnFormatNumber(data);
            }
          },
          {
            data: "created_at"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              return '<div class="action">' +
                '<a href="<?php echo base_url('permintaanbarang/wizard/0/') ?>' + row.id + '"><i class="zmdi zmdi-edit"></i></a>' +
                '<a href="javascript:;" class="action-delete"><i class="zmdi zmdi-delete"></i></a>' +
                '</div>';
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 2]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_po).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data delete: Permintaan Barang
    $("#" + _table_po).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_packingorder.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('permintaanbarang/ajax_delete_draft/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_po).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Initialize DataTables : Approval Result
    if ($("#" + _table_approvalresult)[0]) {
      var table_aprovalresult = $("#" + _table_approvalresult).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('permintaanbarang/ajax_get_approvalresult/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "dari"
          },
          {
            data: "penerima"
          },
          {
            data: "tanggal"
          },
          {
            data: "item_count",
            render: function(data, type, row, meta) {
              return meta.settings.fnFormatNumber(data);
            }
          },
          {
            data: "status",
            render: function(data, type, row, meta) {
              color = (jQuery.inArray(data, ["3", "4", "10", "11", "12"]) !== -1) ? 'style="color: red;"' : '';
              status = '<span ' + color + '>' + getStatus(data) + '</span>';
              return status;
            }
          },
          {
            data: "created_at"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              var buttons = '';
              var role = '<?= strtolower($this->session->userdata('user')['role']) ?>';

              buttons += '<div class="action">';
              buttons += '<a href="<?php echo base_url('permintaanbarang/wizard/3/') ?>' + row.id + '"><i class="zmdi zmdi-eye"></i></a>';
              // if (jQuery.inArray(row.status, ["3", "4", "10", "11", "12"]) !== -1 && role == "administrasi") {
              //   buttons += '<a href="<?php echo base_url('permintaanbarang/wizard/0/') ?>' + row.id + '"><i class="zmdi zmdi-edit"></i></a>';
              // };
              buttons += '</div>';

              return buttons;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7]
        }, {
          className: 'tablet',
          targets: [0, 1, 2]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_approvalresult).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Initialize DataTables : Approved
    if ($("#" + _table_approved)[0]) {
      var table_approved = $("#" + _table_approved).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('permintaanbarang/ajax_get_approved/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "tanggal"
          },
          {
            data: "nomor"
          },
          {
            data: "supplier_nama"
          },
          {
            data: "supplier_nama_kontak"
          },
          {
            data: "sub_total",
            render: function(data, type, row, meta) {
              return meta.settings.fnFormatNumber(data);
            }
          },
          {
            data: "ppn",
            render: function(data, type, row, meta) {
              return meta.settings.fnFormatNumber(data);
            }
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              return '<div class="action">' +
                '<a href="<?php echo base_url('permintaanbarang/wizard/2/') ?>' + row.id + '" title="Detail"><i class="zmdi zmdi-eye"></i></a>' +
                '<a href="<?php echo base_url('permintaanbarang/download/') ?>' + row.id + '" title="Download" target="_blank"><i class="zmdi zmdi-download"></i></a>' +
                '</div>';
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'mobile',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 30,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        // sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        sDom: '<"dataTables__top"fB>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Permintaan Barang",
          exportOptions: {
            columns: [0, 1, 2, 3, 4, 5, 6]
          }
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '<div class="dropdown actions__item"><i data-toggle="dropdown" class="zmdi zmdi-download" /><ul class="dropdown-menu dropdown-menu-right"><a href="" class="dropdown-item" data-table-action="excel">Excel (.xlsx)</a></ul></div>' +
            '</div>'
          );
        },
        order: [
          [1, "asc"],
          [3, "asc"],
        ],
        rowsGroup: [3],
        rowGroup: {
          startRender: null,
          endRender: function(rows, group) {
            var amount_total = rows
              .data()
              .reduce(function(a, b) {
                return a + b.sub_total * 1;
              }, 0);
            amount_total = $.fn.dataTable.render.number(',', '.', 0, '').display(amount_total);

            var ppn_total = rows
              .data()
              .reduce(function(a, b) {
                return a + b.ppn * 1;
              }, 0);
            ppn_total = $.fn.dataTable.render.number(',', '.', 0, '').display(ppn_total);

            return $('<tr/>')
              .append('<td colspan="5" class="row-group">Sub total for ' + group + '</td>')
              .append('<td class="row-group">' + amount_total + '</td>')
              .append('<td colspan="2" class="row-group">' + ppn_total + '</td>');
          },
          dataSrc: "supplier_nama"
        }
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if (
          ("excel" === b && $(this).closest(".dataTables_wrapper").find(".buttons-excel").trigger("click"),
            "reload" === b)
        ) {
          $("#" + _table_approved).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Submit filter
    $("#" + _section + " .page-action-filter").on("click", function(e) {
      var month = $("#" + _section + " .filter-month").val();
      var year = $("#" + _section + " .filter-year").val();

      table_approved.ajax.url("<?php echo base_url('permintaanbarang/ajax_get_approved/') ?>" + month + "/" + year);
      table_approved.clear().draw();
    });

    // Initialize DataTables : Approval
    if ($("#" + _table_approval)[0]) {
      var table_aproval = $("#" + _table_approval).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('permintaanbarang/ajax_get_approval/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "dari"
          },
          {
            data: "penerima"
          },
          {
            data: "tanggal"
          },
          {
            data: "item_count",
            render: function(data, type, row, meta) {
              return meta.settings.fnFormatNumber(data);
            }
          },
          {
            data: "created_at"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              return '<div class="action">' +
                '<a href="javascript:;" class="action-approval btn btn-sm btn-success" data-toggle="modal" data-target="#' + _modal_approval + '"><i class="zmdi zmdi-eye"></i> Detail</a>' +
                '</div>';
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 2]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_approval).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Initialize DataTables : Penawaran
    if ($("#" + _table_penawaran)[0]) {
      var table_penawaran = $("#" + _table_penawaran).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('permintaanbarang/ajax_get_penawaran/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "dari"
          },
          {
            data: "penerima"
          },
          {
            data: "tanggal"
          },
          {
            data: "item_count",
            render: function(data, type, row, meta) {
              return meta.settings.fnFormatNumber(data);
            }
          },
          {
            data: "created_at"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              var role = "<?= strtolower($this->session->userdata('user')['role']) ?>";
              var buttons = "";

              if (role === 'admin billing') {
                buttons += `<a href="javascript:;" class="action-penawaran btn btn-sm btn-success" data-toggle="modal" data-target="#${_modal_penawaran}"><i class="zmdi zmdi-plus-circle"></i> Create</a>`;
              } else {
                buttons += `<a href="javascript:;" class="action-penawaran-approval btn btn-sm btn-success" data-toggle="modal" data-target="#${_modal_penawaran_approval}"><i class="zmdi zmdi-eye"></i> Detail</a>`;
              };

              return `<div class="action" style="flex-direction: column;">${buttons}</div>`;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 2]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_penawaran).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle approval modal
    $("#" + _table_approval).on("click", "a.action-approval", function(e) {
      e.preventDefault();
      resetFormApproval();
      var temp = table_aproval.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form_approval + " .approval-packing_order_id").val(_key);
      $("#" + _modal_approval + " .approval-preview").html("Please wait...");

      $.ajax({
        type: "get",
        url: "<?php echo base_url('permintaanbarang/ajax_get_preview/') ?>" + _key,
        success: function(response) {
          $(".approval-preview").html(response);
        }
      });
    });

    // Handle penawaran modal
    $("#" + _table_penawaran).on("click", "a.action-penawaran", function(e) {
      e.preventDefault();
      resetFormPenawaran();
      var temp = table_penawaran.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form_penawaran + " .approval-packing_order_id").val(_key);
      $("#" + _modal_penawaran + " .approval-preview").html("Please wait...");

      $.ajax({
        type: "get",
        url: "<?php echo base_url('permintaanbarang/ajax_get_preview_penawaran/') ?>" + _key,
        success: function(response) {
          $(".approval-preview").html(response);
          $('.mask-money').unmask().mask('#,##0', {
            reverse: true
          });
        }
      });
    });

    // Handle penawaran approval modal
    $("#" + _table_penawaran).on("click", "a.action-penawaran-approval", function(e) {
      e.preventDefault();
      resetFormPenawaran();
      var temp = table_penawaran.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form_penawaran_approval + " .approval-packing_order_id").val(_key);
      $("#" + _modal_penawaran_approval + " .approval-preview").html("Please wait...");

      $.ajax({
        type: "get",
        url: "<?php echo base_url('permintaanbarang/ajax_get_preview_penawaran/') ?>" + _key + "/?readonly=true",
        success: function(response) {
          $(".approval-preview").html(response);
        }
      });
    });

    // Handle submit penawaran
    $(".penawaran-action-send").on("click", function(e) {
      e.preventDefault();
      swal({
        title: "Confirm",
        text: "Anda akan mengajukan penawaran ke Supervisor, lanjutkan?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "post",
            url: "<?php echo base_url('permintaanbarang/ajax_send_penawaran/') ?>" + _key,
            data: $("#" + _form_penawaran).serialize(),
            success: function(response) {
              var response = JSON.parse(response);
              if (response.status === true) {
                $("#" + _modal_penawaran).modal("hide");
                $("#" + _table_penawaran).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            },
            error: function(response) {
              notify(`(${response.status}) ${response.statusText}`, "danger");
            }
          });
        };
      });
    });

    // Handle submit penawaran approval
    $(".approval-action-penawaran").on("click", function(e) {
      e.preventDefault();
      swal({
        title: "Confirm",
        text: "Anda akan mengajukan penawaran ke Billing, lanjutkan?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "post",
            url: "<?php echo base_url('permintaanbarang/ajax_save_penawaran/1/') ?>" + _key,
            data: $("#" + _form_approval).serialize(),
            success: function(response) {
              var response = JSON.parse(response);
              if (response.status === true) {
                $("#" + _modal_approval).modal("hide");
                $("#" + _table_approval).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            },
            error: function(response) {
              notify(`(${response.status}) ${response.statusText}`, "danger");
            }
          });
        };
      });
    });

    // Handle submit approve penawaran
    $(".approval-action-approve-penawaran").on("click", function(e) {
      e.preventDefault();
      var action = "Approve";
      var note = $("#form-penawaran-approval .approval-note");

      swal({
        title: "Confirm",
        text: "Are you sure to approve?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "post",
            url: "<?php echo base_url('permintaanbarang/ajax_save_penawaran/1/') ?>" + _key,
            data: $("#" + _form_penawaran_approval).serialize(),
            success: function(response) {
              var response = JSON.parse(response);
              if (response.status === true) {
                $("#" + _modal_penawaran_approval).modal("hide");
                $("#" + _table_penawaran).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle submit reject penawaran
    $(".approval-action-reject-penawaran").on("click", function(e) {
      var action = "Reject";
      var note = $("#form-penawaran-approval .approval-note");

      if (note.val().trim() != "") {
        swal({
          title: "Confirm",
          text: "Are you sure to reject?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false
        }).then((result) => {
          if (result.value) {
            $.ajax({
              type: "post",
              url: "<?php echo base_url('permintaanbarang/ajax_save_penawaran/0/') ?>" + _key,
              data: $("#" + _form_penawaran_approval).serialize(),
              success: function(response) {
                var response = JSON.parse(response);
                if (response.status === true) {
                  $("#" + _modal_penawaran_approval).modal("hide");
                  $("#" + _table_penawaran).DataTable().ajax.reload(null, false);
                  notify(response.data, "success");
                } else {
                  notify(response.data, "danger");
                };
              }
            });
          };
        });
      } else {
        note.focus();
        notify("Please fill the note field.", "danger");
      };
    });

    // Handle submit approve
    $(".approval-action-approve").on("click", function(e) {
      e.preventDefault();
      var action = "Approve";
      var note = $(".approval-note");

      swal({
        title: "Confirm",
        text: "Are you sure to approve?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "post",
            url: "<?php echo base_url('permintaanbarang/ajax_save_approval/1/') ?>" + _key,
            data: $("#" + _form_approval).serialize(),
            success: function(response) {
              var response = JSON.parse(response);
              if (response.status === true) {
                $("#" + _modal_approval).modal("hide");
                $("#" + _table_approval).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle submit reject
    $(".approval-action-reject").on("click", function(e) {
      var action = "Reject";
      var note = $(".approval-note");

      if (note.val().trim() != "") {
        swal({
          title: "Confirm",
          text: "Are you sure to reject?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false
        }).then((result) => {
          if (result.value) {
            $.ajax({
              type: "post",
              url: "<?php echo base_url('permintaanbarang/ajax_save_approval/0/') ?>" + _key,
              data: $("#" + _form_approval).serialize(),
              success: function(response) {
                var response = JSON.parse(response);
                if (response.status === true) {
                  $("#" + _modal_approval).modal("hide");
                  $("#" + _table_approval).DataTable().ajax.reload(null, false);
                  notify(response.data, "success");
                } else {
                  notify(response.data, "danger");
                };
              }
            });
          };
        });
      } else {
        note.focus();
        notify("Please fill the note field.", "danger");
      };
    });

    // Handle submit po
    $("#" + _section + " #" + _form_po).on("submit", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('permintaanbarang/ajax_save_po/') ?>" + _dataId,
        data: new FormData(this),
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            window.location = response.data;
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });
    // END ## Permintaan Barang

    // Order Item
    // Initialize DataTables
    if ($("#" + _table)[0]) {
      var table_orderitem = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('permintaanbarang/ajax_get_orderitem/') ?>" + _dataId,
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nama_barang"
          },
          {
            data: "quantity",
            render: function(data, type, row, meta) {
              return meta.settings.fnFormatNumber(data);
            }
          },
          {
            data: "quantity_unit",
          },
          {
            data: "created_at"
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              var visibilty = (jQuery.inArray(_headStatus, ["0"]) !== -1) ? true : false;
              table_orderitem.column(5).visible(visibilty);

              if (visibilty) {
                return '<div class="action">' +
                  // '<a href="javascript:;" class="action-edit" data-toggle="modal" data-target="#' + _modal + '"><i class="zmdi zmdi-edit"></i></a>' +
                  '<a href="javascript:;" class="action-delete"><i class="zmdi zmdi-delete"></i></a>' +
                  '</div>';
              } else {
                return "";
              };
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 3]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle submit
    $("#" + _modal + " .order_item-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('permintaanbarang/ajax_save_orderitem/') ?>" + _key,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle push to g. affair
    $("#" + _section).on("click", "button.page-action-push", function(e) {
      e.preventDefault();
      swal({
        title: "Send To G. Affair",
        text: "Are you sure the data to be sent is correct?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "post",
            url: "<?php echo base_url('permintaanbarang/ajax_set_status/') ?>" + _dataId,
            success: function(response) {
              var response = JSON.parse(response);
              if (response.status === true) {
                window.location = response.data;
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data add
    $("#" + _section).on("click", "button.permintaanbarang-action-add-order_item", function(e) {
      e.preventDefault();
      resetForm();

      $("#" + _form + " .order_item-packing_order_id").val(_dataId);
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();
      var temp = table_orderitem.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form + " .order_item-packing_order_id").val(_dataId);
      $("#" + _form + " .order_item-description").val(temp.description.replace(/<br\s*[\/]?>/gi, "\r\n"));
      $("#" + _form + " .order_item-note").val(temp.note.replace(/<br\s*[\/]?>/gi, "\r\n"));
      $("#" + _form + " .order_item-quantity").val(temp.quantity).trigger("input");
      $("#" + _form + " .order_item-quantity_unit").val(temp.quantity_unit).trigger("input");
      $("#" + _form + " .order_item-unit_price").val(temp.unit_price).trigger("input");
      $("#" + _form + " .order_item-amount").val(temp.amount).trigger("input");

      // Handle textarea height
      setTimeout(function() {
        $("#" + _form + " .order_item-description").height($("#" + _form + " .order_item-description")[0].scrollHeight);
        $("#" + _form + " .order_item-note").height($("#" + _form + " .order_item-note")[0].scrollHeight);
      }, 500);
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_orderitem.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('permintaanbarang/ajax_delete_orderitem/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle penawaran input harga beli
    penawaranCalculateHargaBeli = (id) => {
      var input_quantity = $(`.penawaran-${id}-quantity`).val().replace(/[^\d]/g, "");;
      var input_hargaJualTotal = $(`.penawaran-${id}-amount`).val().replace(/[^\d]/g, "");;
      var input_hargaBeli = $(`.penawaran-${id}-harga_beli`).val().replace(/[^\d]/g, "");;
      var input_hargaBeliTotal = $(`.penawaran-${id}-harga_beli_total`);
      var input_selisih = $(`.penawaran-${id}-selisih`);
      var input_selisihPersen = $(`.penawaran-${id}-selisih_persen`);

      var hargaBeliTotal = input_hargaBeli * input_quantity;
      var selisih = input_hargaJualTotal - hargaBeliTotal;
      var selisihPersen = (selisih / input_hargaJualTotal) * 100;

      input_hargaBeliTotal.val(hargaBeliTotal).unmask().mask('#,##0', {
        reverse: true
      });
      input_selisih.val(selisih).unmask().mask('#,##0', {
        reverse: true
      });
      input_selisihPersen.val(parseFloat(selisihPersen).toFixed(0)).unmask().mask('#,##0', {
        reverse: true
      });
    };

    // Handle form reset : Order Item
    resetForm = () => {
      _key = "";
      $("#" + _form).trigger("reset");
      $("#" + _form + " .order_item-description").css("height", "31px");
      $("#" + _form + " .order_item-note").css("height", "31px");


      // Clear detail
      $("#" + _form + " .order_item-data-departemen").html("-");
      $("#" + _form + " .order_item-data-code_part").html("-");
      $("#" + _form + " .order_item-data-nama_part").html("-");
      $("#" + _form + " .order_item-data-harga").html("-").unmask();
      $("#" + _form + " .order_item-data-satuan").html("-");

      // Clear input
      $("#" + _form + " .order_item-supplier_barang").val("").trigger('change');
      $("#" + _form + " .order_item-description").val("").trigger("input");
      $("#" + _form + " .order_item-quantity_unit").val("PCS").trigger("input");
      $("#" + _form + " .order_item-unit_price").val("").trigger("input");
      $("#" + _form + " .order_item-amount").val("").trigger("input");
    };

    // Handle form reset : Approval
    resetFormApproval = () => {
      _key = "";
      $("#" + _form_approval).trigger("reset");
      $("#" + _form_approval + " .approval-note").css("height", "31px");
    };

    // Handle form reset : Penawaran
    resetFormPenawaran = () => {
      _key = "";
      $("#" + _form_penawaran).trigger("reset");
      $("#" + _form_penawaran + " .approval-note").css("height", "31px");
    };

    // Handle calculating
    calculateAmount = () => {
      var quantity = $("#" + _form + " .order_item-quantity").val().replace(/[^\d]/g, "");
      quantity = (quantity != "") ? quantity : 0;

      var unit_price = $("#" + _form + " .order_item-unit_price").val().replace(/[^\d]/g, "");
      unit_price = (unit_price != "") ? unit_price : 0;

      var amount = $("#" + _form + " .order_item-amount");
      var result = unit_price * quantity;

      amount.val(result).trigger("input");
    };

    // Perform calculating: Unit Price
    $("#" + _form + " .order_item-quantity").on("keyup click", function() {
      calculateAmount();
    });

    // Handle item selection
    $("#" + _form + " .order_item-supplier_barang").on("change", function() {
      var id = $(this).val();

      // Clear detail
      $("#" + _form + " .order_item-data-departemen").html("-");
      $("#" + _form + " .order_item-data-code_part").html("-");
      $("#" + _form + " .order_item-data-nama_part").html("-");
      $("#" + _form + " .order_item-data-harga").html("-").unmask();
      $("#" + _form + " .order_item-data-satuan").html("-");

      // Clear input
      $("#" + _form + " .order_item-permintaan_barang_id").val(_dataId);
      $("#" + _form + " .order_item-supplier_barang_id").val("").trigger("input");
      $("#" + _form + " .order_item-kode_barang").val("").trigger("input");
      $("#" + _form + " .order_item-nama_barang").val("").trigger("input");
      $("#" + _form + " .order_item-quantity").val("").trigger("input");
      $("#" + _form + " .order_item-quantity_unit").val("PCS").trigger("input");
      $("#" + _form + " .order_item-unit_price").val("").trigger("input");
      $("#" + _form + " .order_item-amount").val("").trigger("input");

      if (id != "") {
        $.ajax({
          type: "get",
          url: "<?php echo base_url('permintaanbarang/ajax_get_supplier_barang/') ?>" + id,
          dataType: "json",
          success: function(response) {
            if (response != null) {
              // Set detail
              $("#" + _form + " .order_item-data-departemen").html(response.departemen);
              $("#" + _form + " .order_item-data-code_part").html(response.code_part);
              $("#" + _form + " .order_item-data-nama_part").html(response.nama_part);
              $("#" + _form + " .order_item-data-harga").html(response.harga).mask('#,##0', {
                reverse: true
              });
              $("#" + _form + " .order_item-data-satuan").html(response.satuan);

              // Set input
              $("#" + _form + " .order_item-permintaan_barang_id").val(_dataId);
              $("#" + _form + " .order_item-supplier_barang_id").val(response.id).trigger("input").focus();
              $("#" + _form + " .order_item-nama_barang").val(response.nama_part).trigger("input");
              $("#" + _form + " .order_item-kode_barang").val(response.code_part).trigger("input");
              $("#" + _form + " .order_item-quantity_unit").val(response.satuan).trigger("input");
              $("#" + _form + " .order_item-quantity").val(1).trigger("input");
              $("#" + _form + " .order_item-unit_price").val(response.harga).trigger("input");
              $("#" + _form + " .order_item-amount").val(response.harga * 1).trigger("input");
              $("#" + _form + " .order_item-supplier_barang").focus();
            } else {
              notify("Item is not found.", "danger");
            };
          }
        });
      };
    });
    // END ## Order Item
  });

  function getStatus(status) {
    switch (status.toString()) {
      case '0':
        return 'Draft';
        break;
      case '1':
        return 'Menunggu Persetujuan G. Affair';
        break;
      case '2':
        return 'Diproses Admin Gudang';
        break;
      case '3':
        return 'Ditolak G. Affair';
        break;
      case '4':
        return 'Ditolak Admin Gudang';
        break;
      case '5':
        return 'Selesai';
        break;
      case '6':
        return 'Menunggu Penawaran Admin Billing';
        break;
      case '7':
        return 'Menunggu Persetujuan SPV';
        break;
      case '8':
        return 'Menunggu Persetujuan Manager';
        break;
      case '9':
        return 'Menunggu Persetujuan GM';
        break;
      case '10':
        return 'Penawaran Ditolak SPV';
        break;
      case '11':
        return 'Penawaran Ditolak Manager';
        break;
      case '12':
        return 'Penawaran Ditolak GM';
        break;
      case '13':
        return 'Penawaran Selesai';
        break;
      default:
        return 'Undefined';
        break;
    };
  };

  function randomString(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;

    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    };

    return result;
  };
</script>