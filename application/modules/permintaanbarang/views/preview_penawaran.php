<?php require_once('main.css.php') ?>

<div class="preview-po">
  <div class="preview-header">
    <table class="table-header">
      <tr>
        <td style="width: 80px;">
          <img src="<?php echo base_url('themes/_public/img/logo/dkm-black.png') ?>" class="logo" />
        </td>
        <td style="padding-left: 20px; padding-right: 20px;">
          <div class="kop-surat">
            <h5>PT. DHARMA KARYATAMA MULIA</h5>
            <span>Jl. Raya Bogor Km 29, Gandaria, Ps. Rebo - Jakarta 13710, Indonesia</span> <br />
            <span>Phone : +62 21 8719964-5, Fax : +62 21 8727018</span> <br />
            <span>Email : pt_dkm@dkm.co.id, Web : www.dkm.co.id</span>
          </div>
        </td>
        <td style="width: 80px; text-align: right;">
          <!-- <img src="<?php echo base_url('themes/_public/img/logo/sics.png') ?>" class="logo" /> -->
        </td>
      </tr>
    </table>
    <hr class="double-line">
  </div>
  <div class="preview-body">
    <div style="text-align: center; margin-bottom: 20px;">
      <h5>PERMINTAAN / PENAWARAN BARANG</h5>
    </div>
    <table style="width: 100%; margin-bottom: 20px;">
      <tr>
        <td valign="top">
          <table class="table-body">
            <tr>
              <td valign="top" class="th">Dari</td>
              <td valign="top" class="td"><?php echo (isset($data_permintaan_barang->dari)) ? $data_permintaan_barang->dari : '' ?></td>
            </tr>
            <tr>
              <td valign="top" class="th">Tanggal</td>
              <td valign="top" class="td"><?php echo (isset($data_permintaan_barang->tanggal_penawaran)) ? date('Y-m-d', strtotime($data_permintaan_barang->tanggal_penawaran)) : '' ?></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <div class="table-responsive">
      <table class="table-order-item">
        <thead class="thead-default">
          <tr>
            <th class="text-center" rowspan="2" width="80">No</th>
            <th class="text-center" rowspan="2">Uraian</th>
            <th class="text-center" rowspan="2">Qty</th>
            <th class="text-center" colspan="2">Harga Beli</th>
            <th class="text-center" colspan="2">Harga Jual</th>
            <th class="text-center" rowspan="2">Selisih</th>
            <th class="text-center" rowspan="2">%</th>
          </tr>
          <tr>
            <th class="text-center">Satuan</th>
            <th class="text-center">Total</th>
            <th class="text-center">Satuan</th>
            <th class="text-center">Total</th>
          </tr>
        </thead>
        <tbody>
          <?php if (count($data_order_item) > 0) : ?>
            <?php $no = 1; ?>
            <?php foreach ($data_order_item as $index => $item) : ?>
              <tr>
                <td><?= $no++ ?></td>
                <td><?= $item->nama_barang ?></td>
                <td>
                  <?= number_format($item->quantity) . ' ' . $item->quantity_unit ?>
                </td>
                <td>
                  <?= number_format($item->harga_beli) ?>
                </td>
                <td>
                  <?= number_format($item->harga_beli_total) ?>
                </td>
                <td>
                  <?= number_format($item->unit_price) ?>
                </td>
                <td>
                  <?= number_format($item->amount) ?>
                </td>
                <td>
                  <?= number_format($item->selisih) ?>
                </td>
                <td>
                  <?= number_format($item->selisih_persen) ?>
                </td>
              </tr>
            <?php endforeach ?>
          <?php else : ?>
            <tr>
              <td colspan="7">Tidak ditemukan data</td>
            </tr>
          <?php endif ?>
        </tbody>
      </table>
    </div>
    <div class="table-responsive">
      <table class="table-order-item mb-0">
        <thead>
          <tr>
            <th>Diajukan</th>
            <th colspan="3">Disetujui</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td valign="center" align="center" class="ttd">
              <?php echo (isset($data_ttd->admin_billing)) ? $data_ttd->admin_billing : '-' ?>
            </td>
            <td valign="center" align="center" class="ttd">
              <?php echo (isset($data_ttd->supervisor)) ? $data_ttd->supervisor : '-' ?>
            </td>
            <td valign="center" align="center" class="ttd">
              <?php echo (isset($data_ttd->manager)) ? $data_ttd->manager : '-' ?>
            </td>
            <td valign="center" align="center" class="ttd">
              <?php echo (isset($data_ttd->g_manager)) ? $data_ttd->g_manager : '-' ?>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <th align="center">Admin Billing</th>
            <th align="center">Supervisor</th>
            <th align="center">Manager</th>
            <th align="center">G. Manager</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>