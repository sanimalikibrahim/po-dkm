<div class="modal fade" id="modal-form-approval" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-xl modal-dialog-centered" style="<?php echo ($is_mobile) ? 'max-width: 98%' : '' ?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Persetujuan</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">

        <div class="approval-preview">Please wait...</div>
        <hr />

        <div class="approval-form">
          <form id="form-approval">
            <input type="hidden" name="packing_order_id" class="approval-packing_order_id" />
            <div class="form-group">
              <label>Note</label>
              <textarea name="note" class="form-control textarea-autosize text-counter approval-note" rows="1" data-max-length="300" placeholder="(optional, but required for reject)" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"></textarea>
              <i class="form-group__bar"></i>
            </div>
          </form>
        </div>

      </div>
      <div class="modal-footer">
        <?php if (strtolower($this->session->userdata('user')['role']) == 'admin gudang') : ?>
          <button class="btn btn-primary btn--icon-text approval-action-penawaran" title="Ajukan Penawaran Ke Billing">
            <i class="zmdi zmdi-replay"></i> Ajukan Penawaran
          </button>
        <?php endif ?>
        <div class="btn-group" role="group" aria-label="Basic example">
          <button class="btn btn-success btn--icon-text approval-action-approve">
            <i class="zmdi zmdi-check-circle"></i> Approve
          </button>
          <button class="btn btn-danger btn--icon-text approval-action-reject">
            <i class="zmdi zmdi-close-circle"></i> Reject
          </button>
        </div>
        <button type="button" class="btn btn-light btn--icon-text approval-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>