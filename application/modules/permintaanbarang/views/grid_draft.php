<?php require_once('main.css.php') ?>

<section id="permintaanbarang">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action">
                <div class="buttons">
                    <a href="<?php echo base_url('permintaanbarang/wizard/') ?>" class="btn btn--raised btn-primary btn--icon-text">
                        <i class="zmdi zmdi-plus"></i> Add New
                    </a>
                </div>
            </div>

            <div class="table-responsive">
                <table id="table-permintaanbarang" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Dari</th>
                            <th>Penerima</th>
                            <th>Tanggal</th>
                            <th>Jumlah Barang</th>
                            <th>Created At</th>
                            <th width="100">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>