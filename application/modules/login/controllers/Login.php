<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Login extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model(['LoginModel']);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('login'),
      'page_title' => 'Login | ' . $this->app()->app_name
    );
    $this->load->view('index', $data);
  }

  public function ajax_submit()
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->LoginModel->rules());

    if ($this->form_validation->run() === true) {
      $nik = $this->input->post('username');
      $pass = $this->input->post('password');
      $temp = $this->LoginModel->getDetail(['nik' => $nik, 'password' => md5($pass)]);

      if (count($temp) == 1) {
        $user = array(
          'id' => $temp->id,
          'nik' => $temp->nik,
          'username' => $temp->username,
          'nama_lengkap' => $temp->nama_lengkap,
          'jabatan' => $temp->jabatan,
          'cabang' => $temp->cabang,
          'departemen' => $temp->departemen,
          'role' => $temp->role,
          'is_login' => true
        );
        $this->session->set_userdata('user', $user);

        echo json_encode(array('status' => true, 'data' => 'Successfully login.'));
      } else {
        echo json_encode(array('status' => false, 'data' => 'NIK or Password is wrong, try again.'));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }
}
