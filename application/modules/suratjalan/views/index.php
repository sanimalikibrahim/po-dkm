<?php require_once('main.css.php') ?>
<?php include_once('form_surat_jalan.php') ?>
<?php include_once('view_surat_jalan.php') ?>

<section id="suratjalan">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-responsive">
                <table id="table-list" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Dari</th>
                            <th>Penerima</th>
                            <th>Tanggal</th>
                            <th>Jumlah Barang</th>
                            <th>Created At</th>
                            <th>Status Pengiriman</th>
                            <th width="180" style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>