<div class="modal fade" id="modal-suratjalan_view" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-xl modal-dialog-centered" style="<?php echo ($is_mobile) ? 'max-width: 98%' : '' ?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Surat Jalan</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <div class="suratjalan-preview"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn--icon-text suratjalan-action-print">
          <i class="zmdi zmdi-print"></i> Print
        </button>
        <button type="button" class="btn btn-light btn--icon-text" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>