<script type="text/javascript">
  $(document).ready(function() {
    var _key = "";
    var _section = "suratjalan";
    var _table_list = "table-list";
    var _table_item_list = "table-suratjalan_item";
    var _modal_form_surat_jalan = "modal-suratjalan";
    var _modal_view_surat_jalan = "modal-suratjalan_view";
    var _form_surat_jalan = "form-suratjalan";

    // Initialize DataTables : List
    if ($("#" + _table_list)[0]) {
      var table_aprovalresult = $("#" + _table_list).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('suratjalan/ajax_get_list/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "dari"
          },
          {
            data: "penerima"
          },
          {
            data: "tanggal"
          },
          {
            data: "item_count",
            render: function(data, type, row, meta) {
              return `${meta.settings.fnFormatNumber(data)} Item (Quantity: ${row.total_info.total_permintaan})`;
            }
          },
          {
            data: "created_at"
          },
          {
            data: "status",
            render: function(data, type, row, meta) {
              var totalInfo = row.total_info;
              var suratJalan = row.surat_jalan;
              var htmlDom = "";
              var htmlDomEmpty = "<i>Belum ada surat jalan</i>";

              if (totalInfo !== null) {
                if (totalInfo.selisih > 0) {
                  htmlDom += `<span class="badge badge-danger mb-2"><i class="zmdi zmdi-info"></i> Belum Dikirim Semua (-${totalInfo.selisih})</span> `;
                };
              };

              if (suratJalan.length > 0) {
                suratJalan.map((item, index) => {
                  var buttonStatus = "";

                  switch (item.status_pengiriman) {
                    case "0":
                      borderColor = "border-danger";
                      textColor = "text-danger";
                      break;
                    case "1":
                      borderColor = "border-success";
                      textColor = "";
                      break;
                    case "2":
                      borderColor = "border-primary";
                      textColor = "";
                      break;
                    default:
                      borderColor = "border-secondary";
                      textColor = "";
                      break;
                  };

                  if (item.status_pengiriman === "0") {
                    buttonStatus = `<a href="javascript:;" class="btn btn-danger btn-sm suratjalan-action-status" data-status="1" data-surat-jalan="${item.id}">Kirim?</a>`;
                  } else if (item.status_pengiriman === "1") {
                    buttonStatus = `<a href="javascript:;" class="btn btn-success btn-sm suratjalan-action-status" data-status="2" data-surat-jalan="${item.id}">Complete?</a>`;
                  };

                  htmlDom += `
                    <div class="p-2 border ${borderColor} ${textColor} rounded mb-2">
                      <b>Nomor</b>: ${item.nomor} <hr class="mt-1 mb-1"/>
                      <b>Tanggal</b>: ${item.tanggal} <hr class="mt-1 mb-1"/>
                      <b>Dikirim</b>: ${item.total_dikirim} <hr class="mt-1 mb-1"/>
                      <b>Status</b>: ${getStatus(item.status_pengiriman)} <hr class="mt-1 mb-1"/>
                      <a href="#" class="btn btn-secondary btn-sm suratjalan-action-view" data-surat-jalan="${item.id}" data-toggle="modal" data-target="#${_modal_view_surat_jalan}">
                        <i class="zmdi zmdi-eye"></i> View
                      </a>
                      ${buttonStatus}
                    </div>
                  `;
                });
              };

              return (htmlDom !== "") ? htmlDom : htmlDomEmpty;
            }
          },
          {
            data: null,
            className: "text-center",
            render: function(data, type, row, meta) {
              var htmlDom = "";

              if (row.total_info.selisih > 0) {
                htmlDom = `
                  <div class="action">
                    <a href="javascript:;" data-toggle="modal" data-target="#${_modal_form_surat_jalan}" class="btn btn-success action-add-surat" permintaan-id="${row.id}">
                      <i class="zmdi zmdi-plus-circle"></i> Buat Surat Jalan
                    </a>
                  </div>
                `;
              } else {
                htmlDom = `<i class="zmdi zmdi-check-circle" title="Selesai, tidak ada sisa barang yang belum dikirim."></i>`;
              };

              return htmlDom;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7]
        }, {
          className: 'tablet',
          targets: [0, 1, 2]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_list).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle add surat
    $(document).on("click", ".action-add-surat", function() {
      resetFormSuratJalan();

      const permintaanId = $(this).attr("permintaan-id");
      _key = permintaanId;

      $.ajax({
        url: "<?= base_url('suratjalan/ajax_get_item_by_permintaan/') ?>" + permintaanId,
        type: "get",
        dataType: "json",
        success: function(response) {
          if (response) {
            var head = response.head;
            var item = response.item;
            var nomor = (head.nomor !== null) ? head.nomor : response.nomor_baru;
            var no = 1;
            var htmlDom = "";

            // Create table item
            if (item.length > 0) {
              item.map((item, index) => {
                var isEditable = `<td colspan="2"><i>Terkirim semua</i></td>`;

                if (item.belum_dikirim > 0) {
                  isEditable = `
                    <td>
                      <div class="form-group m-0">
                        <div class="input-group">
                          <input type="number" name="dikirim[${item.supplier_barang_id}][quantity]" class="form-control suratjalan-dikirim suratjalan-dikirim-${item.supplier_barang_id} mask-number pl-0" placeholder="0" min="0" max="${item.belum_dikirim}"/>
                          <i class="form-group__bar"></i>
                          <div class="input-group-append">
                            <span class="input-group-text">${item.quantity_unit}</span>
                          </div>
                        </div>
                      </div>
                      <input type="hidden" name="dikirim[${item.supplier_barang_id}][quantity_unit]" class="form-control" value="${item.quantity_unit}"/>
                      <input type="hidden" name="dikirim[${item.supplier_barang_id}][kode_barang]" class="form-control" value="${item.kode_barang}"/>
                      <input type="hidden" name="dikirim[${item.supplier_barang_id}][nama_barang]" class="form-control" value="${item.nama_barang}"/>
                    </td>
                    <td>
                      <div class="form-group m-0">
                        <input type="text" name="dikirim[${item.supplier_barang_id}][keterangan]" class="form-control suratjalan-keterangan suratjalan-keterangan-${item.supplier_barang_id}"/>
                        <i class="form-group__bar"></i>
                      </div>
                    </td>
                  `;
                };

                htmlDom += `
                  <tr>
                    <td>${no++}</td>
                    <td>${item.nama_barang}</td>
                    <td>${item.kode_barang}</td>
                    <td>${item.permintaan} ${item.quantity_unit}</td>
                    <td>${item.belum_dikirim} ${item.quantity_unit}</td>
                    ${isEditable}
                  </tr>
                `;
              });
            };

            $(`#${_form_surat_jalan} .suratjalan-permintaan_barang_id`).val(permintaanId).trigger("input");
            $(`#${_form_surat_jalan} .suratjalan-pengiriman_ke`).val(head.next_pengiriman_ke).trigger("input");
            $(`#${_form_surat_jalan} .suratjalan-penerima`).val(head.penerima).trigger("input");
            $(`#${_form_surat_jalan} .suratjalan-nomor`).val(nomor).trigger("input");
            $(`#${_table_item_list} .suratjalan-list`).html("");
            $(`#${_table_item_list} .suratjalan-list`).append(htmlDom);
          };
        }
      });
    });

    // Handle view surat
    $(document).on("click", ".suratjalan-action-view", function(e) {
      e.preventDefault();
      const suratJalan = $(this).attr("data-surat-jalan");

      $("#" + _modal_view_surat_jalan + " .suratjalan-preview").html("Please wait...");

      $.ajax({
        type: "get",
        url: `<?php echo base_url('suratjalan/ajax_get_preview/') ?>${suratJalan}`,
        success: function(response) {
          $("#" + _modal_view_surat_jalan + " .suratjalan-preview").html(response);
        }
      });
    });

    // Handle change status
    $(document).on("click", ".suratjalan-action-status", function(e) {
      e.preventDefault();

      const suratJalan = $(this).attr("data-surat-jalan");
      const status = $(this).attr("data-status");
      var statusLabel = (status == "1") ? "Sedang Dikirim" : "Complete";

      swal({
        title: `Ubah status pengiriman menjadi "${statusLabel}" ?`,
        text: "Once changed, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "GET",
            url: `<?php echo base_url('suratjalan/ajax_set_status/') ?>${suratJalan}/${status}`,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_list).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle print
    $(document).on("click", ".suratjalan-action-print", function(e) {
      e.preventDefault();
      $("#" + _modal_view_surat_jalan + " .suratjalan-preview").printThis();
    });

    // Handle data submit
    $("#" + _modal_form_surat_jalan + " .suratjalan-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('suratjalan/ajax_save/') ?>" + _key,
        data: $("#" + _form_surat_jalan).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            $("#" + _modal_form_surat_jalan).modal("hide");
            $("#" + _table_list).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    function resetFormSuratJalan() {
      _key = "";
      $(`#${_form_surat_jalan} .suratjalan-permintaan_barang_id`).val("").trigger("input");
      $(`#${_form_surat_jalan} .suratjalan-pengiriman_ke`).val("").trigger("input");
      $(`#${_form_surat_jalan} .suratjalan-penerima`).val("").trigger("input");
      $(`#${_form_surat_jalan} .suratjalan-nomor`).val("").trigger("input");
      $(`#${_table_item_list} .suratjalan-list`).html("");
    };
  });

  function getStatus(status) {
    switch (status.toString()) {
      case '0':
        return 'Belum Dikirim';
        break;
      case '1':
        return 'Sedang Dikirim';
        break;
      case '2':
        return 'Complete';
        break;
      default:
        return 'Undefined';
        break;
    };
  };

  function randomString(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;

    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    };

    return result;
  };
</script>