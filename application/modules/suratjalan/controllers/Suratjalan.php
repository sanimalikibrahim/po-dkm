<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Suratjalan extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'SupplierbarangModel',
      'PermintaanbarangModel',
      'PermintaanbarangitemModel',
      'PermintaanbaranghistoryModel',
      'SuratjalanModel',
      'SuratjalanitemModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $agent = new Mobile_Detect;
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('suratjalan', false, array(
        'controller' => $this,
      )),
      'card_title' => 'Surat Jalan',
      'is_mobile' => $agent->isMobile(),
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_list()
  {
    $this->handle_ajax_request();
    $role = strtolower($this->session->userdata('user')['role']);
    $dtAjax_config = array(
      'table_name' => 'view_permintaan_barang',
      'order_column' => 5,
      'order_column_dir' => 'desc',
      'static_conditional' => ($role == 'administrasi') ? array('created_by' => $this->session->userdata('user')['id']) : [],
      'static_conditional_in_key' => 'status',
      'static_conditional_in' => array(5),
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    $responseData = (isset($response['data'])) ? $response['data'] : array();

    // Assign new object
    if (count($responseData) > 0) {
      foreach ($responseData as $index => $item) {
        $totalInfo = $this->SuratjalanModel->getTotalInfo($item['id']);
        $suratJalanData = $this->SuratjalanModel->getAll(['permintaan_barang_id' => $item['id']]);

        // Create object total_info
        $response['data'][$index]['total_info'] = $totalInfo;

        if (count($suratJalanData) > 0) {
          foreach ($suratJalanData as $index2 => $item2) {
            $totalDikirim = $this->SuratjalanitemModel->getTotalDikirim($item2->id);
            $item2->total_dikirim = $totalDikirim->quantity;

            $response['data'][$index]['surat_jalan'][] = $item2;
          };
        } else {
          $response['data'][$index]['surat_jalan'] = array(); // default
        };
      };
    };

    echo json_encode($response);
  }

  public function ajax_set_status($suratJalanId = null, $status = null)
  {
    $this->handle_ajax_request();
    $fullName = $this->session->userdata('user')['nama_lengkap'];

    if (!is_null($suratJalanId) && !is_null($status)) {
      $response = $this->SuratjalanModel->set_status($suratJalanId, $status);

      if ((int) $status == 2) {
        $suratJalan = $this->SuratjalanModel->getDetail(['id' => $suratJalanId]);
        // Notification
        $notification_data = array(
          'ref' => 'suratjalan',
          'ref_id' => $suratJalanId,
          'description' => '<span style="color: #2196F3;">' . $fullName . '</span> pengiriman barang selesai.',
          'link' => 'tagihan/?ref=' . $suratJalan->nomor
        );
        $this->set_notification($notification_data, 'Admin Billing');
        // END ## Notification
      };
    } else {
      $response = array('status' => false, 'data' => '(System) Parameters is bad.');
    };

    echo json_encode($response);
  }

  public function ajax_get_orderitem($id = null)
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'permintaan_barang_item',
      'order_column' => 4,
      'order_column_dir' => 'asc',
      'static_conditional' => array('permintaan_barang_id' => $id)
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SuratjalanModel->rules());

    if ($this->form_validation->run() === true) {
      $suratJalan = $this->SuratjalanModel->insert();

      if ($suratJalan['status'] === true) {
        $dikirim = $this->input->post('dikirim');
        $suratJalanItem_temp = array();

        if (count($dikirim) > 0) {
          foreach ($dikirim as $supplier_barang_id => $item) {
            $suratJalanItem_temp[] = array(
              'surat_jalan_id' => $suratJalan['data_id'],
              'supplier_barang_id' => $supplier_barang_id,
              'kode_barang' => $item['kode_barang'],
              'nama_barang' => $item['nama_barang'],
              'quantity' => $item['quantity'],
              'quantity_unit' => $item['quantity_unit'],
              'keterangan' => $item['keterangan'],
              'pengiriman_ke' => $this->input->post('pengiriman_ke')
            );
          };

          $suratJalanItem = $this->SuratjalanitemModel->insertBatch($suratJalanItem_temp);
          echo json_encode($suratJalanItem);
        } else {
          echo json_encode(array('status' => false, 'data' => 'Tidak ada barang yang dikirim.'));
        };
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete_orderitem($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PermintaanbarangitemModel->delete($id));
  }

  public function ajax_get_supplier_barang($id = null)
  {
    $this->handle_ajax_request();
    echo json_encode($this->SupplierbarangModel->getDetail(['id' => $id]));
  }

  public function ajax_get_item_by_permintaan($permintaanBarangId = null)
  {
    $this->handle_ajax_request();

    $nomor = $this->SuratjalanModel->generateNomor();
    $head = $this->SuratjalanModel->getHeadInfo($permintaanBarangId);
    $item = $this->SuratjalanitemModel->getAllByPermintaan($permintaanBarangId);
    $result = array(
      'nomor_baru' => $nomor,
      'head' => $head,
      'item' => $item
    );

    echo json_encode($result);
  }

  public function ajax_get_preview($id = null)
  {
    $this->handle_ajax_request();

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('suratjalan'),
      'card_title' => 'Surat Jalan › Preview',
      'data_id' => null,
      'data_suratjalan' => $this->SuratjalanModel->getDetail(['id' => $id]),
      'data_suratjalan_item' => $this->SuratjalanitemModel->getAll(['surat_jalan_id' => $id]),
      'controller' => $this
    );

    $agent = new Mobile_Detect;
    if ($agent->isMobile()) {
      $this->load->view('preview_sj_mobile', $data);
    } else {
      $this->load->view('preview_sj', $data);
    };
  }

  public function format_date($date)
  {
    $date = explode('-', $date);

    if (count($date) == 3) {
      $year = $date[0];
      $month = $date[1];
      $day = $date[2];
      $month_name = $this->get_month($month);

      return $day . ' ' . $month_name . ' ' . $year;
    } else {
      return '-';
    };
  }

  public function get_status($status)
  {
    switch ((int) $status) {
      case 0:
        return 'Belum Dikirim';
        break;
      case 1:
        return 'Sedang Dikirim';
        break;
      case 2:
        return 'Complete';
        break;
      default:
        return 'Undefined';
        break;
    };
  }
}
