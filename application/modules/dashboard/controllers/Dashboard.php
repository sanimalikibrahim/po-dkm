<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Dashboard extends AppBackend
{
	function __construct()
	{
		parent::__construct();
		$this->load->model([
			'PackingorderModel',
			'PenawaranModel',
			'NotificationModel',
		]);
	}

	public function index()
	{
		$data = array(
			'app' => $this->app(),
			'page_title' => 'Dashboard',
			'page_subTitle' => 'Welcome to ' . $this->app()->app_name . ' v' . $this->app()->app_version,
			'data_statistic' => $this->PackingorderModel->getWaitApprovalCount(),
			'data_statistic_penawaran' => $this->PenawaranModel->getWaitApprovalCount(),
		);

		$this->template->set('title', $data['app']->app_name, TRUE);
		$this->template->load_view('index', $data, TRUE);
		$this->template->render();
	}
}
