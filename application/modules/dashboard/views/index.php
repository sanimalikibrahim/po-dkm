<style type="text/css">
    .img-home {
        width: 70%;
    }

    .text-small {
        font-size: 1rem;
        display: block;
        color: rgba(255, 255, 255, .8);
        font-weight: 600;
    }

    @media only screen and (max-width: 768px) {
        .img-home {
            width: 100%;
        }
    }
</style>

<div class="row">
    <div class="col-md-12">
        <center>
            <img src="<?php echo base_url('themes/_public/img/po-home.png') ?>" class="img-home" />
        </center>
    </div>
</div>

<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">Packing Order</a>
        <a class="nav-item nav-link" id="nav-2-tab" data-toggle="tab" href="#nav-2" role="tab" aria-controls="nav-2" aria-selected="false">Penawaran</a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-1" role="tabpanel" aria-labelledby="nav-1-tab">
        <div class="row quick-stats">
            <div class="col-xs-12 col-md-4">
                <div class="quick-stats__item bg-cyan <?= ($app->is_mobile === true) ? 'mb-3' : '' ?>">
                    <div class="quick-stats__info" style="position: absolute;">
                        <h2><?php echo $data_statistic->wait_supervisor ?></h2>
                        <small>Menunggu Persetujuan </small>
                        <div class="text-small">Supervisor</div>
                    </div>
                    <div class="quick-stats__chart">
                        <i class="zmdi zmdi-calendar-note text-white" style="font-size: 50px; opacity: 0.5;"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="quick-stats__item bg-blue <?= ($app->is_mobile === true) ? 'mb-3' : '' ?>">
                    <div class="quick-stats__info" style="position: absolute;">
                        <h2><?php echo $data_statistic->wait_manager ?></h2>
                        <small>Menunggu Persetujuan </small>
                        <div class="text-small">Manager</div>
                    </div>
                    <div class="quick-stats__chart">
                        <i class="zmdi zmdi-calendar-note text-white" style="font-size: 50px; opacity: 0.5;"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="quick-stats__item bg-amber <?= ($app->is_mobile === true) ? 'mb-3' : '' ?>">
                    <div class="quick-stats__info" style="position: absolute;">
                        <h2><?php echo $data_statistic->wait_finance ?></h2>
                        <small>Menunggu Persetujuan </small>
                        <div class="text-small">Finance</div>
                    </div>
                    <div class="quick-stats__chart">
                        <i class="zmdi zmdi-calendar-note text-white" style="font-size: 50px; opacity: 0.5;"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="quick-stats__item bg-purple <?= ($app->is_mobile === true) ? 'mb-3' : '' ?>">
                    <div class="quick-stats__info" style="position: absolute;">
                        <h2><?php echo $data_statistic->wait_g_manager ?></h2>
                        <small>Menunggu Persetujuan </small>
                        <div class="text-small">General Manager</div>
                    </div>
                    <div class="quick-stats__chart">
                        <i class="zmdi zmdi-calendar-note text-white" style="font-size: 50px; opacity: 0.5;"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="quick-stats__item bg-indigo <?= ($app->is_mobile === true) ? 'mb-3' : '' ?>">
                    <div class="quick-stats__info" style="position: absolute;">
                        <h2><?php echo $data_statistic->wait_deputy_dir ?></h2>
                        <small>Menunggu Persetujuan </small>
                        <div class="text-small">Deputi Direktur</div>
                    </div>
                    <div class="quick-stats__chart">
                        <i class="zmdi zmdi-calendar-note text-white" style="font-size: 50px; opacity: 0.5;"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="quick-stats__item bg-green <?= ($app->is_mobile === true) ? 'mb-3' : '' ?>">
                    <div class="quick-stats__info" style="position: absolute;">
                        <h2><?php echo $data_statistic->wait_direktur ?></h2>
                        <small>Menunggu Persetujuan </small>
                        <div class="text-small">Direktur</div>
                    </div>
                    <div class="quick-stats__chart">
                        <i class="zmdi zmdi-calendar-note text-white" style="font-size: 50px; opacity: 0.5;"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-2" role="tabpanel" aria-labelledby="nav-2-tab">
        <div class="row quick-stats">
            <div class="col-xs-12 col-md-4">
                <div class="quick-stats__item bg-blue pb-5 <?= ($app->is_mobile === true) ? 'mb-3' : '' ?>">
                    <div class="quick-stats__info" style="position: absolute;">
                        <h2><?php echo $data_statistic_penawaran->menunggu_persetujuan ?></h2>
                        <small>Menunggu Persetujuan </small>
                        <span class="badge badge-warning mt-2" style="font-size: 8px; background: #227ec7;">ALL</span>
                    </div>
                    <div class="quick-stats__chart">
                        <i class="zmdi zmdi-calendar-note text-white" style="font-size: 50px; opacity: 0.5;"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="quick-stats__item bg-success pb-5 <?= ($app->is_mobile === true) ? 'mb-3' : '' ?>">
                    <div class="quick-stats__info" style="position: absolute;">
                        <h2><?php echo $data_statistic_penawaran->selesai ?></h2>
                        <small>Disetujui </small>
                        <span class="badge badge-warning mt-2" style="font-size: 8px; background: #2aa872;">ALL</span>
                    </div>
                    <div class="quick-stats__chart">
                        <i class="zmdi zmdi-calendar-check text-white" style="font-size: 50px; opacity: 0.5;"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="quick-stats__item bg-danger pb-5 <?= ($app->is_mobile === true) ? 'mb-3' : '' ?>">
                    <div class="quick-stats__info" style="position: absolute;">
                        <h2><?php echo $data_statistic_penawaran->ditolak ?></h2>
                        <small>Ditolak </small>
                        <span class="badge badge-warning mt-2" style="font-size: 8px; background: #d45553;">ALL</span>
                    </div>
                    <div class="quick-stats__chart">
                        <i class="zmdi zmdi-calendar-close text-white" style="font-size: 50px; opacity: 0.5;"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="quick-stats__item bg-indigo pb-5 <?= ($app->is_mobile === true) ? 'mb-3' : '' ?>">
                    <div class="quick-stats__info" style="position: absolute;">
                        <h2><?php echo $data_statistic_penawaran->dibatalkan ?></h2>
                        <small>Dibatalkan </small>
                        <span class="badge badge-warning mt-2" style="font-size: 8px; background: #314199;">ALL</span>
                    </div>
                    <div class="quick-stats__chart">
                        <i class="zmdi zmdi-calendar text-white" style="font-size: 50px; opacity: 0.5;"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>