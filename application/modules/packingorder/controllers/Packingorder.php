<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Packingorder extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'DocumentModel',
      'PackingorderModel',
      'PackingorderitemModel',
      'PackingorderhistoryModel',
      'SupplierModel',
      'SupplierbarangModel'
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    redirect(base_url('packingorder/approvalresult'));
  }

  public function draft()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('packingorder'),
      'card_title' => 'Packing Order › Draft'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('grid_draft', $data, TRUE);
    $this->template->render();
  }

  public function approvalresult()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('packingorder'),
      'card_title' => 'Packing Order › Hasil Persetujuan'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('grid_approval_result', $data, TRUE);
    $this->template->render();
  }

  public function approved()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('packingorder'),
      'card_title' => 'Packing Order › Rekap'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('grid_approved', $data, TRUE);
    $this->template->render();
  }

  public function approval()
  {
    $agent = new Mobile_Detect;
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('packingorder'),
      'card_title' => 'Packing Order › Persetujuan',
      'controller' => $this,
      'is_mobile' => $agent->isMobile()
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('grid_approval', $data, TRUE);
    $this->template->render();
  }

  public function download($id = null)
  {
    libxml_use_internal_errors(true);
    try {
      $data = $this->PackingorderModel->getDetail(['id' => $id]);
      $dataOrderitem = $this->PackingorderitemModel->getAll(['packing_order_id' => $id]);
      $dataTtd = $this->_getTtd($id);
      $fileName = 'DKM-Packing-Order-' . date('YmdHis') . '.pdf';
      $template = file_get_contents(FCPATH . '/directory/templates/packing-order-print.php');
      $supplier_fax = (isset($data->supplier_fax) && !empty($data->supplier_fax)) ? ' / ' . $data->supplier_fax : '';

      // Set static value
      $template = str_replace('%img_logo_1%', base_url('themes/_public/img/logo/dkm-black.png'), $template);
      $template = str_replace('%img_logo_2%', base_url('themes/_public/img/logo/sics.png'), $template);
      // $template = str_replace('%p2_total_potongan%', number_format(null, 0, ',', '.'), $template);
      $template = str_replace('%supplier_nama%', $data->supplier_nama, $template);
      $template = str_replace('%supplier_alamat%', $data->supplier_alamat, $template);
      $template = str_replace('%supplier_nama_kontak%', $data->supplier_nama_kontak, $template);
      $template = str_replace('%supplier_telepon_fax%', $data->supplier_telepon . $supplier_fax, $template);
      $template = str_replace('%tanggal%', $this->format_date($data->tanggal), $template);
      $template = str_replace('%nomor%', $data->nomor, $template);
      $template = str_replace('%term_of_delivery%', $this->format_date($data->term_of_delivery), $template);
      $template = str_replace('%term_of_payment%', $data->term_of_payment . ' ' . $data->term_of_payment_satuan, $template);

      // Order item lists
      $sub_total = 0;
      $ppn = 0;
      $total = 0;
      $no = 1;
      $orderItemLists  = '';

      if (isset($dataOrderitem) && count($dataOrderitem) > 0) {
        foreach ($dataOrderitem as $key => $item) {
          $note = (!empty(trim($item->note))) ? '<br/><br/> Note : </br>' . $item->note : '';
          $orderItemLists .= '
            <tr>
              <td valign="top" align="center">' . $no++ . '</td>
              <td valign="top">
                ' . $item->description . '
                ' . $note . '
              </td>
              <td valign="top" align="center" width="100">
                ' . $item->quantity . '
                ' . $item->quantity_unit . '
              </td>
              <td valign="top" width="150">
                <table style="width: 100%;">
                  <tr>
                    <td style="border: none; padding: 0;">Rp</td>
                    <td style="border: none; padding: 0; text-align: right;">' . number_format($item->unit_price) . '</td>
                  </tr>
                </table>
              </td>
              <td valign="top" width="150">
                <table style="width: 100%;">
                  <tr>
                    <td style="border: none; padding: 0;">Rp</td>
                    <td style="border: none; padding: 0; text-align: right;">' . number_format($item->amount) . '</td>
                  </tr>
                </table>
              </td>
            </tr>';
          $sub_total = $sub_total + (float) $item->amount;
        };

        $ppn = $sub_total * 0.11;
        $total = ($data->ppn == 'Yes') ? $sub_total + $ppn : $sub_total;
      } else {
        $orderItemLists  = '
          <tr>
            <td colspan="5" style="padding: 15px;">
              No data available in table
            </td>
          </tr>';
      };
      // END ## Order item lists

      $start_wrap_ppn = ($data->ppn == 'Yes') ? '' : '<div style="display: none;">';
      $end_wrap_ppn = ($data->ppn == 'Yes') ? '' : '</div>';

      $template = str_replace('%order_item_list%', $orderItemLists, $template);
      $template = str_replace('%sub_total%', number_format($sub_total), $template);
      $template = str_replace('%ppn%', number_format($ppn), $template);
      $template = str_replace('%total%', number_format($total), $template);
      $template = str_replace('%ttd_administrasi%', $dataTtd->administrasi, $template);
      $template = str_replace('%ttd_supervisor%', $dataTtd->supervisor, $template);
      $template = str_replace('%ttd_manager%', $dataTtd->manager, $template);
      $template = str_replace('%ttd_finance%', $dataTtd->finance, $template);
      $template = str_replace('%ttg_g_manager%', $dataTtd->g_manager, $template);
      $template = str_replace('%ttd_deputy_dir%', $dataTtd->deputy_dir, $template);
      $template = str_replace('%ttd_direktur%', $dataTtd->direktur, $template);
      $template = str_replace('%start_wrap_ppn%', $start_wrap_ppn, $template);
      $template = str_replace('%end_wrap_ppn%', $end_wrap_ppn, $template);

      // Generate PDF
      $mpdf = new \Mpdf\Mpdf();
      $mpdf->WriteHTML($template);

      header("Content-type:application/pdf");
      header("Content-Disposition:attachment;filename='$fileName'");

      $mpdf->Output($fileName, 'D');
    } catch (\Throwable $th) {
      echo '<h5>Terjadi kesalahan ketika membuat file, coba lagi nanti.</h5><hr/><i>Atau silahkan hubungi Administrator.</i>';
    };
  }

  public function wizard($step = 0, $id = null)
  {
    switch ($step) {
      case 0:
        return $this->_wizard_0($id);
        break;
      case 1:
        return $this->_wizard_1($id);
        break;
      case 2:
        return $this->_wizard_2($id);
        break;
      case 3:
        return $this->_wizard_3($id);
        break;
      default:
        return $this->_wizard_0($id);
        break;
    };
  }

  private function _wizard_0($id)
  {
    if (strtolower($this->session->userdata('user')['role']) == 'administrasi') {
      $agent = new Mobile_Detect;
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('packingorder'),
        'card_title' => 'Packing Order',
        'data_id' => $id,
        'data_auto_nomor' => $this->PackingorderModel->generateNomor(),
        'data_packing_order' => $this->PackingorderModel->getDetail(['id' => $id]),
        'data_supplier' => $this->SupplierModel->getAll(),
        'data_attachment' => $this->DocumentModel->getAll(['ref' => 'packingorder', 'ref_id' => $id]),
        'tab_wizard' => 0,
        'status_po' => 0,
        'controller' => $this,
        'is_mobile' => $agent->isMobile()
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_packing_order', $data, TRUE);
      $this->template->render();
    } else {
      redirect(base_url('packingorder/wizard/2/' . $id));
    };
  }

  private function _wizard_1($id)
  {
    if (strtolower($this->session->userdata('user')['role']) == 'administrasi') {
      $agent = new Mobile_Detect;
      $packingOrder = $this->PackingorderModel->getDetail(['id' => $id]);
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('packingorder'),
        'card_title' => 'Packing Order › Input Order Item',
        'data_id' => $id,
        'data_packing_order' => $packingOrder,
        'data_attachment' => $this->DocumentModel->getAll(['ref' => 'packingorder', 'ref_id' => $id]),
        'data_supplier_barang' => $this->SupplierbarangModel->getAll(['supplier_id' => $packingOrder->supplier_id]),
        'tab_wizard' => 1,
        'status_po' => 0,
        'controller' => $this,
        'is_mobile' => $agent->isMobile()
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_order_item', $data, TRUE);
      $this->template->render();
    } else {
      redirect(base_url('packingorder/wizard/2/' . $id));
    };
  }

  private function _wizard_2($id)
  {
    $packingOrder = $this->PackingorderModel->getDetail(['id' => $id]);
    if ($packingOrder->status == 0 && strtolower($this->session->userdata('user')['role']) != 'administrasi') {
      redirect(base_url());
    } else {
      $agent = new Mobile_Detect;
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('packingorder'),
        'card_title' => 'Packing Order › Preview',
        'data_id' => $id,
        'data_packing_order' => $packingOrder,
        'data_order_item' => $this->PackingorderitemModel->getAll(['packing_order_id' => $id]),
        'data_ttd' => $this->_getTtd($id),
        'data_attachment' => $this->DocumentModel->getAll(['ref' => 'packingorder', 'ref_id' => $id]),
        'tab_wizard' => 2,
        'status_po' => 0,
        'controller' => $this,
        'is_mobile' => $agent->isMobile()
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_preview', $data, TRUE);
      $this->template->render();
    };
  }

  private function _wizard_3($id)
  {
    $packingOrder = $this->PackingorderModel->getDetail(['id' => $id]);
    if ($packingOrder->status == 0 && strtolower($this->session->userdata('user')['role']) != 'administrasi') {
      redirect(base_url());
    } else {
      $agent = new Mobile_Detect;
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('packingorder'),
        'card_title' => 'Packing Order › Approval History',
        'data_id' => $id,
        'data_packing_order' => $packingOrder,
        'data_approval_history' => $this->PackingorderhistoryModel->getAll(['packing_order_id' => $id]),
        'tab_wizard' => 3,
        'status_po' => 1,
        'controller' => $this,
        'is_mobile' => $agent->isMobile()
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_approval_history', $data, TRUE);
      $this->template->render();
    };
  }

  public function ajax_get_draft()
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'view_packing_order',
      'order_column' => 8,
      'order_column_dir' => 'desc',
      'static_conditional' => array(
        'status' => 0,
        'created_by' => $this->session->userdata('user')['id']
      ),
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_delete_draft($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PackingorderModel->delete($id));
  }

  public function ajax_get_approvalresult()
  {
    $this->handle_ajax_request();
    $role = strtolower($this->session->userdata('user')['role']);
    $dtAjax_config = array(
      'table_name' => 'view_packing_order',
      'order_column' => 8,
      'order_column_dir' => 'desc',
      'static_conditional' => ($role == 'administrasi') ? array('created_by' => $this->session->userdata('user')['id']) : [],
      'static_conditional_in_key' => 'status',
      'static_conditional_in' => array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_get_approved($month = null, $year = null)
  {
    $this->handle_ajax_request();
    $month = (is_null($month)) ? date('m') : $month;
    $year = (is_null($year)) ? date('Y') : $year;
    $dtAjax_config = array(
      'table_name' => 'view_packing_order_rekap',
      'static_conditional' => array(
        'MONTH(tanggal)' => $month,
        'YEAR(tanggal)' => $year
      )
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_get_approval()
  {
    $this->handle_ajax_request();
    $role = strtolower($this->session->userdata('user')['role']);
    $status = [];

    switch ($role) {
      case 'supervisor':
        $status = [1];
        break;
      case 'manager':
        $status = [2];
        break;
      case 'finance':
        $status = [3];
        break;
      case 'g. manager':
        $status = [4];
        break;
      case 'deputy. dir':
        $status = [5];
        break;
      case 'direktur':
        $status = [6];
        break;
      default:
        $status = [-1];
        break;
    };

    $dtAjax_config = array(
      'table_name' => 'view_packing_order',
      'order_column' => 7,
      'order_column_dir' => 'desc',
      'static_conditional_in_key' => 'status',
      'static_conditional_in' => $status
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_get_preview($id)
  {
    $this->handle_ajax_request();

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('packingorder'),
      'card_title' => 'Packing Order › Preview',
      'data_id' => $id,
      'data_packing_order' => $this->PackingorderModel->getDetail(['id' => $id]),
      'data_order_item' => $this->PackingorderitemModel->getAll(['packing_order_id' => $id]),
      'data_ttd' => $this->_getTtd($id),
      'tab_wizard' => 2,
      'status_po' => 0,
      'controller' => $this
    );

    $agent = new Mobile_Detect;
    if ($agent->isMobile()) {
      $this->load->view('preview_po_mobile', $data);
    } else {
      $this->load->view('preview_po', $data);
    };
  }

  public function ajax_get_preview_attachment($id)
  {
    $agent = new Mobile_Detect;
    $data = array(
      'app' => $this->app(),
      'data_attachment' => $this->DocumentModel->getAll(['ref' => 'packingorder', 'ref_id' => $id]),
      'is_mobile' => $agent->isMobile()
    );
    $this->load->view('preview_attachment', $data);
  }

  public function ajax_save_po($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->PackingorderModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        $transaction = $this->PackingorderModel->insert();

        if ($transaction['status'] === true) {
          $id = $transaction['data_id'];
          $attachment = $this->_save_po_attachment($id);

          if ($attachment['status'] === true) {
            $response = array('status' => true, 'data' => base_url('packingorder/wizard/1/' . $id));
          } else {
            $response = $attachment;
          };
        } else {
          $response = $transaction;
        };
      } else {
        $transaction = $this->PackingorderModel->update($id);

        if ($transaction['status'] === true) {
          $attachment = $this->_save_po_attachment($id);

          if ($attachment['status'] === true) {
            $response = array('status' => true, 'data' => base_url('packingorder/wizard/1/' . $id));
          } else {
            $response = $attachment;
          };
        } else {
          $response = $transaction;
        };
      };

      echo json_encode($response);
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  private function _save_po_attachment($id)
  {
    $this->handle_ajax_request();

    if (!empty($_FILES['file_name'])) {
      $cpUpload = new CpUpload();
      $files = $cpUpload->re_arrange($_FILES['file_name']);
      $post = array();
      $error = '';
      $directory = 'packingorder';

      foreach ($files as $item) {
        if (!empty($item['name'])) {
          $upload = $cpUpload->run($item, $directory, true, true, 'jpg|jpeg|png|gif|pdf', true);

          if ($upload->status === true) {
            $post[] = array(
              'ref' => $directory,
              'ref_id' => $id,
              'description' => $_POST['nomor'],
              'file_raw_name' => $upload->data->raw_name . $upload->data->file_ext,
              'file_raw_name_thumb' => ($upload->data->is_image) ? $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
              'file_name' => $upload->data->base_path,
              'file_name_thumb' => ($upload->data->is_image) ? 'directory/' . $directory . '/' . $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
              'file_size' => $upload->data->file_size,
              'file_type' => $upload->data->file_type,
              'file_ext' => $upload->data->file_ext,
              'created_by' =>  $this->session->userdata('user')['id']
            );
          } else {
            $error .= $upload->data;
          };
        };
      };

      if (empty($error) && count($post) > 0) {
        return $this->DocumentModel->insertBatch($post);
      } else {
        return array('status' => false, 'data' => $error);
      };
    } else {
      return array('status' => true, 'data' => 'Skip, no data to upload.');
    };
  }

  public function ajax_set_status($id = null)
  {
    $this->handle_ajax_request();

    if (!is_null($id)) {
      $transaction = $this->PackingorderModel->set_status($id, 1);
      $packingOrder = $this->PackingorderModel->getDetail(['id' => $id]);

      if ($transaction['status'] === true) {
        // Log
        $log_data = array(
          'packing_order_id' => $id,
          'user_pic' => $this->session->userdata('user')['id'],
          'action' => 'Sent',
          'description' => 'Mengajukan packing order',
          'revisi' => $transaction['revisi']
        );
        $log = $this->PackingorderhistoryModel->insert($log_data);
        // END ## Log

        if ($log['status'] === true) {
          // Notification
          $notification_data = array(
            'ref' => 'packingorder',
            'ref_id' => $id,
            'description' => '<span style="color: #2196F3;">' . $packingOrder->created_by_name . '</span> mengajukan packing order ' . $packingOrder->nomor . '.',
            'link' => 'packingorder/approval/?ref=' . $id
          );
          $notification = $this->set_notification($notification_data, 'Supervisor');
          // END ## Notification

          if ($notification['status'] === true) {
            $response = array('status' => true, 'data' => base_url('packingorder/wizard/3/' . $id));
          } else {
            $response = $notification;
          };
        } else {
          $response = $log;
        };
      } else {
        $response = $transaction;
      };
    } else {
      $response = array('status' => false, 'data' => '(System) Parameters is bad.');
    };

    echo json_encode($response);
  }

  public function ajax_get_orderitem($id = null)
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'view_packing_order_item',
      'order_column' => 6,
      'order_column_dir' => 'asc',
      'static_conditional' => array('packing_order_id' => $id)
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save_orderitem($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->PackingorderitemModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->PackingorderitemModel->insert());
      } else {
        echo json_encode($this->PackingorderitemModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete_orderitem($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PackingorderitemModel->delete($id));
  }

  public function ajax_save_approval($status, $id)
  {
    $this->handle_ajax_request();

    $post = $this->input->post();
    $role = strtolower($this->session->userdata('user')['role']);
    $action = ((int) $status == 1) ? 'Approve' : 'Reject';
    $note = (isset($post['note']) && !empty($post['note'])) ? ', Note :<br/>' . $post['note'] : '';
    $description = ((int) $status == 1) ? 'Menyetujui' . $note : 'Menolak' . $note;
    $notification_status = ((int) $status == 1) ? 'menyetujui packing order' : 'menolak packing order';
    $status_by_role = null;
    $notification_back_to = [];
    $notification_next_to = [];
    $notification_status_back = null;
    $notification_status_next = null;

    if ((int) $status == 1) {
      // Approved
      switch ($role) {
        case 'supervisor':
          $status_by_role = 2;
          $notification_back_to = [];
          $notification_next_to = ['Manager'];
          $notification_status_back = null;
          $notification_status_next = 'mengajukan packing order';
          break;
        case 'manager':
          $status_by_role = 3;
          $notification_back_to = ['Supervisor'];
          $notification_next_to = ['Finance'];
          $notification_status_back = 'menyetujui packing order';
          $notification_status_next = 'mengajukan packing order';
          break;
        case 'finance':
          $status_by_role = 4;
          $notification_back_to = ['Supervisor', 'Manager'];
          $notification_next_to = ['G. Manager'];
          $notification_status_back = 'menyetujui packing order';
          $notification_status_next = 'mengajukan packing order';
          break;
        case 'g. manager':
          $status_by_role = 5;
          $notification_back_to = ['Supervisor', 'Manager', 'Finance'];
          $notification_next_to = ['Deputy. Dir'];
          $notification_status_back = 'menyetujui packing order';
          $notification_status_next = 'mengajukan packing order';
          break;
        case 'deputy. dir':
          $status_by_role = 6;
          $notification_back_to = ['Supervisor', 'Manager', 'Finance', 'G. Manager'];
          $notification_next_to = ['Direktur'];
          $notification_status_back = 'menyetujui packing order';
          $notification_status_next = 'mengajukan packing order';
          break;
        case 'direktur':
          $status_by_role = 13;
          $notification_back_to = ['Supervisor', 'Manager', 'Finance', 'G. Manager', 'Deputy. Dir'];
          $notification_next_to = [];
          $notification_status_back = 'menyetujui packing order';
          $notification_status_next = null;
          break;
      };
    } else {
      // Rejected
      switch ($role) {
        case 'supervisor':
          $status_by_role = 7;
          $notification_back_to = [];
          $notification_next_to = [];
          $notification_status_back = null;
          $notification_status_next = null;
          break;
        case 'manager':
          $status_by_role = 8;
          $notification_back_to = ['Supervisor'];
          $notification_next_to = [];
          $notification_status_back = 'menolak packing order';;
          $notification_status_next = null;
          break;
        case 'finance':
          $status_by_role = 9;
          $notification_back_to = ['Supervisor', 'Manager'];
          $notification_next_to = [];
          $notification_status_back = 'menolak packing order';
          $notification_status_next = null;
          break;
        case 'g. manager':
          $status_by_role = 10;
          $notification_back_to = ['Supervisor', 'Manager', 'Finance'];
          $notification_next_to = [];
          $notification_status_back = 'menolak packing order';
          $notification_status_next = null;
          break;
        case 'deputy. dir':
          $status_by_role = 11;
          $notification_back_to = ['Supervisor', 'Manager', 'Finance', 'G. Manager'];
          $notification_next_to = [];
          $notification_status_back = 'menolak packing order';
          $notification_status_next = null;
          break;
        case 'direktur':
          $status_by_role = 12;
          $notification_back_to = ['Supervisor', 'Manager', 'Finance', 'G. Manager', 'Deputy. Dir'];
          $notification_next_to = [];
          $notification_status_back = 'menolak packing order';
          $notification_status_next = null;
          break;
      };
    };

    if (!is_null($status_by_role)) {
      $transaction = $this->PackingorderModel->set_status($id, $status_by_role);

      if ($transaction['status'] === true) {
        $log_data = array(
          'packing_order_id' => $id,
          'user_pic' => $this->session->userdata('user')['id'],
          'action' => $action,
          'description' => $description,
          'revisi' => $transaction['revisi']
        );
        $log = $this->PackingorderhistoryModel->insert($log_data);

        if ($log['status'] === true) {
          $packingOrder = $this->PackingorderModel->getDetail(['id' => $id]);
          $userFullName = $this->session->userdata('user')['nama_lengkap'];

          // Notification
          // To Administrasi
          $notification_data = array(
            'user_to' => $packingOrder->created_by,
            'ref' => 'packingorder',
            'ref_id' => $id,
            'description' => '<span style="color: #2196F3;">' . $userFullName . '</span> ' . $notification_status . ' ' . $packingOrder->nomor . '.',
            'link' => 'packingorder/wizard/3/' . $id
          );
          $this->set_notification($notification_data);

          // To Back Role Receiver
          if (count($notification_back_to) > 0) {
            foreach ($notification_back_to as $key => $item) {
              $notification_data = array(
                'ref' => 'packingorder',
                'ref_id' => $id,
                'description' => '<span style="color: #2196F3;">' . $userFullName . '</span> ' . $notification_status_back . ' ' . $packingOrder->nomor . '.',
                'link' => 'packingorder/wizard/3/' . $id
              );
              $this->set_notification($notification_data, $item);
            };
          };

          // To Next Role Receiver
          if (count($notification_next_to) > 0) {
            foreach ($notification_next_to as $key => $item) {
              $notification_data = array(
                'ref' => 'packingorder',
                'ref_id' => $id,
                'description' => '<span style="color: #2196F3;">' . $userFullName . '</span> ' . $notification_status_next . ' ' . $packingOrder->nomor . '.',
                'link' => 'packingorder/wizard/3/' . $id
              );
              $this->set_notification($notification_data, $item);
            };
          };
          // END ## Notification

          $response = array('status' => true, 'data' => 'Data has been saved.');
        } else {
          $response = $log;
        };
      } else {
        $response = $transaction;
      };
    } else {
      $response = array('status' => false, 'data' => '(System) Parameters is bad.');
    };

    echo json_encode($response);
  }

  public function ajax_delete_attachment($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->DocumentModel->delete($id));
  }

  public function ajax_get_supplier_barang($id = null)
  {
    $this->handle_ajax_request();
    echo json_encode($this->SupplierbarangModel->getDetail(['id' => $id]));
  }

  private function _getTtd($id)
  {
    $administrasi =  $this->PackingorderModel->checkTtd($id, 'Sent', 'Administrasi');
    $supervisor =  $this->PackingorderModel->checkTtd($id, 'Approve', 'Supervisor');
    $manager =  $this->PackingorderModel->checkTtd($id, 'Approve', 'Manager');
    $finance =  $this->PackingorderModel->checkTtd($id, 'Approve', 'Finance');
    $g_manager =  $this->PackingorderModel->checkTtd($id, 'Approve', 'G. Manager');
    $deputy_dir =  $this->PackingorderModel->checkTtd($id, 'Approve', 'Deputy. Dir');
    $direktur =  $this->PackingorderModel->checkTtd($id, 'Approve', 'Direktur');

    $result = array(
      'administrasi' => $administrasi,
      'supervisor' => $supervisor,
      'manager' => $manager,
      'finance' => $finance,
      'g_manager' => $g_manager,
      'deputy_dir' => $deputy_dir,
      'direktur' => $direktur
    );

    return (object) $result;
  }

  public function format_date($date)
  {
    $date = explode('-', $date);

    if (count($date) == 3) {
      $year = $date[0];
      $month = $date[1];
      $day = $date[2];
      $month_name = $this->get_month($month);

      return $day . ' ' . $month_name . ' ' . $year;
    } else {
      return '-';
    };
  }

  public function get_status($status)
  {
    switch ((int) $status) {
      case 0:
        return 'Draft';
        break;
      case 1:
        return 'Menunggu Persetujuan Supervisor';
        break;
      case 2:
        return 'Menunggu Persetujuan Manager';
        break;
      case 3:
        return 'Menunggu Persetujuan Finance';
        break;
      case 4:
        return 'Menunggu Persetujuan G. Manager';
        break;
      case 5:
        return 'Menunggu Persetujuan Deputy. Dir';
        break;
      case 6:
        return 'Menunggu Persetujuan Direktur';
        break;
      case 7:
        return 'Ditolak Oleh Supervisor';
        break;
      case 8:
        return 'Ditolak Oleh Manager';
        break;
      case 9:
        return 'Ditolak Oleh Finance';
        break;
      case 10:
        return 'Ditolak Oleh G. Manager';
        break;
      case 11:
        return 'Ditolak Oleh Deputy. Dir';
        break;
      case 12:
        return 'Ditolak Oleh Direktur';
        break;
      case 13:
        return 'Disetujui Direktur';
        break;
      default:
        return 'Undefined';
        break;
    };
  }
}
