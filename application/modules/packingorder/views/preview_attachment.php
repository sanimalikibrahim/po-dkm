<?php require_once('main.css.php') ?>

<?php if (count($data_attachment) > 0) : ?>
  <?php foreach ($data_attachment as $key => $item) : ?>
    <?php $isImage = (strpos($item->file_type, 'image') !== false) ? true : false ?>
    <div class="attachment-item attachment-data-item-<?php echo $item->id ?>">
      <div class="row">
        <div class="col-auto">
          <?php if ($isImage) : ?>
            <img src="<?php echo base_url($item->file_name_thumb) ?>" class="attachment-preview attachment-preview-img" />
          <?php else : ?>
            <div class="attachment-preview attachment-preview-file">
              <i class="zmdi zmdi-file-text"></i>
            </div>
          <?php endif; ?>
        </div>
        <div class="col-auto" style="padding-left: 0;">
          <div style="<?php echo ($is_mobile) ? 'text-overflow: ellipsis; overflow: hidden; width: 150px; height: 1.2em; white-space: nowrap;' : '' ?>">
            <a href="<?php echo base_url($item->file_name) ?>" target="_blank">
              <span><?php echo $item->file_raw_name ?></span>
            </a>
          </div>
          <span class="attachment-size">
            <?php echo $item->file_size ?> KB
          </span>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
<?php else : ?>
  <label>No data available</label>
<?php endif; ?>