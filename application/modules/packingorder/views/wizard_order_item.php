<?php require_once('main.css.php') ?>
<?php include_once('form_input_order_item.php') ?>

<?php $isAdmin = (strtolower($this->session->userdata('user')['role']) == 'administrasi') ? true : false ?>
<?php $flagLabel = ($isAdmin && isset($data_packing_order->status) && in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])) ? 'EDIT' : 'VIEW' ?>
<?php $isReadonly = ($isAdmin && isset($data_packing_order->status) && !in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])) ? 'disabled="true"' : '' ?>
<?php $isReadonly_opacity = ($isAdmin && isset($data_packing_order->status) && !in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])) ? '' : 'opacity: 1;' ?>

<section id="packingorder">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute;">
                        <?php echo (isset($data_packing_order->nomor)) ? $flagLabel : 'NEW' ?>
                    </span>
                    <div style="margin-left: 60px; margin-top: 5px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <div class="row">
                <?php require_once("tab_wizard.php") ?>
                <div class="col">
                    <?php if (isset($data_packing_order->status)) : ?>
                        <div class="alert alert-secondary">
                            <i class="zmdi zmdi-info-outline"></i> &nbsp;
                            <?php echo (isset($data_packing_order->status)) ? $controller->get_status($data_packing_order->status) : '' ?>
                        </div>
                        <div class="clear-sm"></div>
                    <?php endif; ?>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row">
                                <?php if (!$is_mobile) : ?>
                                    <div class="col-2 col-2-custom"><b>Nomor</b></div>
                                    <div class="col">: <?php echo (isset($data_packing_order->nomor)) ? $data_packing_order->nomor : '' ?></div>
                                <?php else : ?>
                                    <div class="col-12"><b>Nomor :</b></div>
                                    <div class="col-12"><?php echo (isset($data_packing_order->nomor)) ? $data_packing_order->nomor : '' ?></div>
                                <?php endif; ?>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <?php if (!$is_mobile) : ?>
                                    <div class="col-2 col-2-custom"><b>Supplier</b></div>
                                    <div class="col">: <?php echo (isset($data_packing_order->supplier_nama)) ? $data_packing_order->supplier_nama : '' ?></div>
                                <?php else : ?>
                                    <div class="col-12"><b>Supplier :</b></div>
                                    <div class="col"><?php echo (isset($data_packing_order->supplier_nama)) ? $data_packing_order->supplier_nama : '' ?></div>
                                <?php endif; ?>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <?php if (!$is_mobile) : ?>
                                    <div class="col-2 col-2-custom"><b>Tanggal</b></div>
                                    <div class="col">: <?php echo $controller->format_date((isset($data_packing_order->tanggal)) ? $data_packing_order->tanggal : '') ?></div>
                                <?php else : ?>
                                    <div class="col-12"><b>Tanggal :</b></div>
                                    <div class="col"><?php echo $controller->format_date((isset($data_packing_order->tanggal)) ? $data_packing_order->tanggal : '') ?></div>
                                <?php endif; ?>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <?php if (!$is_mobile) : ?>
                                    <div class="col-2 col-2-custom"><b>Term Of Delivery</b></div>
                                    <div class="col">: <?php echo $controller->format_date((isset($data_packing_order->term_of_delivery)) ? $data_packing_order->term_of_delivery : '') ?></div>
                                <?php else : ?>
                                    <div class="col-12"><b>Term Of Delivery :</b></div>
                                    <div class="col"><?php echo $controller->format_date((isset($data_packing_order->term_of_delivery)) ? $data_packing_order->term_of_delivery : '') ?></div>
                                <?php endif; ?>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <?php if (!$is_mobile) : ?>
                                    <div class="col-2 col-2-custom"><b>Term Of Payment</b></div>
                                    <div class="col">
                                        : <?php echo (isset($data_packing_order->term_of_payment)) ? $data_packing_order->term_of_payment : '' . ' ' ?>
                                        <?php echo (isset($data_packing_order->term_of_payment_satuan)) ? $data_packing_order->term_of_payment_satuan : '' ?>
                                    </div>
                                <?php else : ?>
                                    <div class="col-12"><b>Term Of Payment :</b></div>
                                    <div class="col">
                                        <?php echo (isset($data_packing_order->term_of_payment)) ? $data_packing_order->term_of_payment : '' . ' ' ?>
                                        <?php echo (isset($data_packing_order->term_of_payment_satuan)) ? $data_packing_order->term_of_payment_satuan : '' ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <?php if (!$is_mobile) : ?>
                                    <div class="col-2 col-2-custom"><b>PPN</b></div>
                                    <div class="col">: <?php echo (isset($data_packing_order->ppn)) ? $data_packing_order->ppn : '' . ' ' ?></div>
                                <?php else : ?>
                                    <div class="col-12"><b>PPN :</b></div>
                                    <div class="col"><?php echo (isset($data_packing_order->ppn)) ? $data_packing_order->ppn : '' . ' ' ?></div>
                                <?php endif; ?>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <?php if (!$is_mobile) : ?>
                                    <div class="col-2 col-2-custom"><b>Attachment</b></div>
                                <?php else : ?>
                                    <div class="col-12" style="margin-bottom: 4px;"><b>Attachment :</b></div>
                                <?php endif; ?>
                                <div class="col">
                                    <div style="display: flex;">
                                        <?php echo (!$is_mobile) ? '<span>:</span>&nbsp;' : '' ?>
                                        <div class="form-group" style="margin-bottom: 0px; flex: 1;">
                                            <div class="attachment" style="margin-top: 0px;">
                                                <?php if (count($data_attachment) > 0) : ?>
                                                    <?php foreach ($data_attachment as $key => $item) : ?>
                                                        <?php $isImage = (strpos($item->file_type, 'image') !== false) ? true : false ?>
                                                        <div class="attachment-item attachment-data-item-<?php echo $item->id ?>">
                                                            <div class="row">
                                                                <div class="col-auto">
                                                                    <?php if ($isImage) : ?>
                                                                        <img src="<?php echo base_url($item->file_name_thumb) ?>" class="attachment-preview attachment-preview-img" />
                                                                    <?php else : ?>
                                                                        <div class="attachment-preview attachment-preview-file">
                                                                            <i class="zmdi zmdi-file-text"></i>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="col-auto" style="padding-left: 0;">
                                                                    <div style="<?php echo ($is_mobile) ? 'text-overflow: ellipsis; overflow: hidden; width: 150px; height: 1.2em; white-space: nowrap;' : '' ?>">
                                                                        <a href="<?php echo base_url($item->file_name) ?>" target="_blank">
                                                                            <span><?php echo $item->file_raw_name ?></span>
                                                                        </a>
                                                                    </div>
                                                                    <span class="attachment-size">
                                                                        <?php echo $item->file_size ?> KB
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php else : ?>
                                                    <label>No data available</label>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <div class="clear"></div>
                    <div class="card">
                        <div class="card-header card-header-custom">
                            Packing Order Item
                            <?php if ($isAdmin && (!isset($data_packing_order->status) || (in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])))) : ?>
                                <div class="pull-right">
                                    <button class="btn btn-success btn-sm btn--raised packingorder-action-add-order_item" data-toggle="modal" data-target="#modal-form-order_item">
                                        <i class="zmdi zmdi-plus"></i> Add Item
                                    </button>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="card-body card-body-custom">
                            <div class="table-responsive">
                                <table id="table-order_item" class="table table-bordered">
                                    <thead class="thead-default">
                                        <tr>
                                            <th width="100">No</th>
                                            <th>Description</th>
                                            <th>Note</th>
                                            <th>Quantity</th>
                                            <th>Unit Price (IDR)</th>
                                            <th>Amount (IDR)</th>
                                            <th>Created At</th>
                                            <th width="100">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="buttons-container">
                        <div class="row">
                            <div class="col-auto">
                                <a href="<?php echo base_url('packingorder/wizard/0/' . $data_id) ?>" class="btn btn--raised btn-light btn--icon-text page-action-back spinner-action-button <?php echo (!$is_mobile) ? 'btn-custom' : '' ?>">
                                    <?php echo (!$is_mobile) ? 'Back' : '<i class="zmdi zmdi-arrow-left"></i>' ?>
                                </a>
                            </div>
                            <div class="col text-right">
                                <a href="<?php echo base_url('packingorder/wizard/2/' . $data_id) ?>" class="btn btn--raised btn-primary btn--icon-text btn-custom page-action-next spinner-action-button">
                                    Next
                                    <i class="zmdi zmdi-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>