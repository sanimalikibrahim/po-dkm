<?php require_once('main.css.php') ?>

<section id="packingorder">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-responsive">
                <table id="table-approvalresult" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Nomor</th>
                            <th>Supplier</th>
                            <th>Tanggal</th>
                            <th>Term Of Delivery</th>
                            <th>Term Of Payment</th>
                            <th>PPN</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th width="100" style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>