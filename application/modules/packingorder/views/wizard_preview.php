<?php require_once('main.css.php') ?>

<?php $isAdmin = (strtolower($this->session->userdata('user')['role']) == 'administrasi') ? true : false ?>
<?php $flagLabel = ($isAdmin && isset($data_packing_order->status) && in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])) ? 'EDIT' : 'VIEW' ?>
<?php $isReadonly = ($isAdmin && isset($data_packing_order->status) && !in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])) ? 'disabled="true"' : '' ?>
<?php $isReadonly_opacity = ($isAdmin && isset($data_packing_order->status) && !in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])) ? '' : 'opacity: 1;' ?>

<section id="packingorder">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute;">
                        <?php echo (isset($data_packing_order->nomor)) ? $flagLabel : 'NEW' ?>
                    </span>
                    <div style="margin-left: 60px; margin-top: 5px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <div class="row">
                <?php require_once("tab_wizard.php") ?>
                <div class="col">
                    <?php if (isset($data_packing_order->status)) : ?>
                        <div class="alert alert-secondary">
                            <i class="zmdi zmdi-info-outline"></i> &nbsp;
                            <?php echo (isset($data_packing_order->status)) ? $controller->get_status($data_packing_order->status) : '' ?>
                        </div>
                        <div class="clear-sm"></div>
                    <?php endif; ?>

                    <?php
                    if (!$is_mobile) {
                        include_once('preview_po.php');
                    } else {
                        include_once('preview_po_mobile.php');
                    };
                    ?>

                    <div class="form-group" style="margin-bottom: 0px;">
                        <div class="attachment">
                            <label style="margin-bottom: 0px;">Attachment</label>
                            <hr />
                            <?php if (count($data_attachment) > 0) : ?>
                                <?php foreach ($data_attachment as $key => $item) : ?>
                                    <?php $isImage = (strpos($item->file_type, 'image') !== false) ? true : false ?>
                                    <div class="attachment-item attachment-data-item-<?php echo $item->id ?>">
                                        <div class="row">
                                            <div class="col-auto">
                                                <?php if ($isImage) : ?>
                                                    <img src="<?php echo base_url($item->file_name_thumb) ?>" class="attachment-preview attachment-preview-img" />
                                                <?php else : ?>
                                                    <div class="attachment-preview attachment-preview-file">
                                                        <i class="zmdi zmdi-file-text"></i>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-auto" style="padding-left: 0;">
                                                <div style="<?php echo ($is_mobile) ? 'text-overflow: ellipsis; overflow: hidden; width: 200px; height: 1.2em; white-space: nowrap;' : '' ?>">
                                                    <a href="<?php echo base_url($item->file_name) ?>" target="_blank">
                                                        <span><?php echo $item->file_raw_name ?></span>
                                                    </a>
                                                </div>
                                                <span class="attachment-size">
                                                    <?php echo $item->file_size ?> KB
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <label>No data available</label>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="buttons-container">
                        <div class="row">
                            <?php if (strtolower($this->session->userdata('user')['role']) == 'administrasi') : ?>
                                <div class="col-auto">
                                    <a href="<?php echo base_url('packingorder/wizard/1/' . $data_id) ?>" class="btn btn--raised btn-light btn--icon-text page-action-back spinner-action-button <?php echo (!$is_mobile) ? 'btn-custom' : '' ?>">
                                        <?php echo (!$is_mobile) ? 'Back' : '<i class="zmdi zmdi-arrow-left"></i>' ?>
                                    </a>
                                </div>
                            <?php endif; ?>
                            <div class="col text-right">
                                <?php if ($isAdmin && (!isset($data_packing_order->status) || (in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])))) : ?>
                                    <button class="btn btn--raised btn-primary btn--icon-text btn-custom page-action-push spinner-action-button">
                                        Send To Supervisor
                                        <i class="zmdi zmdi-long-arrow-right"></i>
                                    </button>
                                <?php else : ?>
                                    <a href="<?php echo base_url('packingorder/wizard/3/' . $data_id) ?>" class="btn btn--raised btn-primary btn--icon-text btn-custom spinner-action-button">
                                        View History
                                        <i class="zmdi zmdi-long-arrow-right"></i>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>