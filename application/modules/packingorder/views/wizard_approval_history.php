<?php require_once('main.css.php') ?>

<?php $isAdmin = (strtolower($this->session->userdata('user')['role']) == 'administrasi') ? true : false ?>
<?php $flagLabel = ($isAdmin && isset($data_packing_order->status) && in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])) ? 'EDIT' : 'VIEW' ?>
<?php $isReadonly = ($isAdmin && isset($data_packing_order->status) && !in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])) ? 'disabled="true"' : '' ?>
<?php $isReadonly_opacity = ($isAdmin && isset($data_packing_order->status) && !in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])) ? '' : 'opacity: 1;' ?>

<section id="packingorder">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute;">
                        <?php echo (isset($data_packing_order->nomor)) ? $flagLabel : 'NEW' ?>
                    </span>
                    <div style="margin-left: 60px; margin-top: 5px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <div class="row">
                <?php require_once("tab_wizard.php") ?>
                <div class="col">
                    <?php if (isset($data_packing_order->status)) : ?>
                        <div class="alert alert-secondary">
                            <i class="zmdi zmdi-info-outline"></i> &nbsp;
                            <?php echo (isset($data_packing_order->status)) ? $controller->get_status($data_packing_order->status) : '' ?>
                        </div>
                        <div class="clear-sm"></div>
                    <?php endif; ?>

                    <div class="approval-history">
                        <div class="current-status">
                            <p>
                                <strong>PO Nomor</strong>
                                <span>: <?php echo (isset($data_packing_order->nomor)) ? $data_packing_order->nomor : '' ?></span>
                            </p>
                        </div>
                        <?php
                        if (!$is_mobile) {
                            include_once('approval_history.php');
                        } else {
                            include_once('approval_history_mobile.php');
                        };
                        ?>
                    </div>
                    <div class="buttons-container">
                        <div class="row">
                            <div class="col-auto">
                                <a href="<?php echo base_url('packingorder/wizard/2/' . $data_id) ?>" class="btn btn--raised btn-light btn--icon-text page-action-back spinner-action-button <?php echo (!$is_mobile) ? 'btn-custom' : '' ?>">
                                    <?php echo (!$is_mobile) ? 'Back' : '<i class="zmdi zmdi-arrow-left"></i>' ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>