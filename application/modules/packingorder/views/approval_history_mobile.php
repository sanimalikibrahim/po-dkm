<div class="table-responsive">
  <table class="table table-bordered table-condensed">
    <thead>
      <tr>
        <th style="text-align: center;" width="50">No</th>
        <th style="text-align: center;">Description</th>
      </tr>
    </thead>
    <tbody>
      <?php if (count($data_approval_history) > 0) : ?>
        <?php $no = 1; ?>
        <?php foreach ($data_approval_history as $key => $item) : ?>
          <tr <?php echo ($item->action == 'Reject') ? 'style="background: #ff6b68; color: #f9f9f9;"' : '' ?>>
            <td valign="top" align="center"><?php echo $no++ ?></td>
            <td valign="top">
              <?php echo $item->nama_lengkap ?>
              <?php echo $item->description ?>
              <br />
              <small>
                <i class="zmdi zmdi-time"></i>
                <?php echo $item->created_at ?>
              </small>
            </td>
          </tr>
        <?php endforeach; ?>
      <?php else : ?>
        <tr>
          <td colspan="5" style="padding: 15px;">
            No data available in table
          </td>
        </tr>
      <?php endif; ?>
    </tbody>
  </table>
</div>