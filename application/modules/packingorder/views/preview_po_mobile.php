<?php require_once('main.css.php') ?>

<div class="preview-po">
  <div class="preview-body">
    <table class="table-body" style="width: 100%; margin-bottom: 20px;">
      <tr>
        <td valign="top" class="th" width="130">Tanggal</td>
        <td valign="top" class="td"><?php echo $controller->format_date((isset($data_packing_order->tanggal)) ? $data_packing_order->tanggal : '') ?></td>
      </tr>
      <tr>
        <td valign="top" class="th">PO No.</td>
        <td valign="top" class="td"><?php echo (isset($data_packing_order->nomor)) ? $data_packing_order->nomor : '' ?></td>
      </tr>
      <tr>
        <td valign="top" class="th">Term Of Delivery</td>
        <td valign="top" class="td"><?php echo $controller->format_date((isset($data_packing_order->term_of_delivery)) ? $data_packing_order->term_of_delivery : '') ?></td>
      </tr>
      <tr>
        <td valign="top" class="th">Term Of Payment</td>
        <td valign="top" class="td">
          <?php echo (isset($data_packing_order->term_of_payment)) ? $data_packing_order->term_of_payment : '' ?> <?php echo (isset($data_packing_order->term_of_payment_satuan)) ? $data_packing_order->term_of_payment_satuan : '' ?> <br />
          Setelah invoice diterima.
        </td>
      </tr>
    </table>
    <table class="table-body" style="width: 100%; margin-bottom: 20px;">
      <tr>
        <td valign="top" class="th" width="130">Nama Supplier</td>
        <td valign="top" align="center" class="td"><?php echo (isset($data_packing_order->supplier_nama)) ? $data_packing_order->supplier_nama : '' ?></td>
      </tr>
      <tr>
        <td valign="top" class="th">Alamat Supplier</td>
        <td valign="top" class="td"><?php echo (isset($data_packing_order->supplier_alamat)) ? $data_packing_order->supplier_alamat : '' ?></td>
      </tr>
      <tr>
        <td valign="top" class="th">
          Contact Person <br />
          Telpon No. / Fax
        </td>
        <td valign="top" class="td">
          <?php echo (isset($data_packing_order->supplier_nama_kontak)) ? $data_packing_order->supplier_nama_kontak : '' ?> <br />
          <?php echo (isset($data_packing_order->supplier_telepon)) ? $data_packing_order->supplier_telepon : '' ?>
          <?php echo (isset($data_packing_order->supplier_fax) && (!empty($data_packing_order->supplier_fax))) ? ' / ' . $data_packing_order->supplier_fax : '' ?>
        </td>
      </tr>
    </table>
    <table class="table-order-item" style="margin-bottom: 0px;">
      <thead>
        <tr>
          <th width="50">NO.</th>
          <th>DESCRIPTION OF GOOD</th>
          <th>QTY</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $sub_total = 0;
        $ppn = 0;
        $total = 0;
        ?>
        <?php if (isset($data_order_item) && count($data_order_item) > 0) : ?>
          <?php $no = 1 ?>
          <?php foreach ($data_order_item as $key => $item) : ?>
            <tr>
              <td valign="top" align="center"><?php echo $no++ ?></td>
              <td valign="top">
                <?php echo $item->description ?>
                <?php echo (!empty(trim($item->note))) ? '<br/><br/> Note : </br>' . $item->note : '' ?>
                <hr style="margin-top: 8px; margin-bottom: 8px;" />
                <div style="display: flex;">
                  <div style="width: 65px; font-weight: bold;">UNIT PRICE</div>
                  <span>: Rp <?php echo number_format($item->unit_price) ?></span>
                </div>
                <div style="display: flex;">
                  <div style="width: 65px; font-weight: bold;">AMOUNT</div>
                  <span>: Rp <?php echo number_format($item->amount) ?></span>
                </div>
              </td>
              <td valign="top" align="center" width="100">
                <?php echo $item->quantity ?>
                <?php echo $item->quantity_unit ?>
              </td>
            </tr>
            <?php $sub_total = $sub_total + $item->amount ?>
          <?php endforeach; ?>
          <?php
          $ppn = $sub_total * 0.11;
          $total = ($data_packing_order->ppn == 'Yes') ? $sub_total + $ppn : $sub_total;
          ?>
        <?php else : ?>
          <tr>
            <td colspan="3" style="padding: 15px;">
              No data available in table
            </td>
          </tr>
        <?php endif; ?>
      </tbody>
      <tfoot>
        <tr>
          <th colspan="1" class="no-border">&nbsp;</th>
          <td colspan="2">
            <div style="display: flex;">
              <div style="width: 65px; font-weight: bold;">SUB TOTAL</div>
              <strong>: Rp <?php echo number_format($sub_total) ?></strong>
            </div>
          </td>
        </tr>
        <?php if ($data_packing_order->ppn == 'Yes') : ?>
          <tr>
            <th colspan="1" class="no-border">&nbsp;</th>
            <td colspan="2">
              <div style="display: flex;">
                <div style="width: 65px; font-weight: bold;">PPN 11%</div>
                <strong>: Rp <?php echo number_format($ppn) ?></strong>
              </div>
            </td>
          </tr>
        <?php endif; ?>
        <tr>
          <th colspan="1" class="no-border">&nbsp;</th>
          <td colspan="2">
            <div style="display: flex;">
              <div style="width: 65px; font-weight: bold;">TOTAL</div>
              <strong>: Rp <?php echo number_format($total) ?></strong>
            </div>
          </td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>