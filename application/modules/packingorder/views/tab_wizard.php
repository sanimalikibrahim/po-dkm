<!-- Temporary -->
<input type="hidden" id="data-id" value="<?php echo $data_id ?>" />

<?php if (!$is_mobile) : ?>
  <div class="col-auto d-none d-sm-block">
    <div class="tab-wizard">
      <?php if (strtolower($this->session->userdata('user')['role']) == 'administrasi') : ?>
        <div class="tab-wizard-badge <?php echo ($tab_wizard == 0) ? 'tab-wizard-badge-active' : '' ?>">
          <label>Packing Order</label>
        </div>
        <div class="tab-wizard-line <?php echo ($tab_wizard == 0) ? 'tab-wizard-line-active' : '' ?>"></div>

        <div class="tab-wizard-badge <?php echo ($tab_wizard == 1) ? 'tab-wizard-badge-active' : '' ?>">
          <label>Input Order Item</label>
        </div>
        <div class="tab-wizard-line <?php echo ($tab_wizard == 1) ? 'tab-wizard-line-active' : '' ?>"></div>
      <?php endif; ?>

      <div class="tab-wizard-badge <?php echo ($tab_wizard == 2) ? 'tab-wizard-badge-active' : '' ?>">
        <label>Preview</label>
      </div>
      <div class="tab-wizard-line <?php echo ($tab_wizard == 2) ? 'tab-wizard-line-active' : '' ?>"></div>

      <div class="tab-wizard-badge <?php echo ($tab_wizard == 3) ? 'tab-wizard-badge-active' : '' ?>">
        <label>Waiting For Approval</label>
      </div>
      <div class="tab-wizard-line <?php echo ($tab_wizard == 3) ? 'tab-wizard-line-active' : '' ?>"></div>

      <div class="tab-wizard-badge <?php echo ($tab_wizard == 3) ? 'tab-wizard-badge-active' : '' ?>">
        <label>Approval History</label>
      </div>
      <div class="tab-wizard-line <?php echo ($tab_wizard == 3) ? 'tab-wizard-line-active' : '' ?>"></div>

      <div class="tab-wizard-badge <?php echo (isset($data_packing_order->status) && $data_packing_order->status == 13) ? 'tab-wizard-badge-finish' : '' ?>">
        <label>Finish</label>
      </div>
    </div>
  </div>
<?php endif; ?>