<?php require_once("main.css.php") ?>

<section id="packingorder">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action row">
                <div class="buttons col-md-4">
                    <div class="input-group mb-0">
                        <div class="input-group-prepend">
                            <label class="input-group-text" style="height: 34.13px; background: #f2f2f2;">Bulan / Tahun</label>
                        </div>
                        <select class="custom-select filter-month" style="height: 34.13px;">
                            <option value="01" <?php echo (date('m') == '01') ? 'selected="selected"' : '' ?>>Januari</option>
                            <option value="02" <?php echo (date('m') == '02') ? 'selected="selected"' : '' ?>>Februari</option>
                            <option value="03" <?php echo (date('m') == '03') ? 'selected="selected"' : '' ?>>Maret</option>
                            <option value="04" <?php echo (date('m') == '04') ? 'selected="selected"' : '' ?>>April</option>
                            <option value="05" <?php echo (date('m') == '05') ? 'selected="selected"' : '' ?>>Mei</option>
                            <option value="06" <?php echo (date('m') == '06') ? 'selected="selected"' : '' ?>>Juni</option>
                            <option value="07" <?php echo (date('m') == '07') ? 'selected="selected"' : '' ?>>Juli</option>
                            <option value="08" <?php echo (date('m') == '08') ? 'selected="selected"' : '' ?>>Agustus</option>
                            <option value="09" <?php echo (date('m') == '09') ? 'selected="selected"' : '' ?>>September</option>
                            <option value="10" <?php echo (date('m') == '10') ? 'selected="selected"' : '' ?>>Oktober</option>
                            <option value="11" <?php echo (date('m') == '11') ? 'selected="selected"' : '' ?>>November</option>
                            <option value="12" <?php echo (date('m') == '12') ? 'selected="selected"' : '' ?>>Desember</option>
                        </select>
                        <select class="custom-select filter-year" style="height: 34.13px;">
                            <?php
                            $firstYear = (int) date('Y');
                            $lastYear = $firstYear - 4;
                            for ($i = $firstYear; $i >= $lastYear; $i--) {
                                echo '<option value=' . $i . '>' . $i . '</option>';
                            };
                            ?>
                        </select>
                        <div class="input-group-apend">
                            <button class="btn btn--raised btn-primary btn--icon-text page-action-filter" style="height: 34.13px;">
                                <i class="zmdi zmdi-filter-list"></i> Filter
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table id="table-approved" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Tanggal</th>
                            <th>Nomor</th>
                            <th>Nama Supplier</th>
                            <th>Contact Person</th>
                            <th>Amount (IDR)</th>
                            <th>PPN (IDR)</th>
                            <th width="100">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>