<?php require_once("main.css.php") ?>

<?php $isAdmin = (strtolower($this->session->userdata('user')['role']) == 'administrasi') ? true : false ?>
<?php $flagLabel = ($isAdmin && isset($data_packing_order->status) && in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])) ? 'EDIT' : 'VIEW' ?>
<?php $isReadonly = ($isAdmin && isset($data_packing_order->status) && !in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])) ? 'disabled="true"' : '' ?>
<?php $isReadonly_opacity = ($isAdmin && isset($data_packing_order->status) && !in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])) ? '' : 'opacity: 1;' ?>

<section id="packingorder">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute;">
                        <?php echo (isset($data_packing_order->nomor)) ? $flagLabel : 'NEW' ?>
                    </span>
                    <div style="margin-left: 60px; margin-top: 5px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <div class="row">
                <?php require_once("tab_wizard.php") ?>
                <div class="col">
                    <?php if (isset($data_packing_order->status)) : ?>
                        <div class="alert alert-secondary">
                            <i class="zmdi zmdi-info-outline"></i> &nbsp;
                            <?php echo (isset($data_packing_order->status)) ? $controller->get_status($data_packing_order->status) : '' ?>
                        </div>
                        <div class="clear-sm"></div>
                    <?php endif; ?>

                    <form id="form-packingorder" enctype="multipart/form-data">

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label required>Nomor</label>
                                    <input type="text" name="nomor" class="form-control po-nomor" placeholder="Nomor" value="<?php echo (isset($data_packing_order->nomor)) ? $data_packing_order->nomor : $data_auto_nomor ?>" <?php echo $isReadonly ?> />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label required>Supplier</label>
                                    <div class="select">
                                        <select name="supplier_id" class="form-control select2 po-supllier_id" data-placeholder="Select a supplier" <?php echo $isReadonly ?>>
                                            <?php
                                            if (count($data_supplier) > 0) {
                                                foreach ($data_supplier as $key => $item) {
                                                    $isSelected = ($item->id == $data_packing_order->supplier_id) ? 'selected' : '';
                                                    echo '<option value="' . $item->id . '" ' . $isSelected . '>' . $item->nama . '</option>';
                                                };
                                            };
                                            ?>
                                        </select>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label required>Tanggal</label>
                                    <input type="text" name="tanggal" class="form-control flatpickr-date po-tanggal" placeholder="Tanggal" value="<?php echo (isset($data_packing_order->tanggal)) ? $data_packing_order->tanggal : '' ?>" style="<?php echo $isReadonly_opacity ?>" <?php echo $isReadonly ?> />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label required>Term Of Delivery</label>
                                    <input type="text" name="term_of_delivery" class="form-control flatpickr-date po-term_of_delivery" placeholder="Term Of Delivery" value="<?php echo (isset($data_packing_order->term_of_delivery)) ? $data_packing_order->term_of_delivery : '' ?>" style="<?php echo $isReadonly_opacity ?>" <?php echo $isReadonly ?> />
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label required>Term Of Payment</label>
                                            <input type="text" name="term_of_payment" class="form-control mask-number po-term_of_payment" placeholder="Term Of Payment" value="<?php echo (isset($data_packing_order->term_of_payment)) ? $data_packing_order->term_of_payment : '' ?>" <?php echo $isReadonly ?> />
                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <div class="select">
                                                <select name="term_of_payment_satuan" class="form-control po-term_of_payment_satuan" data-placeholder="Select a unit" style="width: 150px;" <?php echo $isReadonly ?>>
                                                    <?php
                                                    $top_satuan = array(
                                                        'Hari' => 'Hari',
                                                        'Bulan' => 'Bulan',
                                                        'Tahun' => 'Tahun'
                                                    );
                                                    foreach ($top_satuan as $key => $item) {
                                                        $isSelected = ($key == $data_packing_order->term_of_payment_satuan) ? 'selected' : '';
                                                        echo '<option value="' . $key . '" ' . $isSelected . '>' . $item . '</option>';
                                                    };
                                                    ?>
                                                </select>
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" style="border-bottom: 1px solid #eceff1; padding-bottom: 10px;">
                            <label required>Pajak Pertambahan Nilai (PPN)</label>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <?php if (empty($isReadonly)) : ?>
                                    <?php
                                    $ppn_class_1 = (isset($data_packing_order->ppn) && $data_packing_order->ppn == 'Yes') ? 'active' : '';
                                    $ppn_class_2 = (isset($data_packing_order->ppn) && $data_packing_order->ppn == 'No') ? 'active' : '';
                                    $ppn_checked_1 = (isset($data_packing_order->ppn) && $data_packing_order->ppn == 'Yes') ? 'checked' : '';
                                    $ppn_checked_2 = (isset($data_packing_order->ppn) && $data_packing_order->ppn == 'No') ? 'checked' : '';
                                    ?>
                                    <label class="btn option-ppn <?php echo $ppn_class_1 ?>">
                                        <input type="radio" name="ppn" value="Yes" class="po-ppn" autocomplete="off" <?php echo $ppn_checked_1 ?>> Yes
                                    </label>
                                    <label class="btn option-ppn <?php echo $ppn_class_2 ?>">
                                        <input type="radio" name="ppn" value="No" class="po-ppn" autocomplete="off" <?php echo $ppn_checked_2 ?>> No
                                    </label>
                                <?php else : ?>
                                    <label class="btn active">
                                        <input type="radio" checked disabled> <?php echo $data_packing_order->ppn ?>
                                    </label>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group" style="margin-bottom: 0px;">
                                    <label>Attachment</label>
                                    <a href="javascript:;" data-toggle="popover" data-trigger="focus" data-content="Supported format: JPG, PNG & PDF">
                                        <span class="zmdi zmdi-help"></span>
                                    </a>
                                    <?php if ($isAdmin && (!isset($data_packing_order->status) || (in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])))) : ?>
                                        <a href="javascript:;" class="btn btn-sm btn-success btn--raised attachment-add" style="margin-left: 10px;">
                                            <i class="zmdi zmdi-file-plus"></i> Add File
                                        </a>
                                        <div class="attachment-input-wrapper"></div>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <div class="attachment">
                                        <?php if (count($data_attachment) > 0) : ?>
                                            <?php foreach ($data_attachment as $key => $item) : ?>
                                                <?php $isImage = (strpos($item->file_type, 'image') !== false) ? true : false ?>
                                                <div class="attachment-item attachment-data-item-<?php echo $item->id ?>">
                                                    <div class="row">
                                                        <div class="col-auto">
                                                            <?php if ($isImage) : ?>
                                                                <img src="<?php echo base_url($item->file_name_thumb) ?>" class="attachment-preview attachment-preview-img" />
                                                            <?php else : ?>
                                                                <div class="attachment-preview attachment-preview-file">
                                                                    <i class="zmdi zmdi-file-text"></i>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col" style="padding-left: 0;">
                                                            <div style="<?php echo ($is_mobile) ? 'text-overflow: ellipsis; overflow: hidden; width: 200px; height: 1.2em; white-space: nowrap;' : '' ?>">
                                                                <a href="<?php echo base_url($item->file_name) ?>" target="_blank">
                                                                    <span><?php echo $item->file_raw_name ?></span>
                                                                </a>
                                                            </div>
                                                            <?php if ($isAdmin && (!isset($data_packing_order->status) || (in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])))) : ?>
                                                                <a href="javascript:;" class="link-black attachment-delete-existing" data-id="<?php echo $item->id ?>">
                                                                    <small><i class="zmdi zmdi-close-circle"></i> Delete</small>
                                                                </a>
                                                            <?php endif; ?>
                                                            <span class="attachment-size">
                                                                <?php if ($isAdmin && (!isset($data_packing_order->status) || (in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])))) : ?>
                                                                    <i class="zmdi zmdi-minus"></i>
                                                                <?php endif; ?>
                                                                <?php echo $item->file_size ?> KB
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php else : ?>
                                            <label>No data available</label>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <small class="form-text text-muted">
                            Fields with red stars (<label required></label>) are required.
                        </small>

                        <div class="row">
                            <div class="col">
                                <div class="buttons-container">
                                    <div class="row">
                                        <div class="col text-right">
                                            <?php if ($isAdmin && (!isset($data_packing_order->status) || (in_array((int) $data_packing_order->status, [0, 7, 8, 9, 10, 11, 12])))) : ?>
                                                <button class="btn btn--raised btn-primary btn--icon-text btn-custom packingorder-action-save spinner-action-button">
                                                    Save & Next
                                                    <i class="zmdi zmdi-long-arrow-right"></i>
                                                </button>
                                            <?php else : ?>
                                                <a href="<?php echo base_url('packingorder/wizard/1/' . $data_id) ?>" class="btn btn--raised btn-primary btn--icon-text btn-custom spinner-action-button">
                                                    Next
                                                    <i class="zmdi zmdi-long-arrow-right"></i>
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</section>