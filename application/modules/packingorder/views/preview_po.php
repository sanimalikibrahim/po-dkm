<?php require_once('main.css.php') ?>

<div class="preview-po">
  <div class="preview-header">
    <table class="table-header">
      <tr>
        <td style="width: 80px;">
          <img src="<?php echo base_url('themes/_public/img/logo/dkm-black.png') ?>" class="logo" />
        </td>
        <td style="padding-left: 20px; padding-right: 20px;">
          <div class="kop-surat">
            <h5>PT. DHARMA KARYATAMA MULIA</h5>
            <span>Jl. Raya Bogor Km 29, Gandaria, Ps. Rebo - Jakarta 13710, Indonesia</span> <br />
            <span>Phone : +62 21 8719964-5, Fax : +62 21 8727018</span> <br />
            <span>Email : pt_dkm@dkm.co.id, Web : www.dkm.co.id</span>
          </div>
        </td>
        <td style="width: 80px; text-align: right;">
          <img src="<?php echo base_url('themes/_public/img/logo/sics.png') ?>" class="logo" />
        </td>
      </tr>
    </table>
    <hr class="double-line">
  </div>
  <div class="preview-body">
    <table style="width: 100%; margin-bottom: 20px;">
      <tr>
        <td>&nbsp;</td>
        <td style="border: 1px solid #8c8b8b; width: 262px; padding: 4px 8px; text-align: left; font-size: 11px;">
          FM-PCH-01-01 <br>
          Revisi-1-
        </td>
      </tr>
    </table>
    <div style="text-align: center; margin-bottom: 20px;">
      <h5>PURCHASE ORDER</h5>
    </div>
    <table style="width: 100%; margin-bottom: 20px;">
      <tr>
        <td valign="top">
          <table class="table-body">
            <tr>
              <td valign="top" class="th">Nama Supplier</td>
              <td valign="top" align="center" class="td"><?php echo (isset($data_packing_order->supplier_nama)) ? $data_packing_order->supplier_nama : '' ?></td>
            </tr>
            <tr>
              <td valign="top" class="th">Alamat Supplier</td>
              <td valign="top" class="td"><?php echo (isset($data_packing_order->supplier_alamat)) ? $data_packing_order->supplier_alamat : '' ?></td>
            </tr>
            <tr>
              <td valign="top" class="th">
                Contact Person <br />
                Telpon No. / Fax
              </td>
              <td valign="top" class="td">
                <?php echo (isset($data_packing_order->supplier_nama_kontak)) ? $data_packing_order->supplier_nama_kontak : '' ?> <br />
                <?php echo (isset($data_packing_order->supplier_telepon)) ? $data_packing_order->supplier_telepon : '' ?>
                <?php echo (isset($data_packing_order->supplier_fax) && (!empty($data_packing_order->supplier_fax))) ? ' / ' . $data_packing_order->supplier_fax : '' ?>
              </td>
            </tr>
          </table>
        </td>
        <td valign="top">
          <table class="table-body-right" style="float: right;">
            <tr>
              <td valign="top" class="th">Tanggal</td>
              <td valign="top" class="td">: <?php echo $controller->format_date((isset($data_packing_order->tanggal)) ? $data_packing_order->tanggal : '') ?></td>
            </tr>
            <tr>
              <td valign="top" class="th">PO No.</td>
              <td valign="top" class="td">: <?php echo (isset($data_packing_order->nomor)) ? $data_packing_order->nomor : '' ?></td>
            </tr>
            <tr>
              <td valign="top" class="th">Term Of Delivery</td>
              <td valign="top" class="td">: <?php echo $controller->format_date((isset($data_packing_order->term_of_delivery)) ? $data_packing_order->term_of_delivery : '') ?></td>
            </tr>
            <tr>
              <td valign="top" class="th">Term Of Payment</td>
              <td valign="top" class="td">
                : <?php echo (isset($data_packing_order->term_of_payment)) ? $data_packing_order->term_of_payment : '' ?> <?php echo (isset($data_packing_order->term_of_payment_satuan)) ? $data_packing_order->term_of_payment_satuan : '' ?> <br />
                &nbsp; Setelah invoice diterima.
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table class="table-order-item">
      <thead>
        <tr>
          <th width="50">NO.</th>
          <th>DESCRIPTION OF GOOD</th>
          <th>QTY</th>
          <th>UNIT PRICE</th>
          <th>AMOUNT</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $sub_total = 0;
        $ppn = 0;
        $total = 0;
        ?>
        <?php if (isset($data_order_item) && count($data_order_item) > 0) : ?>
          <?php $no = 1 ?>
          <?php foreach ($data_order_item as $key => $item) : ?>
            <tr>
              <td valign="top" align="center"><?php echo $no++ ?></td>
              <td valign="top">
                <?php echo $item->description ?>
                <?php echo (!empty(trim($item->note))) ? '<br/><br/> Note : </br>' . $item->note : '' ?>
              </td>
              <td valign="top" align="center" width="100">
                <?php echo $item->quantity ?>
                <?php echo $item->quantity_unit ?>
              </td>
              <td valign="top" width="150">
                <span>Rp</span>
                <span class="float-right"><?php echo number_format($item->unit_price) ?></span>
              </td>
              <td valign="top" width="150">
                <span>Rp</span>
                <span class="float-right"><?php echo number_format($item->amount) ?></span>
              </td>
            </tr>
            <?php $sub_total = $sub_total + $item->amount ?>
          <?php endforeach; ?>
          <?php
          $ppn = $sub_total * 0.11;
          $total = ($data_packing_order->ppn == 'Yes') ? $sub_total + $ppn : $sub_total;
          ?>
        <?php else : ?>
          <tr>
            <td colspan="5" style="padding: 15px;">
              No data available in table
            </td>
          </tr>
        <?php endif; ?>
      </tbody>
      <tfoot>
        <tr>
          <th colspan="3" class="no-border">&nbsp;</th>
          <th valign="top" style="text-align: left;">SUB TOTAL</th>
          <td>
            <strong>
              <span>Rp</span>
              <span class="float-right"><?php echo number_format($sub_total) ?></span>
            </strong>
          </td>
        </tr>
        <?php if ($data_packing_order->ppn == 'Yes') : ?>
          <tr>
            <th colspan="3" class="no-border">&nbsp;</th>
            <th valign="top" style="text-align: left;">PPN 11%</th>
            <td>
              <strong>
                <span>Rp</span>
                <span class="float-right"><?php echo number_format($ppn) ?></span>
              </strong>
            </td>
          </tr>
        <?php endif; ?>
        <tr>
          <th colspan="3" class="no-border">&nbsp;</th>
          <th valign="top" style="text-align: left;">TOTAL</th>
          <td>
            <strong>
              <span>Rp</span>
              <span class="float-right"><?php echo number_format($total) ?></span>
            </strong>
          </td>
        </tr>
      </tfoot>
    </table>
    <table class="table-order-item mb-0">
      <thead>
        <tr>
          <th colspan="3">Diajukan</th>
          <th>Diperiksa</th>
          <th colspan="3">Disetujui</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td valign="center" align="center" class="ttd">
            <?php echo (isset($data_ttd->administrasi)) ? $data_ttd->administrasi : '-' ?>
          </td>
          <td valign="center" align="center" class="ttd">
            <?php echo (isset($data_ttd->supervisor)) ? $data_ttd->supervisor : '-' ?>
          </td>
          <td valign="center" align="center" class="ttd">
            <?php echo (isset($data_ttd->manager)) ? $data_ttd->manager : '-' ?>
          </td>
          <td valign="center" align="center" class="ttd">
            <?php echo (isset($data_ttd->finance)) ? $data_ttd->finance : '-' ?>
          </td>
          <td valign="center" align="center" class="ttd">
            <?php echo (isset($data_ttd->g_manager)) ? $data_ttd->g_manager : '-' ?>
          </td>
          <td valign="center" align="center" class="ttd">
            <?php echo (isset($data_ttd->deputy_dir)) ? $data_ttd->deputy_dir : '-' ?>
          </td>
          <td valign="center" align="center" class="ttd">
            <?php echo (isset($data_ttd->direktur)) ? $data_ttd->direktur : '-' ?>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <th align="center">Administrasi</th>
          <th align="center">Supervisor</th>
          <th align="center">Manager</th>
          <th align="center">Finance</th>
          <th align="center">G. Manager</th>
          <th align="center">Deputy. Dir</th>
          <th align="center">Direktur</th>
        </tr>
      </tfoot>
    </table>
    <p style="margin: 0; margin-top: 20px; font-size: 11px;">
      *PO ini tidak memerlukan tanda tangan langsung dikarenakan sudah dilakukan tanda tangan elektronik.
    </p>
  </div>
</div>