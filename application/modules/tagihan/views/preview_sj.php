<?php require_once('main.css.php') ?>

<div class="preview-po">
  <div class="preview-header">
    <div style="text-align: center; margin-bottom: -30px;">
      <u>
        <h4 style="margin-bottom: 0px;">
          S U R A T &nbsp; J A L A N
        </h4>
      </u>
      <span><?= $data_suratjalan->nomor ?></span>
    </div>
    <table class="table-header">
      <tr>
        <td style="padding-right: 20px;">
          <img src="<?php echo base_url('themes/_public/img/logo/dkm-black.png') ?>" class="logo" style="margin-bottom: 10px; margin-left: 12px;" />
          <div class="kop-surat">
            <h5>PT. DHARMA KARYATAMA MULIA</h5>
            <span>Jl. Raya Bogor Km 29, Gandaria, Ps. Rebo - Jakarta 13710, Indonesia</span> <br />
            <span>Phone : +62 21 8719964-5, Fax : +62 21 8727018</span> <br />
            <span>Email : pt_dkm@dkm.co.id, Web : www.dkm.co.id</span>
          </div>
        </td>
        <td style="width: 250px;">
          Kepada :
          <div style="border-bottom: 1px dotted #ccc; text-align: center; padding-top: 4px; padding-bottom: 4px;">
            <?= $data_suratjalan->penerima ?>
          </div>
        </td>
      </tr>
    </table>
  </div>
  <div class="preview-body" style="margin-top: 15px;">
    <table class="table-order-item">
      <thead>
        <tr>
          <th width="50">No</th>
          <th>Nama Barang</th>
          <th>Code</th>
          <th>Qty</th>
          <th>Keterangan</th>
        </tr>
      </thead>
      <tbody>
        <?php if (isset($data_suratjalan_item) && count($data_suratjalan_item) > 0) : ?>
          <?php $no = 1 ?>
          <?php foreach ($data_suratjalan_item as $key => $item) : ?>
            <tr>
              <td valign="top" align="center"><?php echo $no++ ?></td>
              <td valign="top">
                <?php echo $item->nama_barang ?>
              </td>
              <td valign="top">
                <?php echo $item->kode_barang ?>
              </td>
              <td valign="top" align="center" width="150">
                <?php echo $item->quantity . ' ' . $item->quantity_unit ?>
              </td>
              <td valign="top" align="center">
                <?php echo $item->keterangan ?>
              </td>
            </tr>
          <?php endforeach; ?>
        <?php else : ?>
          <tr>
            <td colspan="5" style="padding: 15px;">
              No data available in table
            </td>
          </tr>
        <?php endif; ?>
      </tbody>
    </table>
    <table style="width: 100%; margin-top: 30px; " class="mb-0">
      <tr>
        <td>
          Jakarta, <span style="text-decoration:underline"><?= $controller->format_date($data_suratjalan->tanggal) ?></span> <br>
        </td>
      </tr>
    </table>
  </div>
</div>