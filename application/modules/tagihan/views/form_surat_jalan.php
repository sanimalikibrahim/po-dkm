<style type="text/css">
  .form-control:disabled,
  .form-control[readonly] {
    opacity: 1;
  }
</style>

<div class="modal fade" id="modal-suratjalan" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-xl modal-dialog-centered" style="<?php echo ($is_mobile) ? 'max-width: 98%' : '' ?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Surat Jalan</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <div class="suratjalan-form">
          <form id="form-suratjalan">
            <!-- Hidden field -->
            <input type="hidden" name="permintaan_barang_id" class="suratjalan-permintaan_barang_id" placeholder="Permintaan Barang ID" readonly />
            <input type="hidden" name="pengiriman_ke" class="suratjalan-pengiriman_ke" placeholder="Pengiriman Ke" readonly />
            <!-- END ## Hidden field -->
            <div class="form-group">
              <label required>Penerima</label>
              <input type="text" name="penerima" class="form-control suratjalan-penerima" placeholder="Penerima" readonly required>
              <i class="form-group__bar"></i>
            </div>
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label required>Nomor</label>
                  <input type="text" name="nomor" class="form-control suratjalan-nomor" placeholder="Nomor" required>
                  <i class="form-group__bar"></i>
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label required>Tanggal</label>
                  <input type="text" name="tanggal" class="form-control suratjalan-tanggal flatpickr-date" placeholder="Tanggal" value="<?= date('Y-m-d') ?>" required>
                  <i class="form-group__bar"></i>
                </div>
              </div>
            </div>
            <!-- Table item -->
            <table id="table-suratjalan_item" class="table table-bordered table-sm table-condensed mb-0">
              <thead>
                <tr class="bg-light">
                  <th width="50">No</th>
                  <th>Nama Barang</th>
                  <th>Code</th>
                  <th>Permintaan</th>
                  <th>Belum Dikrim</th>
                  <th>Akan Dikirim</th>
                  <th>Keterangan</th>
                </tr>
              </thead>
              <tbody class="suratjalan-list"></tbody>
            </table>
            <!-- END ## Table item -->
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <div class="btn-group" role="group" aria-label="Basic example">
          <button class="btn btn-success btn--icon-text suratjalan-action-save">
            <i class="zmdi zmdi-check-circle"></i> Save
          </button>
        </div>
        <button type="button" class="btn btn-light btn--icon-text suratjalan-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>