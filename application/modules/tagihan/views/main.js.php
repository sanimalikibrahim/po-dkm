<script type="text/javascript">
  $(document).ready(function() {
    var _key = "";
    var _section = "suratjalan";
    var _table_list = "table-list";
    var _modal_view_surat_jalan = "modal-suratjalan_view";

    // Initialize DataTables : List
    if ($("#" + _table_list)[0]) {
      var table_aprovalresult = $("#" + _table_list).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('tagihan/ajax_get_list/' . ($this->input->get('ref') !== null ? '?ref=' . $this->input->get('ref') : '')) ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "dari"
          },
          {
            data: "penerima"
          },
          {
            data: "tanggal"
          },
          {
            data: "item_count",
            render: function(data, type, row, meta) {
              return `${meta.settings.fnFormatNumber(data)} Item (Quantity: ${row.total_info.total_permintaan})`;
            }
          },
          {
            data: "created_at"
          },
          {
            data: "status",
            render: function(data, type, row, meta) {
              var role = "<?= strtolower($this->session->userdata('user')['role']) ?>";
              var totalInfo = row.total_info;
              var suratJalan = row.surat_jalan;
              var htmlDom = "";
              var htmlDomEmpty = "<i>Belum ada surat jalan</i>";

              if (suratJalan.length > 0) {
                suratJalan.map((item, index) => {
                  var buttonStatus = "";

                  switch (item.status_pembayaran) {
                    case "1":
                      borderColor = "border-success";
                      textColor = "";
                      break;
                    default:
                      borderColor = "border-secondary";
                      textColor = "";
                      break;
                  };

                  if (item.status_pembayaran === "0" && role === "admin billing") {
                    buttonStatus = `<a href="javascript:;" class="btn btn-danger btn-sm suratjalan-action-status" data-status="1" data-surat-jalan="${item.id}">Sudah Bayar?</a>`;
                  };

                  htmlDom += `
                    <div class="p-2 border ${borderColor} ${textColor} rounded mb-2">
                      <b>Nomor</b>: ${item.nomor} <hr class="mt-1 mb-1"/>
                      <b>Tanggal</b>: ${item.tanggal} <hr class="mt-1 mb-1"/>
                      <b>Dikirim</b>: ${item.total_dikirim} <hr class="mt-1 mb-1"/>
                      <b>Status</b>: ${getStatus(item.status_pembayaran)} <hr class="mt-1 mb-1"/>
                      <a href="#" class="btn btn-secondary btn-sm suratjalan-action-view" data-surat-jalan="${item.id}" data-toggle="modal" data-target="#${_modal_view_surat_jalan}">
                        <i class="zmdi zmdi-eye"></i> View
                      </a>
                      ${buttonStatus}
                    </div>
                  `;
                });
              };

              return (htmlDom !== "") ? htmlDom : htmlDomEmpty;
            }
          },
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_list).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle view surat
    $(document).on("click", ".suratjalan-action-view", function(e) {
      e.preventDefault();
      const suratJalan = $(this).attr("data-surat-jalan");

      $("#" + _modal_view_surat_jalan + " .suratjalan-preview").html("Please wait...");

      $.ajax({
        type: "get",
        url: `<?php echo base_url('tagihan/ajax_get_preview/') ?>${suratJalan}/?readonly=true`,
        success: function(response) {
          $("#" + _modal_view_surat_jalan + " .suratjalan-preview").html(response);
        }
      });
    });

    // Handle change status
    $(document).on("click", ".suratjalan-action-status", function(e) {
      e.preventDefault();

      const suratJalan = $(this).attr("data-surat-jalan");
      const status = $(this).attr("data-status");
      var statusLabel = (status == "1") ? "Sudah Bayar" : "Undefined";

      swal({
        title: `Ubah status pembayaran menjadi "${statusLabel}" ?`,
        text: "Once changed, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "GET",
            url: `<?php echo base_url('tagihan/ajax_set_status/') ?>${suratJalan}/${status}`,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_list).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });
  });

  function getStatus(status) {
    switch (status.toString()) {
      case '0':
        return 'Belum Bayar';
        break;
      case '1':
        return 'Sudah Bayar';
        break;
      default:
        return 'Undefined';
        break;
    };
  };

  function randomString(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;

    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    };

    return result;
  };
</script>