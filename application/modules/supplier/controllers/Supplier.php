<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Supplier extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model(['AppModel', 'SupplierModel', 'SupplierbarangModel']);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('supplier'),
      'card_title' => 'Supplier'
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_getAll()
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'supplier',
      'order_column' => 4,
      'order_column_dir' => 'desc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SupplierModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->SupplierModel->insert());
      } else {
        echo json_encode($this->SupplierModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_import()
  {
    $this->handle_ajax_request();

    try {
      $this->file = json_decode(json_encode($_FILES['file']));

      if (isset($this->file) && !empty($this->file->name)) {
        $inputFileTemp = $this->file->tmp_name;

        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileTemp);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

        $reader->setReadDataOnly(true);

        $spreadsheet = $reader->load($inputFileTemp);
        $activeSheet = $spreadsheet->getActiveSheet();
        $worksheetInfo = $reader->listWorksheetInfo($inputFileTemp);
        $worksheetInfo = $worksheetInfo[0];

        $colStart = 1;
        $rowStart = 3;
        $columnEnd = $worksheetInfo['totalColumns'];
        $rowEnd = $worksheetInfo['totalRows'];

        for ($row = $rowStart; $row <= $rowEnd; $row++) {
          for ($col = $colStart; $col <= $columnEnd; $col++) {
            $worksheetData[$row][$this->SupplierModel->getColumnName($col)] = $activeSheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
            $worksheetData[$row]['created_by'] = $this->session->userdata('user')['id'];
          };
        };

        $worksheetData = array_values($worksheetData);

        echo json_encode($this->SupplierModel->insertBatch($worksheetData));
      } else {
        $errors = '<div>The File field is required.</div>';
        echo json_encode(array('status' => false, 'data' => $errors));
      };
    } catch (\Throwable $th) {
      $errors = '<div>Failed to get information data.</div>';
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->SupplierModel->delete($id));
  }

  public function ajax_generate_kode()
  {
    $this->handle_ajax_request();
    echo json_encode($this->SupplierModel->generateKode());
  }

  // Barang
  public function barang($supplier_id = null)
  {
    $supplier = $this->SupplierModel->getDetail(['id' => $supplier_id]);
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('supplier'),
      'card_title' => 'Supplier › Barang',
      'card_subTitle' => $supplier->nama,
      'data_supplier' => $supplier
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('grid_barang', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_barang($supplier_id)
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'supplier_barang',
      'order_column' => 6,
      'order_column_dir' => 'desc',
      'static_conditional' => array(
        'supplier_id' => $supplier_id,
      )
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save_barang($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SupplierbarangModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->SupplierbarangModel->insert());
      } else {
        echo json_encode($this->SupplierbarangModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_import_barang()
  {
    $this->handle_ajax_request();

    try {
      $this->file = json_decode(json_encode($_FILES['file']));

      if (isset($this->file) && !empty($this->file->name)) {
        $inputFileTemp = $this->file->tmp_name;

        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileTemp);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

        $reader->setReadDataOnly(true);

        $spreadsheet = $reader->load($inputFileTemp);
        $activeSheet = $spreadsheet->getActiveSheet();
        $worksheetInfo = $reader->listWorksheetInfo($inputFileTemp);
        $worksheetInfo = $worksheetInfo[0];

        $colStart = 1;
        $rowStart = 3;
        $columnEnd = $worksheetInfo['totalColumns'];
        $rowEnd = $worksheetInfo['totalRows'];

        // Collect column list from excel
        $columnNameList = $this->SupplierbarangModel->getColumnName();
        $columnNameTemp = [];
        for ($col = $colStart; $col <= $columnEnd; $col++) {
          $columnName = $this->SupplierbarangModel->getColumnName($col);

          if (!is_null($columnName) && !empty($columnName) && $columnName !== 0) {
            $columnNameTemp[] = $columnName;
          };
        };

        // Compare column excel and model, must be same (case-sensitive)
        $columnHasNoError = ($columnNameTemp === $columnNameList) ? true : false;
        $rowValueHasError = [];

        if ($columnHasNoError === true) {
          for ($row = $rowStart; $row <= $rowEnd; $row++) {
            for ($col = $colStart; $col <= $columnEnd; $col++) {
              $columnName = $this->SupplierbarangModel->getColumnName($col);
              $rowValue = $activeSheet->getCellByColumnAndRow($col, $row)->getFormattedValue();

              if ($columnName === 'supplier_id') {
                $supplier = $this->SupplierModel->getDetail(['kode' => html_escape($rowValue)]);
              };

              if (!is_null($columnName) && !empty($columnName) && $columnName !== 0) {
                // Check value is empty when required
                if (empty(trim($rowValue)) && !in_array($rowValue, ['0', '-'])) {
                  $rowValueHasError[] = [
                    'row' => $row,
                    'column' => $col,
                    'value' => $rowValue
                  ];
                } else {
                  // Collect data
                  if (!is_null($supplier)) {
                    $worksheetData[$row][$columnName] = $rowValue;
                    $worksheetData[$row]['supplier_id'] = $supplier->id;
                    $worksheetData[$row]['created_by'] = $this->session->userdata('user')['id'];
                  };
                };
              };
            };
          };

          if (count($rowValueHasError) === 0) {
            $worksheetData = array_values($worksheetData);

            echo json_encode($this->SupplierbarangModel->insertBatch($worksheetData));
          } else {
            $errors = '<div>Please fix the following errors :';
            $errors .= '<ul style="margin-top: 5px; margin-left: -15px;">';

            foreach ($rowValueHasError as $index => $item) {
              $errors .= '<li>Row: ' . $item['row'] . ', col: ' . $item['column'] . ' cannot be empty.</li>';
            };

            $errors .= '</ul>';
            $errors .= '</div>';
            echo json_encode(array('status' => false, 'data' => $errors));
          };
        } else {
          $errors = '<div>The uploaded file does not match with template.</div>';
          echo json_encode(array('status' => false, 'data' => $errors));
        };
      } else {
        $errors = '<div>The File field is required.</div>';
        echo json_encode(array('status' => false, 'data' => $errors));
      };
    } catch (\Throwable $th) {
      $errors = '<div>Failed to get information data.</div>';
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete_barang($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->SupplierbarangModel->delete($id));
  }
  // END ## Barang
}
