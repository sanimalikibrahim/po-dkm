<div class="modal fade" id="modal-form-supplier" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Supplier</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-supplier">

          <div class="form-group">
            <label required>Nama</label>
            <input type="text" name="nama" class="form-control supplier-nama" placeholder="Nama" required>
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group" style="margin-bottom: 3rem;">
            <label required>Alamat</label>
            <textarea name="alamat" class="form-control textarea-autosize text-counter supplier-alamat" rows="1" data-max-length="300" placeholder="Alamat" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"></textarea>
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label required>Nama Kontak</label>
            <input type="text" name="nama_kontak" class="form-control supplier-nama_kontak" placeholder="Nama Kontak">
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label>Telepon</label>
            <input type="text" name="telepon" class="form-control mask-number supplier-telepon" placeholder="Telepon">
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label>Fax</label>
            <input type="text" name="fax" class="form-control mask-number supplier-fax" placeholder="Fax">
            <i class="form-group__bar"></i>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text supplier-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text supplier-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>