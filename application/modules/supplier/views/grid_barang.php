<section id="supplier">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action">
                <div class="buttons">
                    <a href="<?php echo base_url('supplier') ?>" class="btn btn-secondary">
                        <i class="zmdi zmdi-arrow-left"></i>
                    </a>
                    <button class="btn btn--raised btn-primary btn--icon-text supplier-barang-action-add" data-toggle="modal" data-target="#modal-form-supplier-barang">
                        <i class="zmdi zmdi-plus"></i> Add New
                    </button>
                </div>
            </div>

            <?php include_once('form_barang.php') ?>

            <div class="table-responsive">
                <input type="hidden" name="supplier_id" id="supplier_id" value="<?php echo $data_supplier->id ?>" readonly />
                <table id="table-supplier-barang" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Departemen</th>
                            <th>Code Part</th>
                            <th>Nama Part</th>
                            <th>Harga</th>
                            <th>Satuan</th>
                            <th>Created At</th>
                            <th width="100">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>