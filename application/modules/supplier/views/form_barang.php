<div class="modal fade" id="modal-form-supplier-barang" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Supplier Barang</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-supplier-barang">

          <input type="hidden" name="supplier_id" class="supplier_barang-supplier_id" readonly />

          <div class="form-group">
            <label required>Departemen</label>
            <input type="text" name="departemen" class="form-control supplier_barang-departemen" placeholder="Departemen" required>
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group">
            <label required>Kode Part</label>
            <input type="text" name="code_part" class="form-control supplier_barang-code_part" maxlength="50" placeholder="Nama Kontak">
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group" style="margin-bottom: 3rem;">
            <label required>Nama Part</label>
            <textarea name="nama_part" class="form-control textarea-autosize text-counter supplier_barang-nama_part" rows="1" data-max-length="200" placeholder="Nama Part" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"></textarea>
            <i class="form-group__bar"></i>
          </div>

          <div class="row">
            <div class="col">
              <div class="form-group">
                <label required>Harga</label>
                <input type="text" name="harga" class="form-control mask-money supplier_barang-harga" placeholder="Harga">
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label required>Satuan</label>
                <input type="text" name="satuan" class="form-control supplier_barang-satuan" placeholder="Satuan" value="PCS">
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text supplier_barang-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text supplier_barang-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>