<section id="supplier">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action">
                <div class="buttons">
                    <button class="btn btn--raised btn-primary btn--icon-text supplier-action-add" data-toggle="modal" data-target="#modal-form-supplier">
                        <i class="zmdi zmdi-plus"></i> Add New
                    </button>
                    <div class="btn-group" role="group">
                        <button id="btn-export" type="button" class="btn btn-success btn--raised dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-cloud-upload"></i> Import
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btn-export">
                            <a class="dropdown-item supplier-action-import" data-toggle="modal" data-target="#modal-form-supplier-import">Supplier</a>
                            <a class="dropdown-item supplier-action-import-barang" data-toggle="modal" data-target="#modal-form-supplier-import-barang">Supplier Barang</a>
                        </div>
                    </div>
                    <button class="btn btn--raised btn-success btn--icon-text supplier-action-gkode">
                        <i class="zmdi zmdi-key"></i> Generate Kode
                    </button>
                </div>
            </div>

            <?php include_once('form.php') ?>
            <?php include_once('import.php') ?>
            <?php include_once('import_barang.php') ?>

            <div class="table-responsive">
                <table id="table-supplier" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Created At</th>
                            <th width="100">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>