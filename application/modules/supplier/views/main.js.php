<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "supplier";
    var _table = "table-supplier";
    var _table_barang = "table-supplier-barang";
    var _modal = "modal-form-supplier";
    var _modal_import = "modal-form-supplier-import";
    var _modal_import_barang = "modal-form-supplier-import-barang";
    var _modal_barang = "modal-form-supplier-barang";
    var _form = "form-supplier";
    var _form_import = "form-supplier-import";
    var _form_import_barang = "form-supplier-import-barang";
    var _form_barang = "form-supplier-barang";
    var _supplier_id = $("#supplier_id").val();

    // Supplier
    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_supplier = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('supplier/ajax_getall/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "kode"
          },
          {
            data: "nama"
          },
          {
            data: "alamat"
          },
          {
            data: "created_at"
          },
          {
            data: null,
            className: "center",
            defaultContent: '<div class="action">' +
              '<a href="javascript:;" class="action-barang btn btn-sm btn-light">Barang</a>&nbsp;' +
              '<a href="javascript:;" class="action-edit" data-toggle="modal" data-target="#' + _modal + '"><i class="zmdi zmdi-edit"></i></a>' +
              '<a href="javascript:;" class="action-delete"><i class="zmdi zmdi-delete"></i></a>' +
              '</div>'
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5]
        }, {
          className: 'tablet',
          targets: [0, 1, 2]
        }, {
          className: 'mobile',
          targets: [0, 2]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data add
    $("#" + _section).on("click", "button.supplier-action-add", function(e) {
      e.preventDefault();
      resetForm();
    });

    // Handle data add import
    $("#" + _section).on("click", "button.supplier-action-import", function(e) {
      e.preventDefault();
      resetForm();
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();
      var temp = table_supplier.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form + " .supplier-nama").val(temp.nama);
      $("#" + _form + " .supplier-alamat").val(temp.alamat.replace(/<br\s*[\/]?>/gi, "\r\n"));
      $("#" + _form + " .supplier-nama_kontak").val(temp.nama_kontak);
      $("#" + _form + " .supplier-telepon").val(temp.telepon);
      $("#" + _form + " .supplier-fax").val(temp.fax);

      // Handle textarea height
      setTimeout(function() {
        $("#" + _form + " .supplier-alamat").height($("#" + _form + " .supplier-alamat")[0].scrollHeight);
      }, 500);
    });

    // Handle redirect to: Supplier Barang
    $("#" + _table).on("click", "a.action-barang", function(e) {
      e.preventDefault();
      var temp = table_supplier.row($(this).closest('tr')).data();

      window.location = "<?php echo base_url('supplier/barang/') ?>" + temp.id;
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_supplier.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('supplier/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data submit
    $("#" + _modal + " .supplier-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('supplier/ajax_save/') ?>" + _key,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data submit import
    $("#" + _modal_import + " .supplier-action-save-import").on("click", function(e) {
      e.preventDefault();

      var form = $("#" + _form_import)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('supplier/ajax_import/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            resetForm();
            $("#" + _modal_import).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      $("#" + _form).trigger("reset");
      $("#" + _form + " .supplier-alamat").css("height", "31px");
      $("#" + _form_import).trigger("reset");
      $('.upload-inline .upload-preview').html("No file chosen");
    };

    // Handle upload
    $(".supplier-file").change(function() {
      readUploadInlineDocURL(this);
    });

    // Handle generate kode
    $("#" + _section).on("click", "button.supplier-action-gkode", function(e) {
      e.preventDefault();
      swal({
        title: "Are you sure to generate kode?",
        text: "Once generate, you will not be able to recover the data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('supplier/ajax_generate_kode/') ?>",
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });
    // END ## Supplier

    // Barang
    // Initialize DataTables: Index
    if ($("#" + _table_barang)[0]) {
      var table_supplier_barang = $("#" + _table_barang).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('supplier/ajax_get_barang/') ?>" + _supplier_id,
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "departemen"
          },
          {
            data: "code_part"
          },
          {
            data: "nama_part"
          },
          {
            data: "harga",
            render: function(data, type, row, meta) {
              return meta.settings.fnFormatNumber(data);
            }
          },
          {
            data: "satuan"
          },
          {
            data: "created_at"
          },
          {
            data: null,
            className: "center",
            defaultContent: '<div class="action">' +
              '<a href="javascript:;" class="action-edit" data-toggle="modal" data-target="#' + _modal_barang + '"><i class="zmdi zmdi-edit"></i></a>' +
              '<a href="javascript:;" class="action-delete"><i class="zmdi zmdi-delete"></i></a>' +
              '</div>'
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          targets: [1],
          visible: false
        }, {
          className: 'desktop',
          targets: [0, 2, 3, 4, 5, 6, 7]
        }, {
          className: 'tablet',
          targets: [0, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
        drawCallback: function(settings) {
          var api = this.api();
          var rows = api.rows({
            page: 'current'
          }).nodes();
          var last = null;

          api.column(1, {
            page: 'current'
          }).data().each(function(group, i) {
            group = (group != null) ? group : "(No departemen)";
            if (last !== group) {
              $(rows).eq(i).before(
                '<tr class="group"><td colspan="11">' + group + '</td></tr>'
              );

              last = group;
            }
          });
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_barang).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data add
    $("#" + _section).on("click", "button.supplier-barang-action-add", function(e) {
      e.preventDefault();
      resetFormBarang();
    });

    // Handle data edit
    $("#" + _table_barang).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetFormBarang();
      var temp = table_supplier_barang.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form_barang + " .supplier_barang-departemen").val(temp.departemen);
      $("#" + _form_barang + " .supplier_barang-code_part").val(temp.code_part);
      $("#" + _form_barang + " .supplier_barang-nama_part").val(temp.nama_part.replace(/<br\s*[\/]?>/gi, "\r\n"));
      $("#" + _form_barang + " .supplier_barang-harga").val(temp.harga).trigger("input");
      $("#" + _form_barang + " .supplier_barang-satuan").val(temp.satuan);

      // Handle textarea height
      setTimeout(function() {
        $("#" + _form_barang + " .supplier_barang-nama_part").height($("#" + _form_barang + " .supplier_barang-nama_part")[0].scrollHeight);
      }, 500);
    });

    // Handle data submit
    $("#" + _modal_barang + " .supplier_barang-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('supplier/ajax_save_barang/') ?>" + _key,
        data: $("#" + _form_barang).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetFormBarang();
            $("#" + _modal_barang).modal("hide");
            $("#" + _table_barang).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table_barang).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_supplier_barang.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('supplier/ajax_delete_barang/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_barang).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data submit import
    $("#" + _modal_import_barang + " .supplier-action-save-import-barang").on("click", function(e) {
      e.preventDefault();

      var form = $("#" + _form_import_barang)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('supplier/ajax_import_barang/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            resetFormBarang();
            $("#" + _modal_import_barang + " .error-panel").hide();
            $("#" + _modal_import_barang).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            // notify(response.data, "danger");
            $("#" + _modal_import_barang + " .error-panel").show();
            $("#" + _modal_import_barang + " .error-panel").html(response.data);
          };
        }
      });
    });

    // Handle upload
    $(".supplier-barang-file").change(function() {
      readUploadInlineDocURL(this);
      $("#" + _modal_import_barang + " .error-panel").hide();
    });

    // Handle data add import
    $("#" + _section).on("click", ".supplier-action-import-barang", function(e) {
      e.preventDefault();
      resetFormBarang();
      $("#" + _modal_import_barang + " .error-panel").hide();
    });

    // Handle form reset
    resetFormBarang = () => {
      _key = "";
      $("#" + _form_import_barang).trigger("reset");
      $("#" + _form_import_barang + " .supplier-barang-file").trigger("change");
      $("#" + _form_barang).trigger("reset");
      $("#" + _form_barang + " .supplier_barang-supplier_id").val(_supplier_id);
      $("#" + _form_barang + " .supplier_barang-nama_part").css("height", "31px");
      $("#" + _modal_import_barang + " .error-panel").hide();
    };
    // END ## Barang

  });
</script>