<section id="evaluasi_vendor">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action">
                <div class="buttons">
                    <button class="btn btn--raised btn-primary btn--icon-text evaluasi_vendor-action-add" data-toggle="modal" data-target="#modal-form-evaluasi_vendor">
                        <i class="zmdi zmdi-plus"></i> Add New
                    </button>
                </div>
            </div>

            <?php include_once('form.php') ?>

            <div class="table-responsive">
                <table id="table-evaluasi_vendor" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Supplier ID</th>
                            <th>Supplier</th>
                            <th>Harga</th>
                            <th>Kualitas</th>
                            <th>Ketepatan Waktu</th>
                            <th>Cara Pembayaran</th>
                            <th>Lain-lain</th>
                            <th>Created At</th>
                            <th width="100">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>