<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "evaluasi_vendor";
    var _table = "table-evaluasi_vendor";
    var _modal = "modal-form-evaluasi_vendor";
    var _form = "form-evaluasi_vendor";

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_evaluasi_vendor = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('evaluasivendor/ajax_get_all/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "supplier_id"
          },
          {
            data: "supplier_nama"
          },
          {
            data: "harga",
            render: function(data, type, row, meta) {
              return meta.settings.fnFormatNumber(data);
            }
          },
          {
            data: "kualitas_barang"
          },
          {
            data: "waktu_pengiriman"
          },
          {
            data: "cara_pembayaran"
          },
          {
            data: "lain_lain"
          },
          {
            data: "created_at"
          },
          {
            data: null,
            className: "center",
            defaultContent: '<div class="action">' +
              '<a href="javascript:;" class="action-edit" data-toggle="modal" data-target="#' + _modal + '"><i class="zmdi zmdi-edit"></i></a>' +
              '<a href="javascript:;" class="action-delete"><i class="zmdi zmdi-delete"></i></a>' +
              '</div>'
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [1];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          targets: [1],
          visible: false,
          searchable: false
        }, {
          className: 'desktop',
          targets: [0, 2, 3, 4, 5, 6, 7, 8, 9]
        }, {
          className: 'tablet',
          targets: [0, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 2]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data add
    $("#" + _section).on("click", "button.evaluasi_vendor-action-add", function(e) {
      e.preventDefault();
      resetForm();
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();
      var temp = table_evaluasi_vendor.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form + " .evaluasi_vendor-supplier_id").val(temp.supplier_id).trigger('change');
      $("#" + _form + " .evaluasi_vendor-harga").val(temp.harga).trigger("input");
      $("#" + _form + " .evaluasi_vendor-kualitas_barang").val(temp.kualitas_barang.replace(/<br\s*[\/]?>/gi, "\r\n"));
      $("#" + _form + " .evaluasi_vendor-waktu_pengiriman").val(temp.waktu_pengiriman.replace(/<br\s*[\/]?>/gi, "\r\n"));
      $("#" + _form + " .evaluasi_vendor-cara_pembayaran").val(temp.cara_pembayaran.replace(/<br\s*[\/]?>/gi, "\r\n"));
      $("#" + _form + " .evaluasi_vendor-lain_lain").val(temp.lain_lain.replace(/<br\s*[\/]?>/gi, "\r\n"));

      // Handle textarea height
      setTimeout(function() {
        $("#" + _form + " .evaluasi_vendor-kualitas_barang").height($("#" + _form + " .evaluasi_vendor-kualitas_barang")[0].scrollHeight);
        $("#" + _form + " .evaluasi_vendor-waktu_pengiriman").height($("#" + _form + " .evaluasi_vendor-waktu_pengiriman")[0].scrollHeight);
        $("#" + _form + " .evaluasi_vendor-cara_pembayaran").height($("#" + _form + " .evaluasi_vendor-cara_pembayaran")[0].scrollHeight);
        $("#" + _form + " .evaluasi_vendor-lain_lain").height($("#" + _form + " .evaluasi_vendor-lain_lain")[0].scrollHeight);
      }, 500);
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_evaluasi_vendor.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('evaluasivendor/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data submit
    $("#" + _modal + " .evaluasi_vendor-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('evaluasivendor/ajax_save/') ?>" + _key,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      $("#" + _form).trigger("reset");
      $("#" + _form + " .evaluasi_vendor-kualitas_barang").css("height", "31px");
      $("#" + _form + " .evaluasi_vendor-waktu_pengiriman").css("height", "31px");
      $("#" + _form + " .evaluasi_vendor-cara_pembayaran").css("height", "31px");
      $("#" + _form + " .evaluasi_vendor-lain_lain").css("height", "31px");
      $("#" + _form + " .evaluasi_vendor-supplier_id").val("").trigger('change');
    };

  });
</script>