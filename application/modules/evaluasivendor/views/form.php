<div class="modal fade" id="modal-form-evaluasi_vendor" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Evaluasi Vendor</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-evaluasi_vendor">

          <div class="row">
            <div class="col">
              <div class="form-group">
                <label required>Supplier</label>
                <div class="select">
                  <select name="supplier_id" class="form-control select2 evaluasi_vendor-supplier_id" data-placeholder="Select a supplier">
                    <?php
                    if (count($data_supplier) > 0) {
                      echo '<option value="">Select a item</option>';
                      foreach ($data_supplier as $key => $item) {
                        echo '<option value="' . $item->id . '">' . $item->nama . '</option>';
                      };
                    };
                    ?>
                  </select>
                  <i class="form-group__bar"></i>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label required>Harga / Biaya</label>
            <input type="text" name="harga" class="form-control mask-money evaluasi_vendor-harga" placeholder="Harga / Biaya">
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group" style="margin-bottom: 3rem;">
            <label required>Kualitas Barang / Pekerjaan</label>
            <textarea name="kualitas_barang" class="form-control textarea-autosize text-counter evaluasi_vendor-kualitas_barang" rows="1" data-max-length="300" placeholder="Kualitas Barang / Pekerjaan" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"></textarea>
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group" style="margin-bottom: 3rem;">
            <label required>Tepat Waktu Pengiriman / Pelaksanaan Pekerjaan</label>
            <textarea name="waktu_pengiriman" class="form-control textarea-autosize text-counter evaluasi_vendor-waktu_pengiriman" rows="1" data-max-length="300" placeholder="Tepat Waktu Pengiriman / Pelaksanaan Pekerjaan" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"></textarea>
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group" style="margin-bottom: 3rem;">
            <label required>Cara Pembayaran</label>
            <textarea name="cara_pembayaran" class="form-control textarea-autosize text-counter evaluasi_vendor-cara_pembayaran" rows="1" data-max-length="300" placeholder="Cara Pembayaran" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"></textarea>
            <i class="form-group__bar"></i>
          </div>

          <div class="form-group" style="margin-bottom: 3rem;">
            <label>Lain-lain</label>
            <textarea name="lain_lain" class="form-control textarea-autosize text-counter evaluasi_vendor-lain_lain" rows="1" data-max-length="300" placeholder="Lain-lain" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"></textarea>
            <i class="form-group__bar"></i>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text evaluasi_vendor-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text evaluasi_vendor-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>