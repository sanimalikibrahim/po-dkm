<section id="user">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
    
        <div class="table-action">
            <div class="buttons">
                <button class="btn btn--raised btn-primary btn--icon-text user-action-add" data-toggle="modal" data-target="#modal-form-user">
                    <i class="zmdi zmdi-plus"></i> Add New
                </button>
            </div>
        </div>
    
        <?php include_once('form.php') ?>
        <?php include_once('import.php') ?>
        <?php include_once('generate_password.php') ?>
    
        <div class="table-responsive">
            <table id="table-user" class="table table-bordered">
                <thead class="thead-default">
                    <tr>
                        <th width="100">No</th>
                        <th>NIK</th>
                        <th>Email</th>
                        <th>Nama Lengkap</th>
                        <th>Jabatan</th>
                        <th>Cabang</th>
                        <th>Departemen</th>
                        <th width="100">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
      </div>
    </div>
</section>
