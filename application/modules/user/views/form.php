<div class="modal fade" id="modal-form-user" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Pengguna</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-user">

          <div class="row">
            <div class="col-xs-12 col-md-12">
              <div class="form-group">
                <label required>Role</label>
                <select name="role" class="user-role form-control">
                  <option value="Administrasi">Administrasi</option>
                  <option value="Finance">Finance</option>
                  <option value="Manager">Manager</option>
                  <option value="G. Manager">G. Manager</option>
                  <option value="Deputy. Dir">Deputy. Dir</option>
                  <option value="Direktur">Direktur</option>
                  <option value="Supervisor">Supervisor</option>
                  <option value="G. Affair">G. Affair</option>
                  <option value="Admin Gudang">Admin Gudang</option>
                  <option value="Admin Billing">Admin Billing</option>
                  <option value="Admin Invoicing">Admin Invoicing</option>
                  <option value="Konstruksi">Konstruksi</option>
                  <option value="Security">Security</option>
                  <option value="Trading">Trading</option>
                  <option value="Housekeeping">Housekeeping</option>
                </select>
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label required>NIK</label>
                <input type="text" name="nik" class="form-control mask-number user-nik" placeholder="NIK" required>
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label required>Username</label>
                <input type="text" name="username" class="form-control user-username" placeholder="Username" required>
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label required>Nama Lengkap</label>
                <input type="text" name="nama_lengkap" class="form-control user-nama_lengkap" placeholder="Nama Lengkap" required>
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label required>Email</label>
                <input type="email" name="email" class="form-control user-email" placeholder="Email" required>
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label>Jabatan</label>
                <input type="text" name="jabatan" class="form-control user-jabatan" placeholder="Jabatan">
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label>Cabang</label>
                <input type="text" name="cabang" class="form-control user-cabang" placeholder="Cabang">
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label>Departemen</label>
                <input type="text" name="departemen" class="form-control user-departemen" placeholder="Departemen">
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control user-password" placeholder="(Optional)">
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text user-action-save">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text user-action-cancel" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>