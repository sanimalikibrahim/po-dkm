<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "user";
    var _table = "table-user";
    var _modal = "modal-form-user";
    var _modal_import = "modal-form-user-import";
    var _modal_gpw = "modal-form-user-gpw";
    var _form = "form-user";
    var _form_import = "form-user-import";
    var _form_gpw = "form-user-gpw";

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_user = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('user/ajax_getall/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nik"
          },
          {
            data: "email"
          },
          {
            data: "nama_lengkap"
          },
          {
            data: "jabatan"
          },
          {
            data: "cabang"
          },
          {
            data: "departemen"
          },
          {
            data: null,
            className: "center",
            defaultContent: '<div class="action">' +
              '<a href="javascript:;" class="action-edit" data-toggle="modal" data-target="#' + _modal + '"><i class="zmdi zmdi-edit"></i></a>' +
              '<a href="javascript:;" class="action-delete"><i class="zmdi zmdi-delete"></i></a>' +
              '</div>'
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7]
        }, {
          className: 'tablet',
          targets: [0, 1, 3, 4]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data add
    $("#" + _section).on("click", "button.user-action-add", function(e) {
      e.preventDefault();
      resetForm();
    });

    // Handle data add import
    $("#" + _section).on("click", "button.user-action-import", function(e) {
      e.preventDefault();
      resetForm();
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      var temp = table_user.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form + " .user-role").val(temp.role);
      $("#" + _form + " .user-nik").val(temp.nik);
      $("#" + _form + " .user-username").val(temp.username);
      $("#" + _form + " .user-password").val("");
      $("#" + _form + " .user-email").val(temp.email);
      $("#" + _form + " .user-nama_lengkap").val(temp.nama_lengkap);
      $("#" + _form + " .user-jabatan").val(temp.jabatan);
      $("#" + _form + " .user-cabang").val(temp.cabang);
      $("#" + _form + " .user-departemen").val(temp.departemen);
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_user.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('user/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data submit
    $("#" + _modal + " .user-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('user/ajax_save/') ?>" + _key,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data submit import
    $("#" + _modal_import + " .user-action-save-import").on("click", function(e) {
      e.preventDefault();

      var form = $("#" + _form_import)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('user/ajax_import/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            resetForm();
            $("#" + _modal_import).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data submit generate password
    $("#" + _modal_gpw + " .user-action-save-gpw").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('user/ajax_generatepassword/') ?>",
        data: $("#" + _form_gpw).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal_gpw).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      $("#" + _form).trigger("reset");
      $("#" + _form_import).trigger("reset");
      $('.upload-inline .upload-preview').html("No file chosen");
    };

    // Handle upload
    $(".user-file").change(function() {
      readUploadInlineDocURL(this);
    });

  });
</script>