<div class="modal fade" id="modal-form-user-gpw" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Generate Password</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-user-gpw">

          <div class="alert alert-info text-center" role="alert">
            Password hasil generate merupakan kombinasi id dan nik.
          </div>

          <div>
            <div class="radio">
              <label><input type="radio" name="target" value="all" checked>Semua Karyawan</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="target" value="new">Baru Ditambahkan / Belum Memiliki Password</label>
            </div>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text user-action-save-gpw">
          <i class="zmdi zmdi-save"></i> Generate
        </button>
        <button type="button" class="btn btn-light btn--icon-text user-action-cancel-gpw" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>
