<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once( APPPATH.'controllers/AppBackend.php' );
require_once(FCPATH.'vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class User extends AppBackend
{
	function __construct() {
    parent::__construct();
    $this->load->model(['AppModel', 'UserModel']);
    $this->load->library('form_validation');
	}

	public function index() {
		$data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('user'),
			'card_title' => 'Pengguna'
		);
		$this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
		$this->template->load_view('index', $data, TRUE);
		$this->template->render();
  }
  
  public function ajax_getAll() {
    $this->handle_ajax_request();
		$dtAjax_config = array(
      'table_name' => 'user',
      'order_column' => 0
		);
    $response = $this->AppModel->getData_dtAjax( $dtAjax_config );
		echo json_encode( $response );
  }

  public function ajax_save($id = null) {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->UserModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->UserModel->insert());
      } else {
        echo json_encode($this->UserModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_import() {
    $this->handle_ajax_request();

    try {
      $this->file = json_decode(json_encode($_FILES['file']));
  
      if (isset($this->file) && !empty($this->file->name)) {
        $inputFileTemp = $this->file->tmp_name;
        
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileTemp);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        
        $reader->setReadDataOnly(true);
  
        $spreadsheet = $reader->load($inputFileTemp);
        $activeSheet = $spreadsheet->getActiveSheet();
        $worksheetInfo = $reader->listWorksheetInfo($inputFileTemp);
        $worksheetInfo = $worksheetInfo[0];
  
        $colStart = 1;
        $rowStart = 3;
        $columnEnd = $worksheetInfo['totalColumns'];
        $rowEnd = $worksheetInfo['totalRows'];
  
        for ($row = $rowStart; $row <= $rowEnd; $row++) {
          for ($col = $colStart; $col <= $columnEnd; $col++) {
            $worksheetData[$row][$this->UserModel->getColumnName($col)] = $activeSheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
            $worksheetData[$row]['password'] = null;
            $worksheetData[$row]['role'] = 'Karyawan';
            $worksheetData[$row]['created_by'] = null;
            $worksheetData[$row]['created_date'] = date('Y-m-d H:i:s');
            $worksheetData[$row]['updated_by'] = null;
            $worksheetData[$row]['updated_date'] = null;
          };
        };
  
        // $adminData = $this->UserModel->getBackup(['role' => 'Admin']);
        $worksheetData = array_values($worksheetData);
        // $pushData = array_merge($adminData, $worksheetData);
        // $truncateData = $this->UserModel->truncate();

        // if ($truncateData['status']) {
          echo json_encode($this->UserModel->insertBatch($worksheetData));
        // } else {
        //   echo json_encode($truncateData);
        // };
      } else {
        $errors = '<div>The File field is required.</div>';
        echo json_encode(array('status' => false, 'data' => $errors));
      };
    } catch (\Throwable $th) {
      $errors = '<div>Failed to get information data.</div>';
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_generatePassword() {
    $this->handle_ajax_request();
    $this->target = $_POST['target'];

    switch ($this->target) {
      case 'all':
        $params = array('role' => 'Karyawan');
        break;
      case 'new':
        $params = array('role' => 'Karyawan', 'password is null' => null);
        break;
    };

    echo json_encode($this->UserModel->generatePassword($params));
  }

  public function ajax_delete($id) {
    $this->handle_ajax_request();
    echo json_encode($this->UserModel->delete($id));
  }
}
