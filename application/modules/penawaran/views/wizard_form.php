<?php require_once('main.css.php') ?>
<?php
$revisiNomor = null;
$revisiNomorLabel = null;
$isEditor = (in_array(strtolower($this->session->userdata('user')['role']), array('administrasi', 'konstruksi', 'trading'))) ? true : false;
?>

<section id="penawaran">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute;">
                        <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                            <?php $revisiNomor = (!is_null($penawaran->revisi_nomor)) ? (int) $penawaran->revisi_nomor + 1 : 1; ?>
                            REV
                        <?php else : ?>
                            <?php $revisiNomorLabel = (isset($penawaran->revisi_nomor) && !is_null($penawaran->revisi_nomor)) ? ' : Rev. ' . $penawaran->revisi_nomor : null; ?>
                            <?= (isset($penawaran->id)) ? 'EDIT' : 'NEW' ?>
                        <?php endif ?>
                    </span>
                    <div style="margin-left: 60px; margin-top: 5px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title . $revisiNomorLabel : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <form id="form-penawaran" enctype="multipart/form-data">

                <!-- Temporary hidden field -->
                <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                    <input type="hidden" name="is_revisi" value="true" readonly />
                    <input type="hidden" name="revisi_nomor" value="<?= $revisiNomor ?>" readonly />

                    <div class="alert alert-secondary mb-4">
                        <i class="zmdi zmdi-info"></i>
                        <b>Revisi <?= $revisiNomor ?></b>, nomor sebelumnya : <?= $penawaran->nomor ?>
                    </div>
                <?php endif ?>

                <div class="form-group">
                    <label required>Department</label>
                    <input type="text" name="department" class="form-control penawaran-department" placeholder="Department" value="<?= (isset($penawaran->department)) ? $penawaran->department : null ?>" style="opacity: 1;" required />
                    <i class="form-group__bar"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Nomor</label>
                            <input type="text" name="nomor" class="form-control penawaran-nomor" placeholder="Nomor" value="<?= (isset($penawaran->nomor) && $is_revisi === false) ? $penawaran->nomor : $data_auto_nomor ?>" required />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Date</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text p-0 border-bottom-0">
                                        <input type="text" name="city" class="form-control penawaran-city m-0 pl-0" placeholder="City" value="<?= (isset($penawaran->city)) ? $penawaran->city : 'Jakarta' ?>" required />
                                        <i class="form-group__bar"></i>
                                    </span>
                                </div>
                                <input type="text" name="tanggal" class="form-control penawaran-tanggal flatpickr-date" placeholder="Date" style="opacity: 1;" value="<?= (isset($penawaran->tanggal)) ? $penawaran->tanggal : null ?>" required />
                                <i class=" form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label required>Project Name</label>
                    <input type="text" name="project_name" class="form-control penawaran-project_name" placeholder="Project Name" value="<?= (isset($penawaran->project_name)) ? $penawaran->project_name : null ?>" required />
                    <i class="form-group__bar"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Customer Name</label>
                            <input type="text" name="customer" class="form-control penawaran-customer" placeholder="Customer Name" value="<?= (isset($penawaran->customer)) ? $penawaran->customer : null ?>" required />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Attn</label>
                            <input type="text" name="customer_attn" class="form-control penawaran-customer_attn" placeholder="Atn" value="<?= (isset($penawaran->customer_attn)) ? $penawaran->customer_attn : null ?>" required />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Customer Address 1</label>
                            <input type="text" name="customer_address1" class="form-control penawaran-customer_address1" placeholder="Customer Address" value="<?= (isset($penawaran->customer_address1)) ? $penawaran->customer_address1 : null ?>" required />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>Customer Address 2</label>
                            <input type="text" name="customer_address2" class="form-control penawaran-customer_address2" placeholder="Customer Address" value="<?= (isset($penawaran->customer_address2)) ? $penawaran->customer_address2 : null ?>" />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 3rem;">
                    <label>Note</label>
                    <textarea name="note" class="form-control textarea-autosize text-counter penawaran-note" rows="1" data-max-length="1000" placeholder="Note" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"><?= (isset($penawaran->note)) ? $penawaran->note : null ?></textarea>
                    <i class="form-group__bar"></i>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group" style="margin-bottom: 0px;">
                            <label>Attachment</label>
                            <a href="javascript:;" data-toggle="popover" data-trigger="focus" data-content="Supported format: JPG, PNG & PDF">
                                <span class="zmdi zmdi-help"></span>
                            </a>
                            <?php if ($isEditor && (!isset($penawaran->status) || (in_array((int) $penawaran->status, [0, 3])))) : ?>
                                <a href="javascript:;" class="btn btn-sm btn-success btn--raised attachment-add" style="margin-left: 10px;">
                                    <i class="zmdi zmdi-file-plus"></i> Add File
                                </a>
                                <div class="attachment-input-wrapper"></div>
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <div class="attachment">
                                <?php if (count($penawaran_attachment) > 0) : ?>
                                    <?php foreach ($penawaran_attachment as $key => $item) : ?>
                                        <?php $isImage = (strpos($item->file_type, 'image') !== false) ? true : false ?>
                                        <div class="attachment-item attachment-data-item-<?php echo $item->id ?>">
                                            <div class="row">
                                                <div class="col-auto">
                                                    <?php if ($isImage) : ?>
                                                        <img src="<?php echo base_url($item->file_name_thumb) ?>" class="attachment-preview attachment-preview-img" />
                                                    <?php else : ?>
                                                        <div class="attachment-preview attachment-preview-file">
                                                            <i class="zmdi zmdi-file-text"></i>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="col" style="padding-left: 0;">
                                                    <div style="<?php echo ($is_mobile) ? 'text-overflow: ellipsis; overflow: hidden; width: 200px; height: 1.2em; white-space: nowrap;' : '' ?>">
                                                        <a href="<?php echo base_url($item->file_name) ?>" target="_blank">
                                                            <span><?php echo $item->file_raw_name ?></span>
                                                        </a>
                                                    </div>
                                                    <?php if ($isEditor && (!isset($penawaran->status) || (in_array((int) $penawaran->status, [0, 3])))) : ?>
                                                        <a href="javascript:;" class="link-black attachment-delete-existing" data-id="<?php echo $item->id ?>">
                                                            <small><i class="zmdi zmdi-close-circle"></i> Delete</small>
                                                        </a>
                                                    <?php endif; ?>
                                                    <span class="attachment-size">
                                                        <?php if ($isEditor && (!isset($penawaran->status) || (in_array((int) $penawaran->status, [0, 3])))) : ?>
                                                            <i class="zmdi zmdi-minus"></i>
                                                        <?php endif; ?>
                                                        <?php echo $item->file_size ?> KB
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    <label>No data available</label>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bg-white p-3 border mb-4">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group mb-0">
                                <label required>Approval Role</label>
                                <div class="select">
                                    <select name="approval_role" class="penawaran-approval_role form-control">
                                        <?= $approval_role_list ?>
                                    </select>
                                </div>
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group mb-0">
                                <label required>Approval PIC</label>
                                <input type="hidden" name="approval_name" class="form-control penawaran-approval_name" style="opacity: 1; cursor: not-allowed;" placeholder="Approval PIC" value="<?= (isset($penawaran->approval_name)) ? $penawaran->approval_name : null ?>" readonly />
                                <div class="select">
                                    <select name="approval_user_id" class="penawaran-approval_user_id form-control">
                                        <?= $approval_pic_list ?>
                                    </select>
                                </div>
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <small class="form-text text-muted">
                    Fields with red stars (<label required></label>) are required.
                </small>

                <div class="row">
                    <div class="col">
                        <div class="buttons-container">
                            <div class="row">
                                <div class="col text-right">
                                    <button class="btn btn--raised btn-primary btn--icon-text btn-custom penawaran-action-save-draft spinner-action-button">
                                        <i class="zmdi zmdi-save"></i>
                                        Save as Draft
                                    </button>
                                    <button class="btn btn--raised btn-success btn--icon-text btn-custom penawaran-action-save spinner-action-button">
                                        Next
                                        <i class="zmdi zmdi-long-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>