<?php include_once(APPPATH . 'modules/penawaran/views/_import_item.php') ?>

<a href="javascript:;" class="btn btn-dark mb-2 action-import-item" data-toggle="modal" data-target="#modal-import-item">
  <i class="zmdi zmdi-cloud-upload"></i> Import Item
</a>

<div class="table-responsive">
  <table class="table table-sm table-bordered table-hover">
    <thead>
      <tr>
        <th width="80">No</th>
        <th>Uraian Pekerjaan</th>
        <th width="150">Volume</th>
        <th width="150">Satuan</th>
        <th width="150">Unit Price (IDR)</th>
        <th width="150">Total (IDR)</th>
        <th width="80" class="text-center">#</th>
      </tr>
    </thead>
    <tbody>
      <?php
      // Ini gak dipake kalau via import
      // $total = 0;
      // $constructionFee = 0;
      // $grandTotal = 0;
      ?>
      <?php if (count($penawaran_item) > 0) : ?>
        <?php foreach ($penawaran_item as $index => $item) : ?>
          <?php
          // $total = (!is_null($item->total) && !empty($item->total)) ? (float) $item->total + $total : 0 + $total;
          $isBoldStyle = ($item->is_bold == 1) ? 'style="font-weight: bold;"' : '';
          ?>
          <div class="item-rows item-rows-<?= $item->id ?>">
            <?php if ($item->is_header == 1) : ?>
              <tr>
                <td colspan="7" class="p-0" style="height: 10px;"></td>
              </tr>
            <?php endif ?>
            <!-- Row data -->
            <tr>
              <td align="center" <?= $isBoldStyle ?>>
                <?= ($item->is_header == 1) ? '<b style="font-weight: bold;">' . $item->nomor . '</b>' : $item->nomor ?>
              </td>
              <td colspan="<?= ($item->is_header == 1) ? 5 : 0 ?>" <?= $isBoldStyle ?>>
                <?= ($item->is_header == 1) ? '<b style="font-weight: bold;">' . $item->uraian . '</b>' : $item->uraian ?>

                <!-- <?php if (!is_null($item->nomor) && !empty($item->nomor)) : ?>
                  <a href="javascript:;" data-id="<?= $item->id ?>" class="ml-2 item-button-row item-button-row-add item-button-row-<?= $item->id ?>" title="Add Item"><i class="zmdi zmdi-plus-circle"></i></a>
                <?php endif ?> -->
              </td>
              <?php if ($item->is_header == 0) : ?>
                <td <?= $isBoldStyle ?>><?= (!is_null($item->volume)) ? number_format($item->volume, 2) : null ?></td>
                <td <?= $isBoldStyle ?>><?= $item->satuan ?></td>
                <td <?= $isBoldStyle ?>><?= (!is_null($item->unit_price)) ? number_format($item->unit_price, 0) : null ?></td>
                <td <?= $isBoldStyle ?>><?= (!is_null($item->total)) ? number_format($item->total, 0) : null ?></td>
              <?php endif ?>
              <td align="center">
                <!-- <a href="javascript:;" data-id="<?= $item->id ?>" data-is_header="<?= $item->is_header ?>" data-penawaran_item_parent_id="<?= $item->penawaran_item_parent_id ?>" data-nomor="<?= $item->nomor ?>" data-uraian="<?= $item->uraian ?>" data-volume="<?= $item->volume ?>" data-satuan="<?= $item->satuan ?>" data-unit_price="<?= $item->unit_price ?>" data-total="<?= $item->total ?>" class="ml-2 item-button-row item-button-row-edit item-button-row-edit-<?= $item->id ?>" title="Edit">
                  <i class="zmdi zmdi-edit"></i>
                </a> -->

                <a href="javascript:;" data-id="<?= $item->id ?>" class="ml-2 item-button-row item-button-row-delete item-button-row-delete-<?= $item->id ?>" title="Delete"><i class="zmdi zmdi-delete"></i></a>

                <?php if (!is_null($item->uraian) && !empty($item->uraian)) : ?>
                  <?php if ($item->is_bold == 0) : ?>
                    <a href="javascript:;" data-id="<?= $item->id ?>" class="ml-2 item-button-row item-button-row-bold item-button-row-bold-<?= $item->id ?>" title="Set Bold"><i class="zmdi zmdi-format-bold"></i></a>
                  <?php else : ?>
                    <a href="javascript:;" data-id="<?= $item->id ?>" class="ml-2 item-button-row item-button-row-unbold item-button-row-unbold-<?= $item->id ?>" title="Unbold"><i class="zmdi zmdi-format-clear"></i></a>
                  <?php endif ?>
                <?php endif ?>
              </td>
            </tr>
            <!-- END ## Row data -->
            <!-- Item inline form -->
            <tr class="item-form-row item-form-row-<?= $item->id ?> bg-light" style="display: none;">
              <td align="center">
                <span class="badge badge-warning item-form-row-icon-<?= $item->id ?>">N</span>
              </td>
              <td colspan="6" class="pl-0">
                <div class="item-form-input item-form-input-wrapper-<?= $item->id ?> bg-white p-3">
                  <!-- Loader -->
                  <div class="spinner spinner-<?= $item->id ?>">
                    <div class="lds-hourglass"></div>
                  </div>
                  <form method="post" id="item-form-input-<?= $item->id ?>">
                    <!-- Temporary hidden field -->
                    <input type="hidden" name="penawaran_id" value="<?= $penawaran->id ?>" readonly />
                    <input type="hidden" name="penawaran_item_parent_id" class="penawaran_item-penawaran_item_parent_id-<?= $item->id ?>" value="<?= $item->id ?>" readonly />
                    <input type="hidden" name="is_header" class="penawaran_item-is_header-<?= $item->id ?>" value="0" readonly />

                    <div class="row">
                      <div class="col-xs-12 col-md-12">
                        <div class="col-xs-12 col-md-2">
                          <div class="form-group mb-3">
                            <label>Nomor</label>
                            <input type="text" name="nomor" class="form-control penawaran_item-nomor penawaran_item-nomor-<?= $item->id ?>" placeholder="Nomor" />
                            <i class="form-group__bar"></i>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                          <div class="form-group mb-3">
                            <label required>Uraian Pekerjaan</label>
                            <input type="text" name="uraian" class="form-control penawaran_item-uraian penawaran_item-uraian-<?= $item->id ?>" placeholder="Uraian Pekerjaan" required />
                            <i class="form-group__bar"></i>
                          </div>
                        </div>
                        <div class="item-other-input-<?= $item->id ?>">
                          <div class="col-xs-12 col-md-5">
                            <div class="row">
                              <div class="col-xs-12 col-md-6">
                                <div class="form-group mb-3">
                                  <label>Volume</label>
                                  <input type="text" name="volume" class="form-control mask-decimal penawaran_item-volume penawaran_item-volume-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Volume" />
                                  <i class="form-group__bar"></i>
                                </div>
                              </div>
                              <div class="col-xs-12 col-md-6">
                                <div class="form-group mb-3">
                                  <label>Satuan</label>
                                  <input type="text" name="satuan" class="form-control penawaran_item-satuan penawaran_item-satuan-<?= $item->id ?>" placeholder="Satuan" />
                                  <i class="form-group__bar"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-5">
                            <div class="row">
                              <div class="col-xs-12 col-md-6">
                                <div class="form-group mb-3">
                                  <label>Unit Price</label>
                                  <input type="text" name="unit_price" class="form-control mask-money penawaran_item-unit_price penawaran_item-unit_price-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Unit Price" />
                                  <i class="form-group__bar"></i>
                                </div>
                              </div>
                              <div class="col-xs-12 col-md-6">
                                <div class="form-group mb-3">
                                  <label>Total</label>
                                  <input type="text" name="total" class="form-control mask-money penawaran_item-total penawaran_item-total-<?= $item->id ?>" placeholder="Total" readonly />
                                  <i class="form-group__bar"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                          <a href="javascript:;" class="btn btn-success item-form-input-save" data-id="<?= $item->id ?>"><i class="zmdi zmdi-save"></i> Save</a>
                          <a href="javascript:;" class="btn btn-secondary item-form-input-cancel">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </td>
            </tr>
            <!-- END ## Item inline form -->
          </div>
        <?php endforeach ?>
      <?php endif ?>

      <!-- Header inline form -->
      <!-- <div class="header-button">
        <tr class="header-form-row bg-light">
          <td align="center">
            <span class="badge badge-warning header-form-row-icon" style="display: none;">N</span>
          </td>
          <td colspan="6" class="pl-0">
            <a href="javascript:;" class="btn btn-secondary btn-sm header-button-row-add"><i class="zmdi zmdi-plus-circle"></i> Add Header</a>

            <div class="header-form-input-wrapper">
              <form method="post" id="header-form-input" class="bg-white p-3" style="display: none;">
                <div class="spinner spinner-header">
                  <div class="lds-hourglass"></div>
                </div>

                <input type="hidden" name="penawaran_id" value="<?= $penawaran->id ?>" readonly />
                <input type="hidden" name="penawaran_item_parent_id" value="" readonly />
                <input type="hidden" name="is_header" value="1" readonly />

                <div class="row">
                  <div class="col-xs-12 col-md-12">
                    <div class="col-xs-12 col-md-2">
                      <div class="form-group mb-3">
                        <label required>Nomor</label>
                        <input type="text" name="nomor" class="form-control penawaran_item-nomor" placeholder="Nomor" required />
                        <i class="form-group__bar"></i>
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-12">
                      <div class="form-group mb-3">
                        <label required>Uraian Pekerjaan</label>
                        <input type="text" name="uraian" class="form-control penawaran_item-uraian" placeholder="Uraian Pekerjaan" required />
                        <i class="form-group__bar"></i>
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-12">
                      <a href="javascript:;" class="btn btn-success header-form-input-save"><i class="zmdi zmdi-save"></i> Save</a>
                      <a href="javascript:;" class="btn btn-secondary header-form-input-cancel">Cancel</a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </td>
        </tr>
      </div> -->
      <!-- END ## Header inline form -->
    </tbody>
    <!-- <tfoot>
      <tr>
        <td colspan="7" class="p-0" style="height: 10px;"></td>
      </tr>
      <tr>
        <th colspan="5" class="text-right">Total</th>
        <th colspan="2"><?= number_format($total) ?></th>
      </tr>
      <tr>
        <th colspan="5" class="text-right">Construction Fee 10%</th>
        <th colspan="2"><?= number_format(($total / 100) * 10) ?></th>
      </tr>
      <tr>
        <th colspan="5" class="text-right">Grand Total</th>
        <th colspan="2"><?= number_format($total + (($total / 100) * 10)) ?></th>
      </tr>
    </tfoot> -->
  </table>
</div>