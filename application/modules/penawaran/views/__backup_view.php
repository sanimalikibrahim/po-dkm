<?php require_once('main.css.php') ?>

<div class="hidden-print">
  <div id="penawaran-other-wrapper">
    <!-- Persetujuan -->
    <?php if (in_array($penawaran->status, array(2, 3, 4))) : ?>
      <div class="card m-0">
        <div class="card-header p-1" id="penawaran-persetjuan">
          <h5 class="mb-0">
            <button class="btn btn-link" data-toggle="collapse" data-target="#persetjuan-list" aria-expanded="true" aria-controls="persetjuan-list">
              <i class="zmdi zmdi-account mr-1" style="width: 10px; text-align: left;"></i>
              Persetujuan
            </button>
          </h5>
        </div>
        <div id="persetjuan-list" class="collapse show" aria-labelledby="penawaran-persetjuan" data-parent="#penawaran-other-wrapper">
          <div class="card-body p-3">
            <table class="m-0 p-0">
              <tr>
                <td width="60" valign="top">Status</td>
                <td width="10" valign="top">:</td>
                <td valign="top">
                  <?= (isset($penawaran->status)) ? $controller->get_status($penawaran->status) : null ?>
                  <?= (isset($penawaran->approval_date)) ? '(' . $penawaran->approval_date . ')' : null ?>
                </td>
              </tr>
              <tr>
                <td width="60" valign="top">PIC</td>
                <td width="10" valign="top">:</td>
                <td valign="top">
                  <?= (isset($penawaran->approval_name)) ? $penawaran->approval_name : null ?>
                  <?= (isset($penawaran->approval_role)) ? '/ ' . $penawaran->approval_role : null ?>
                </td>
              </tr>
              <tr>
                <td width="60" valign="top">Catatan</td>
                <td width="10" valign="top">:</td>
                <td valign="top"><?= (isset($penawaran->approval_note)) ? str_replace("\r\n", '<br/>', htmlspecialchars_decode($penawaran->approval_note)) : null ?></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    <?php endif ?>
    <!-- Riwayat Revisi -->
    <?php if (count($penawaran_arsip) > 0) : ?>
      <div class="card m-0">
        <div class="card-header p-1" id="penawaran-riwayat">
          <h5 class="mb-0">
            <button class="btn btn-link" data-toggle="collapse" data-target="#riwayat-list" aria-expanded="true" aria-controls="riwayat-list">
              <i class="zmdi zmdi-file-text mr-1" style="width: 10px; text-align: left;"></i> Riwayat Revisi
            </button>
          </h5>
        </div>
        <div id="riwayat-list" class="collapse" aria-labelledby="penawaran-riwayat" data-parent="#penawaran-other-wrapper">
          <div class="card-body p-3">
            <ul class="pl-4 m-0">
              <?php foreach ($penawaran_arsip as $index => $item) : ?>
                <li>
                  <a href="javascript:;" data-id="<?= $item->id ?>" data-status="<?= $item->status ?>" class="action-penawaran-riwayat" data-toggle="modal" data-target="#modal-penawaran-view-riwayat">
                    Rev. <?= $item->revisi_nomor ?> (<?= $item->created_at ?>)
                  </a>
                </li>
              <?php endforeach ?>
            </ul>
          </div>
        </div>
      </div>
    <?php endif ?>
    <!-- Attachment -->
    <?php if (isset($is_attachment_visible) && $is_attachment_visible === true) : ?>
      <div class="card m-0">
        <div class="card-header p-1" id="penawaran-attachment">
          <h5 class="mb-0">
            <button class="btn btn-link" data-toggle="collapse" data-target="#attachment-list" aria-expanded="true" aria-controls="attachment-list">
              <i class="zmdi zmdi-attachment mr-1" style="width: 10px; text-align: left;"></i>
              Attachment (<?= count($penawaran_attachment) ?>)
            </button>
          </h5>
        </div>
        <div id="attachment-list" class="collapse" aria-labelledby="penawaran-attachment" data-parent="#penawaran-other-wrapper">
          <div class="card-body p-3">
            <div style="display: flex;">
              <div class="form-group" style="margin-bottom: 0px; flex: 1;">
                <div class="attachment p-0" style="margin-top: 0px; background: #fff;">
                  <?php if (count($penawaran_attachment) > 0) : ?>
                    <?php foreach ($penawaran_attachment as $key => $item) : ?>
                      <?php $isImage = (strpos($item->file_type, 'image') !== false) ? true : false ?>
                      <div class="attachment-item attachment-data-item-<?php echo $item->id ?>">
                        <div class="row">
                          <div class="col-auto">
                            <?php if ($isImage) : ?>
                              <img src="<?php echo base_url($item->file_name_thumb) ?>" class="attachment-preview attachment-preview-img" />
                            <?php else : ?>
                              <div class="attachment-preview attachment-preview-file">
                                <i class="zmdi zmdi-file-text"></i>
                              </div>
                            <?php endif; ?>
                          </div>
                          <div class="col-auto" style="padding-left: 0;">
                            <div style="<?php echo ($is_mobile) ? 'text-overflow: ellipsis; overflow: hidden; width: 150px; height: 1.2em; white-space: nowrap;' : '' ?>">
                              <a href="<?php echo base_url($item->file_name) ?>" target="_blank">
                                <span><?php echo $item->file_raw_name ?></span>
                              </a>
                            </div>
                            <span class="attachment-size">
                              <?php echo $item->file_size ?> KB
                            </span>
                          </div>
                        </div>
                      </div>
                    <?php endforeach; ?>
                  <?php else : ?>
                    <label>No data available</label>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php endif ?>
  </div>
  <?php if (in_array($penawaran->status, array(2, 3, 4)) || count($penawaran_arsip) > 0 || (isset($is_attachment_visible) && $is_attachment_visible === true)) : ?>
    <hr>
  <?php endif ?>
</div>

<div class="preview-po" style="border: 0; padding: 0;">
  <div class="preview-header">
    <!-- <table class="table-header">
      <tr>
        <td valign="top">
          <div class="kop-surat">
            <span style="font-size: 12px;"><b><?= (isset($penawaran->department)) ? $penawaran->department : null ?></b></span> <br>
            <span style="font-size: 12px;"><?= (isset($penawaran->customer)) ? $penawaran->customer : null ?></span> <br>
            <span style="font-size: 12px;"><?= (isset($penawaran->customer_address1)) ? $penawaran->customer_address1 : null ?></span> <br>
            <span style="font-size: 12px;"><?= (isset($penawaran->customer_address2)) ? $penawaran->customer_address2 : null ?></span>
          </div>
        </td>
        <td valign="top" style="text-align: right;">
          <span style="font-size: 12px;"><?= (isset($penawaran->city)) ? $penawaran->city : null ?>, <?= (isset($penawaran->tanggal)) ? $controller->localizeDate($penawaran->tanggal) : null ?></span> <br>
          <span style="font-size: 12px;"><b>Pen. No. : <?= (isset($penawaran->nomor)) ? $penawaran->nomor : null ?></b></span>
        </td>
      </tr>
    </table> -->
    <div style="margin-bottom: 15px;"></div>
  </div>
  <div class="preview-body">
    <!-- <table style="width: 100%; margin-bottom: 15px;">
      <tr>
        <td style="font-size: 12px; width: 50px;"><b>Attn</b></td>
        <td style="font-size: 12px; width: 10px;">:</td>
        <td style="font-size: 12px;"><b><?= (isset($penawaran->customer_attn)) ? $penawaran->customer_attn : null ?></b></td>
        <?php if (isset($penawaran->revisi_nomor) && !is_null($penawaran->revisi_nomor) && $penawaran->revisi_nomor != 0) : ?>
          <td style="font-size: 12px; width: 80px; text-align: center; background: yellow; border: 1px solid #8c8b8b;">
            <b>Revisi <?= $penawaran->revisi_nomor ?></b>
          </td>
        <?php else : ?>
          <td>&nbsp;</td>
        <?php endif ?>
      </tr>
      <tr>
        <td style="font-size: 12px; width: 50px;">Project</td>
        <td style="font-size: 12px; width: 10px;">:</td>
        <td style="font-size: 12px;" colspan="2"><?= (isset($penawaran->project_name)) ? $penawaran->project_name : null ?></td>
      </tr>
      <tr>
        <td colspan="4">
          <div style="margin-bottom: 15px;"></div>
          <span style="font-size: 12px;">Bersama ini kami mengajukan penawaran harga sebagai berikut :</span>
        </td>
      </tr>
    </table>

    <table class="table-order-item" style="border-collapse: collapse;">
      <thead>
        <tr>
          <th width="50">No</th>
          <th>URAIAN PEKERJAAN</th>
          <th width="80">VOL.</th>
          <th width="80">SAT.</th>
          <th width="130">UNIT PRICE (IDR)</th>
          <th width="130">TOTAL (IDR)</th>
        </tr>
      </thead>
      <tbody>
        <?php $total = 0 ?>
        <?php if (isset($penawaran_item) && count($penawaran_item) > 0) : ?>
          <?php $no = 1 ?>
          <?php foreach ($penawaran_item as $key => $item) : ?>
            <?php $total = (!is_null($item->total) && !empty($item->total)) ? (float) $item->total + $total : 0 + $total ?>
            <?php if ($item->is_header == 1) : ?>
              <tr>
                <td colspan="6" style="height: 15px;"></td>
              </tr>
            <?php endif ?>
            <tr>
              <td valign="top" align="center">
                <?= ($item->is_header == 1) ? '<b style="font-weight: bold;">' . $item->nomor . '</b>' : $item->nomor ?>
              </td>
              <td valign="top" colspan="<?= ($item->is_header == 1) ? 5 : 0 ?>">
                <?= ($item->is_header == 1) ? '<b style="font-weight: bold;">' . $item->uraian . '</b>' : $item->uraian ?>
              </td>
              <?php if ($item->is_header == 0) : ?>
                <td valign="top" style="text-align: right;"><?= (!is_null($item->volume)) ? number_format($item->volume, 2) : null ?></td>
                <td valign="top" style="text-align: center;"><?= $item->satuan ?></td>
                <td valign="top" style="text-align: right;"><?= (!is_null($item->unit_price)) ? number_format($item->unit_price, 0) : null ?></td>
                <td valign="top" style="text-align: right;"><?= (!is_null($item->total)) ? number_format($item->total, 0) : null ?></td>
              <?php endif ?>
            </tr>
          <?php endforeach; ?>
        <?php else : ?>
          <tr>
            <td colspan="6" style="padding: 15px;">
              No data available in table
            </td>
          </tr>
        <?php endif; ?>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="7" class="p-0" style="height: 15px;"></td>
        </tr>
        <tr>
          <th colspan="5" style="text-align: right;">Total</th>
          <th style="text-align: right;"><?= number_format($total) ?></th>
        </tr>
        <tr>
          <th colspan="5" style="text-align: right;">Construction Fee 10%</th>
          <th style="text-align: right;"><?= number_format(($total / 100) * 10) ?></th>
        </tr>
        <tr>
          <th colspan="5" style="text-align: right;">Grand Total</th>
          <th style="text-align: right;"><?= number_format($total + (($total / 100) * 10)) ?></th>
        </tr>
      </tfoot>
    </table> -->

    <!-- INLINE -->
    <div class="inline">
      <?php if (isset($penawaran->revisi_nomor) && !is_null($penawaran->revisi_nomor) && $penawaran->revisi_nomor != 0) : ?>
        <div class="mb-4" style="font-size: 12px; width: 80px; text-align: center; background: yellow; border: 1px solid #8c8b8b;">
          <b>Revisi <?= $penawaran->revisi_nomor ?></b>
        </div>
      <?php endif ?>
      <div class="form-group">
        <label>Department</label>
        <div class="form-control">
          <?= (isset($penawaran->department)) ? $penawaran->department : null ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-md-6">
          <div class="form-group">
            <label>Nomor</label>
            <div class="form-control">
              <?= (isset($penawaran->nomor)) ? $penawaran->nomor : null ?>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-md-6">
          <div class="form-group">
            <label>Date</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text p-0 border-bottom-0">
                  <div class="form-control">
                    <?= (isset($penawaran->city)) ? $penawaran->city : null ?>
                  </div>
                </span>
              </div>
              <div class="form-control">
                <?= (isset($penawaran->tanggal)) ? $penawaran->tanggal : null ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label>Project Name</label>
        <div class="form-control">
          <?= (isset($penawaran->project_name)) ? $penawaran->project_name : null ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-md-6">
          <div class="form-group">
            <label>Customer Name</label>
            <div class="form-control">
              <?= (isset($penawaran->customer)) ? $penawaran->customer : null ?>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-md-6">
          <div class="form-group">
            <label>Attn</label>
            <div class="form-control">
              <?= (isset($penawaran->customer_attn)) ? $penawaran->customer_attn : null ?>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label>Customer Address</label>
        <div class="form-control" style="height: auto;">
          <?= (isset($penawaran->customer_address1)) ? $penawaran->customer_address1 : null ?>
          <?= (isset($penawaran->customer_address2)) ? '<br/>' . $penawaran->customer_address2 : null ?>
        </div>
      </div>
      <div class="form-group">
        <label>Note</label>
        <div class="form-control" style="height: auto;">
          <?= (isset($penawaran->note)) ? str_replace("\r\n", '<br/>', htmlspecialchars_decode($penawaran->note)) : null ?>
        </div>
      </div>
      <div class="form-group mb-0">
        <label>Approval PIC</label>
        <div class="form-control" style="height: auto;">
          <span style="font-size: 12px;"><?= (isset($penawaran->approval_name)) ? $penawaran->approval_name : null ?></span>
          <span style="font-size: 12px;"><?= (isset($penawaran->approval_role)) ? ' &nbsp;/&nbsp; ' . $penawaran->approval_role : null ?></span>
        </div>
      </div>
    </div>
    <!-- END ## INLINE -->

    <!-- <div style="margin-bottom: 15px;"></div>

    <span style="font-size: 12px;"><b>PT. DHARMA KARYATAMA MULIA</b></span>
    <div style="margin-bottom: 50px;"></div>
    <span style="font-size: 12px;"><b><u><?= (isset($penawaran->approval_name)) ? $penawaran->approval_name : null ?></u></b></span> <br>
    <span style="font-size: 12px;"><?= (isset($penawaran->approval_role)) ? $penawaran->approval_role : null ?></span> -->
  </div>
</div>