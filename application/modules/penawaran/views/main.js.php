<script type="text/javascript">
  $(document).ready(function() {
    var _key = "";
    var _section = "penawaran";
    var _table_list = "table-list";
    var _table_item_list = "table-penawaran_item";
    var _table_approved = "table-approved";
    var _form_penawaran = "form-penawaran";
    var _penawaranId = "<?= $penawaran_id ?>";
    var _modal_upload = "modal-form-upload";
    var _form_upload = "form-penawaran-upload";
    var _modal_approval = "modal-form-approval";
    var _form_approval = "form-penawaran-approval";
    var _modal_view = "modal-penawaran-view";
    var _modal_view_riwayat = "modal-penawaran-view-riwayat";
    var _module = "<?= $module ?>";
    var _modal_source = "";
    var _listFilter = "<?= (!is_null($this->input->get('ref'))) ? $this->input->get('ref') : null ?>";

    // PENAWARAN ITEM
    $(document).on("click", "a.item-button-row-add", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var addButton = $(".item-button-row");
      var form = $(".item-form-row-" + ref);
      var formIcon = $(".item-form-row-icon-" + ref);
      var formHeader = $(".header-form-row");
      var formInput = $("#item-form-input-" + ref);
      var otherInput = $(".item-other-input-" + ref);

      formIcon.html('N');
      addButton.fadeOut("fast");
      form.fadeIn("fast");
      formHeader.fadeOut("fast");
      formInput.trigger("reset");
      otherInput.show();

      _key = "";

      $('.mask-decimal').mask('#,##0,00', {
        reverse: true
      });
      $('.mask-money').mask('#,##0', {
        reverse: true
      });
    });

    $(document).on("click", "a.item-form-input-cancel", function(e) {
      e.preventDefault();
      var addButton = $(".item-button-row");
      var form = $(".item-form-row");
      var formHeader = $(".header-form-row");

      addButton.fadeIn("fast");
      form.fadeOut("fast");
      formHeader.fadeIn("fast");
    });

    $(document).on("click", "a.item-form-input-save", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var addButton = $(".item-button-row");
      var form = $(".item-form-row-" + ref);
      var formHeader = $(".header-form-row");
      var formData = $("#item-form-input-" + ref);

      adjustSpinner(ref);
      try {
        $.ajax({
          type: "post",
          url: "<?php echo base_url('penawaran/ajax_save_item/') ?>" + _key,
          data: formData.serialize(),
          success: function(response) {
            var response = JSON.parse(response);

            if (response.status === true) {
              fetchDataPenawaranItem().then(() => {
                addButton.fadeIn("fast");
                form.fadeOut("fast");
                formHeader.fadeIn("fast");
              });

              notify(response.data, "success");
            } else {
              notify(response.data, "danger");
            };
          }
        });
      } catch (error) {
        notify("Failed to save your data.", "danger");
      };
    });

    $(document).on("click", "a.item-button-row-edit", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var is_header = $(this).attr("data-is_header");
      var penawaran_item_parent_id = $(this).attr("data-penawaran_item_parent_id");
      var nomor = $(this).attr("data-nomor");
      var uraian = $(this).attr("data-uraian");
      var volume = $(this).attr("data-volume");
      var satuan = $(this).attr("data-satuan");
      var unit_price = $(this).attr("data-unit_price");
      var total = $(this).attr("data-total");

      var addButton = $(".item-button-row");
      var form = $(".item-form-row-" + ref);
      var formIcon = $(".item-form-row-icon-" + ref);
      var formHeader = $(".header-form-row");
      var formInput = "#item-form-input-" + ref;
      var otherInput = $(".item-other-input-" + ref);

      if (is_header == 1) {
        otherInput.hide();
      } else {
        otherInput.show();
      };

      formIcon.html('E');
      addButton.fadeOut("fast");
      form.fadeIn("fast");
      formHeader.fadeOut("fast");

      // Fill input value
      _key = ref;
      $(formInput).trigger("reset");
      $(formInput + " .penawaran_item-penawaran_item_parent_id-" + ref).val(penawaran_item_parent_id).trigger("input");
      $(formInput + " .penawaran_item-is_header-" + ref).val(is_header).trigger("input");
      $(formInput + " .penawaran_item-nomor-" + ref).val(nomor).trigger("input");
      $(formInput + " .penawaran_item-uraian-" + ref).val(uraian).trigger("input");
      $(formInput + " .penawaran_item-volume-" + ref).val(volume).trigger("input");
      $(formInput + " .penawaran_item-satuan-" + ref).val(satuan).trigger("input");
      $(formInput + " .penawaran_item-unit_price-" + ref).val(unit_price).trigger("input");
      $(formInput + " .penawaran_item-total-" + ref).val(total).trigger("input");

      $('.mask-decimal').mask('#,##0.00', {
        reverse: true
      });
      $('.mask-money').mask('#,##0', {
        reverse: true
      });
    });

    $(document).on("click", "a.item-button-row-delete", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('penawaran/ajax_delete_item/') ?>" + ref,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                fetchDataPenawaranItem();
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    $(document).on("click", "a.item-button-row-bold", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      $.ajax({
        type: "get",
        url: "<?php echo base_url('penawaran/ajax_bold_item/') ?>" + ref + "/1",
        dataType: "json",
        success: function(response) {
          if (response.status) {
            fetchDataPenawaranItem();
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    $(document).on("click", "a.item-button-row-unbold", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      $.ajax({
        type: "get",
        url: "<?php echo base_url('penawaran/ajax_bold_item/') ?>" + ref + "/0",
        dataType: "json",
        success: function(response) {
          if (response.status) {
            fetchDataPenawaranItem();
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    $(document).on("keyup", ".penawaran_item-volume", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      calculateUnitPrice(ref);
    });

    $(document).on("keyup", ".penawaran_item-unit_price", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      calculateUnitPrice(ref);
    });

    $(document).on("click", "a.header-button-row-add", function(e) {
      e.preventDefault();
      var itemButton = $(".item-button-row");
      var addButton = $(".header-button-row-add");
      var formIcon = $(".header-form-row-icon");
      var formInput = $("#header-form-input");

      formIcon.html('N');
      formIcon.fadeIn("fast");
      itemButton.fadeOut("fast");
      addButton.hide();
      formInput.fadeIn("fast");
      formInput.trigger("reset");

      _key = "";
    });

    $(document).on("click", "a.header-form-input-cancel", function(e) {
      e.preventDefault();
      var itemButton = $(".item-button-row");
      var addButton = $(".header-button-row-add");
      var formIcon = $(".header-form-row-icon");
      var formInput = $("#header-form-input");

      itemButton.fadeIn("fast");
      addButton.fadeIn("fast");
      formIcon.fadeOut("fast");
      formInput.fadeOut("fast");
    });

    $(document).on("click", "a.header-form-input-save", function(e) {
      e.preventDefault();
      var itemButton = $(".item-button-row");
      var addButton = $(".header-button-row-add");
      var formInput = $("#header-form-input");

      adjustSpinnerHeader("header");
      try {
        $.ajax({
          type: "post",
          url: "<?php echo base_url('penawaran/ajax_save_item/') ?>" + _key,
          data: formInput.serialize(),
          success: function(response) {
            var response = JSON.parse(response);

            if (response.status === true) {
              fetchDataPenawaranItem().then(() => {
                itemButton.fadeIn("fast");
                addButton.fadeIn("fast");
                formInput.fadeOut("fast");
              });

              notify(response.data, "success");
            } else {
              notify(response.data, "danger");
            };
          }
        });
      } catch (error) {
        notify("Failed to save your data.", "danger");
      };
    });

    async function fetchDataPenawaranItem() {
      var penawaranId = "<?= $penawaran_id ?>";

      await $.ajax({
        type: "get",
        url: "<?php echo base_url('penawaran/ajax_get_penawaran_item_table/') ?>" + penawaranId,
        success: function(response) {
          $("#table-penawaran-item").html(response);
        }
      });
    };

    function calculateUnitPrice(ref) {
      var volume = $(".penawaran_item-volume-" + ref).val().replace(/[^\d.]/g, "");
      volume = (volume != "") ? volume : 0;

      console.log("volume", volume);

      var unitPrice = $(".penawaran_item-unit_price-" + ref).val().replace(/[^\d]/g, "");
      unitPrice = (unitPrice != "") ? unitPrice : 0;

      var total = $(".penawaran_item-total-" + ref);
      var totalTemp = volume * unitPrice;

      if (volume == "" && unitPrice == "") {
        totalTemp = "";
      };

      total.val(totalTemp).trigger("input");
    };

    function adjustSpinner(ref) {
      $(".spinner-" + ref).css("width", $(".item-form-input-wrapper-" + ref).width() + 27);
      $(".spinner-" + ref).css("height", $(".item-form-input-wrapper-" + ref).height() + 27);
      $(".spinner-" + ref).css("margin-top", "-13px");
      $(".spinner-" + ref).css("margin-left", "-13px");
    };

    function adjustSpinnerHeader() {
      $(".spinner-header").css("width", $(".header-form-input-wrapper").width());
      $(".spinner-header").css("height", $(".header-form-input-wrapper").height());
      $(".spinner-header").css("margin-top", "-13px");
      $(".spinner-header").css("margin-left", "-13px");
    };
    // END ## PENAWARAN ITEM

    // Initialize DataTables : List
    if ($("#" + _table_list)[0]) {
      var table_penawaran = $("#" + _table_list).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('penawaran/ajax_get_list/?src=') ?>" + _module,
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "tanggal"
          },
          {
            data: "nomor"
          },
          {
            data: "project_name"
          },
          {
            data: "customer"
          },
          // {
          //   data: "grand_total",
          //   render: function(data, type, row, meta) {
          //     if (data !== null) {
          //       return meta.settings.fnFormatNumber(data.replace(".0000", ""));
          //     } else {
          //       return 0;
          //     };
          //   }
          // },
          {
            data: "status",
            render: function(data, type, row, meta) {
              var statusName = getStatus(data);
              var badgeType = "secondary";
              var revisi = (row.revisi_nomor != null && row.revisi_nomor != "") ? " (Rev. " + row.revisi_nomor + ")" : "";

              switch (data) {
                case "1":
                  badgeType = "warning";
                  break;
                case "2":
                  badgeType = "success";
                  break;
                case "3":
                  badgeType = "danger";
                  break;
                case "4":
                  badgeType = "info";
                  break;
                case "5":
                  badgeType = "danger";
                  break;
                case "6":
                  badgeType = "warning";
                  break;
                case "7":
                  badgeType = "warning";
                  break;
                case "8":
                  badgeType = "warning";
                  break;
                case "9":
                  badgeType = "warning";
                  break;
                default:
                  badgeType = "secondary";
                  break;
              };

              return '<span class="badge badge-' + badgeType + '">' + statusName + revisi + '</span>';
            }
          },
          {
            data: "created_at"
          },
          {
            data: null,
            className: "text-center",
            render: function(data, type, row, meta) {
              var htmlDom = "-";
              var userRole = "<?= strtolower($this->session->userdata('user')['role']) ?>";

              htmlDom = '<div class="action">';

              if (row.status == "0") { // Draft
                // Draft
                htmlDom += '<a href="javascript:;" class="action-view btn btn-light btn-sm mr-1" data-toggle="modal" data-target="#' + _modal_view + '"><i class="zmdi zmdi-eye"></i></a>';
                htmlDom += '<a href="<?= base_url('penawaran/wizard/') ?>' + row.id + '/1" class="action-edit btn btn-secondary btn-sm mr-1"><i class="zmdi zmdi-edit"></i></a>';
                htmlDom += '<a href="javascript:;" class="action-delete btn btn-danger btn-sm"><i class="zmdi zmdi-delete"></i></a>';
              } else if ($.inArray(row.status, ["1", "6", "7", "8", "9"]) !== -1) {
                // Menunggu Persetujuan
                htmlDom += '<a href="javascript:;" class="action-view btn btn-light btn-sm mr-1" data-toggle="modal" data-target="#' + _modal_view + '"><i class="zmdi zmdi-eye"></i></a>';

                if (_module === "approval") {
                  if (row.status == "1" && userRole == "supervisor") {
                    htmlDom += '<a href="javascript:;" class="action-approval btn btn-success btn-sm" data-toggle="modal" data-target="#' + _modal_approval + '">Detail</a>';
                  } else if (row.status == "6" && userRole == "manager") {
                    htmlDom += '<a href="javascript:;" class="action-approval btn btn-success btn-sm" data-toggle="modal" data-target="#' + _modal_approval + '">Detail</a>';
                  } else if (row.status == "7" && userRole == "g. manager") {
                    htmlDom += '<a href="javascript:;" class="action-approval btn btn-success btn-sm" data-toggle="modal" data-target="#' + _modal_approval + '">Detail</a>';
                  } else if (row.status == "8" && userRole == "deputy. dir") {
                    htmlDom += '<a href="javascript:;" class="action-approval btn btn-success btn-sm" data-toggle="modal" data-target="#' + _modal_approval + '">Detail</a>';
                  } else if (row.status == "9" && userRole == "direktur") {
                    htmlDom += '<a href="javascript:;" class="action-approval btn btn-success btn-sm" data-toggle="modal" data-target="#' + _modal_approval + '">Detail</a>';
                  };
                };
              } else if (row.status == "2") {
                // Disetujui
                htmlDom += '<div style="display: flex; flex-direction: column;">';
                htmlDom += '<a href="javascript:;" class="action-view btn btn-light btn-sm mb-2" data-toggle="modal" data-target="#' + _modal_view + '"><i class="zmdi zmdi-eye"></i></a>';
                // htmlDom += '<a href="<?= base_url() ?>' + row.berkas_path + '" class="btn btn-light btn-sm" target="_blank"><i class="zmdi zmdi-download"></i> File</a>';
                // htmlDom += '<small>Upload at ' + row.updated_at + '</small>';
                htmlDom += '</div>';
              } else if (row.status == "3") {
                // Ditolak
                htmlDom += '<a href="javascript:;" class="action-view btn btn-light btn-sm mr-1" data-toggle="modal" data-target="#' + _modal_view + '"><i class="zmdi zmdi-eye"></i></a>';

                if (_module === "approvalresult" && row.created_by == "<?= $this->session->userdata('user')['id'] ?>") {
                  htmlDom += '<a href="<?= base_url('penawaran/wizard/') ?>' + row.id + '/1?src=revisi" class="action-revisi btn btn-secondary btn-sm mr-1">Revisi</a>';
                  htmlDom += '<a href="javascript:;" class="action-delete btn btn-danger btn-sm"><i class="zmdi zmdi-delete"></i></a>';
                };
              } else if (row.status == "4") {
                // Selesai or Disetujui
                htmlDom += '<a href="javascript:;" class="action-view btn btn-light btn-sm mr-1" data-toggle="modal" data-target="#' + _modal_view + '"><i class="zmdi zmdi-eye"></i></a>';

                if (_module === "approvalresult" && row.created_by == "<?= $this->session->userdata('user')['id'] ?>") {
                  htmlDom += '<a href="javascript:;" class="action-cancel btn btn-danger btn-sm" title="Batalkan"><i class="zmdi zmdi-close"></i></a>';
                };
              } else if (row.status == "5") {
                // Dibatalkan
                htmlDom += '<a href="javascript:;" class="action-view btn btn-light btn-sm" data-toggle="modal" data-target="#' + _modal_view + '"><i class="zmdi zmdi-eye"></i></a>';
              } else {
                htmlDom += '<a href="javascript:;" class="action-view btn btn-light btn-sm mr-1" data-toggle="modal" data-target="#' + _modal_view + '"><i class="zmdi zmdi-eye"></i></a>';
              };

              htmlDom += '</div>';

              return htmlDom;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7]
        }, {
          className: 'tablet',
          targets: [0, 2, 3]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        search: {
          search: _listFilter
        },
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_list).DataTable().ajax.reload(null, false);
        };
      });
    };



    // Initialize DataTables : Approved
    if ($("#" + _table_approved)[0]) {
      var table_approved = $("#" + _table_approved).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('penawaran/ajax_get_approved/') ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "tanggal"
          },
          {
            data: "nomor"
          },
          {
            data: "customer"
          },
          {
            data: "customer_attn"
          },
          {
            data: "total",
            render: $.fn.dataTable.render.number(',', '.', 0)
          },
          {
            data: "construction_fee",
            render: $.fn.dataTable.render.number(',', '.', 0)
          },
          {
            data: null,
            render: function(data, type, row, meta) {
              return '<div class="action">' +
                '<a href="javascript:;" class="action-view btn btn-light btn-sm" data-toggle="modal" data-target="#' + _modal_view + '"><i class="zmdi zmdi-eye"></i></a>' +
                '</div>';
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7]
        }, {
          className: 'tablet',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'mobile',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 30,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        // sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        sDom: '<"dataTables__top"fB>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Penawaran",
          exportOptions: {
            columns: [0, 1, 2, 3, 4, 5, 6]
          }
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '<div class="dropdown actions__item"><i data-toggle="dropdown" class="zmdi zmdi-download" /><ul class="dropdown-menu dropdown-menu-right"><a href="" class="dropdown-item" data-table-action="excel">Excel (.xlsx)</a></ul></div>' +
            '</div>'
          );
        },
        order: [
          [1, "asc"],
          [3, "asc"],
        ],
        rowsGroup: [3],
        rowGroup: {
          startRender: null,
          endRender: function(rows, group) {
            var amount_total = rows
              .data()
              .reduce(function(a, b) {
                return a + b.total * 1;
              }, 0);
            amount_total = $.fn.dataTable.render.number(',', '.', 0, '').display(amount_total);

            var ppn_total = rows
              .data()
              .reduce(function(a, b) {
                return a + b.construction_fee * 1;
              }, 0);
            ppn_total = $.fn.dataTable.render.number(',', '.', 0, '').display(ppn_total);

            return $('<tr/>')
              .append('<td colspan="5" class="row-group">Sub total for ' + group + '</td>')
              .append('<td class="row-group">' + amount_total + '</td>')
              .append('<td colspan="2" class="row-group">' + ppn_total + '</td>');
          },
          dataSrc: "customer"
        }
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if (
          ("excel" === b && $(this).closest(".dataTables_wrapper").find(".buttons-excel").trigger("click"),
            "reload" === b)
        ) {
          $("#" + _table_approved).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data submit (Skip input item uraian, save as draft)
    $("#" + _form_penawaran + " button.penawaran-action-save-draft").on("click", function(e) {
      e.preventDefault();
      var activeForm = new FormData($("#" + _form_penawaran)[0]);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('penawaran/ajax_save/') ?>" + _penawaranId,
        data: activeForm,
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            notify(response.data, "success");

            setTimeout(function() {
              location.href = "<?= base_url('penawaran/draft') ?>";
            }, 2000);
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data submit (Skip input item uraian, bypass to PIC)
    // $("#" + _form_penawaran + " button.penawaran-action-save").on("click", function(e) {
    //   e.preventDefault();
    //   var activeForm = new FormData($("#" + _form_penawaran)[0]);

    //   swal({
    //     title: "Are you sure to submit?",
    //     text: "Once submited, you will not be able to recover this data!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: '#DD6B55',
    //     confirmButtonText: "Yes",
    //     cancelButtonText: "No",
    //     closeOnConfirm: false
    //   }).then((result) => {
    //     if (result.value) {
    //       $.ajax({
    //         type: "post",
    //         url: "<?php echo base_url('penawaran/ajax_save_bypass/') ?>" + _penawaranId,
    //         data: activeForm,
    //         enctype: "multipart/form-data",
    //         processData: false,
    //         contentType: false,
    //         cache: false,
    //         success: function(response) {
    //           var response = JSON.parse(response);
    //           if (response.status === true) {
    //             notify(response.data, "success");

    //             setTimeout(function() {
    //               location.href = "<?= base_url('penawaran') ?>";
    //             }, 2000);
    //           } else {
    //             notify(response.data, "danger");
    //           };
    //         }
    //       });
    //     };
    //   });
    // });

    // Handle data submit
    $("#" + _form_penawaran + " button.penawaran-action-save").on("click", function(e) {
      e.preventDefault();
      var activeForm = new FormData($("#" + _form_penawaran)[0]);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('penawaran/ajax_save/') ?>" + _penawaranId,
        data: activeForm,
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            notify(response.data, "success");

            setTimeout(function() {
              location.href = "<?= base_url('penawaran/wizard/') ?>" + response.data_id + "/2";
            }, 1000);
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table_list).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_penawaran.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('penawaran/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_list).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data cancel
    $("#" + _table_list).on("click", "a.action-cancel", function(e) {
      e.preventDefault();
      var temp = table_penawaran.row($(this).closest('tr')).data();

      Swal({
        title: "Anda akan membatalkan penawaran, lanjutkan?",
        text: "Proses pembatalan hanya dapat dilakukan dengan catatan, silahkan masukan catatan :",
        input: "text",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "post",
            url: "<?php echo base_url('penawaran/ajax_cancel/') ?>" + temp.id,
            data: {
              cancel_note: result.value
            },
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_list).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle view
    $("#" + _table_list).on("click", "a.action-view", function(e) {
      e.preventDefault();
      var temp = table_penawaran.row($(this).closest('tr')).data();

      $("#" + _modal_view + " .approval-preview").html("Please wait...");

      $.ajax({
        type: "get",
        url: "<?= base_url('penawaran/ajax_get_preview/') ?>" + temp.id,
        success: function(response) {
          $(".approval-preview").html(response);
          $(".modal-penawaran-view-title").html("Penawaran : " + getStatus(temp.status));
        }
      });

      _modal_source = "view";
    });
    $("#" + _table_approved).on("click", "a.action-view", function(e) {
      e.preventDefault();
      var temp = table_approved.row($(this).closest('tr')).data();

      $("#" + _modal_view + " .approval-preview").html("Please wait...");

      $.ajax({
        type: "get",
        url: "<?= base_url('penawaran/ajax_get_preview/') ?>" + temp.id,
        success: function(response) {
          $(".approval-preview").html(response);
          $(".modal-penawaran-view-title").html("Penawaran : " + getStatus(temp.status));
        }
      });

      _modal_source = "view";
    });

    // Submit filter
    $("#" + _section + " .page-action-filter").on("click", function(e) {
      var month = $("#" + _section + " .filter-month").val();
      var year = $("#" + _section + " .filter-year").val();

      table_approved.ajax.url("<?php echo base_url('penawaran/ajax_get_approved/') ?>" + month + "/" + year);
      table_approved.clear().draw();
    });

    // Handle view riwayat
    $(document).on("click", "a.action-penawaran-riwayat", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var status = $(this).attr("data-status");

      $("#" + _modal_view_riwayat + " .riwayat-preview").html("Please wait...");

      $.ajax({
        type: "get",
        url: "<?= base_url('penawaran/ajax_get_preview_riwayat/') ?>" + ref,
        success: function(response) {
          $(".riwayat-preview").html(response);
          $(".modal-penawaran-view-riwayat-title").html("Riwayat Penawaran : " + getStatus(status));
        }
      });

      if (_modal_source === "view") {
        $("#" + _modal_view).modal("hide");
      } else if (_modal_source === "approval") {
        $("#" + _modal_approval).modal("hide");
      };
    });

    // Handle view riwayat cancel
    $(document).on("click", "button.action-penawaran-riwayat-cancel", function() {
      if (_modal_source === "view") {
        $("#" + _modal_view).modal("show");
      } else if (_modal_source === "approval") {
        $("#" + _modal_approval).modal("show");
      };
    });

    // Handle approval
    $("#" + _table_list).on("click", "a.action-approval", function(e) {
      e.preventDefault();
      var temp = table_penawaran.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $("#" + _form_approval + " .approval-packing_order_id").val(_key);
      $("#" + _modal_approval + " .approval-preview").html("Please wait...");

      $.ajax({
        type: "get",
        url: "<?= base_url('penawaran/ajax_get_preview/') ?>" + _key,
        success: function(response) {
          $(".approval-preview").html(response);
        }
      });

      _modal_source = "approval";
    });

    // Handle submit approve
    $(".approval-action-approve").on("click", function(e) {
      e.preventDefault();
      var action = "Approve";
      var note = $(".approval-note");

      swal({
        title: "Confirm",
        text: "Are you sure to approve?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "post",
            url: "<?php echo base_url('penawaran/ajax_save_approval/4/') ?>" + _key,
            data: $("#" + _form_approval).serialize(),
            success: function(response) {
              var response = JSON.parse(response);
              if (response.status === true) {
                $("#" + _modal_approval).modal("hide");
                $("#" + _table_list).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle submit reject
    $(".approval-action-reject").on("click", function(e) {
      var action = "Reject";
      var note = $(".approval-note");

      if (note.val().trim() != "") {
        swal({
          title: "Confirm",
          text: "Are you sure to reject?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false
        }).then((result) => {
          if (result.value) {
            $.ajax({
              type: "post",
              url: "<?php echo base_url('penawaran/ajax_save_approval/3/') ?>" + _key,
              data: $("#" + _form_approval).serialize(),
              success: function(response) {
                var response = JSON.parse(response);
                if (response.status === true) {
                  $("#" + _modal_approval).modal("hide");
                  $("#" + _table_list).DataTable().ajax.reload(null, false);
                  notify(response.data, "success");
                } else {
                  notify(response.data, "danger");
                };
              }
            });
          };
        });
      } else {
        note.focus();
        notify("Please fill the note field.", "danger");
      };
    });

    // Handle upload
    $("#" + _table_list).on("click", "a.action-upload", function(e) {
      e.preventDefault();
      var temp = table_penawaran.row($(this).closest('tr')).data();

      $("#" + _form_upload + " .penawaran-id").val(temp.id);
    });

    // Handle submit / sent
    $(document).on("click", "button.penawaran-action-sent", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      swal({
        title: "Are you sure to submit?",
        text: "Once submited, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "get",
            url: "<?php echo base_url('penawaran/ajax_set_status/') ?>" + ref + "/1",
            dataType: "json",
            success: function(response) {
              if (response.status) {
                notify(response.data, "success");

                setTimeout(function() {
                  location.href = "<?= base_url('penawaran') ?>";
                }, 2000);
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data submit upload
    $("#" + _modal_upload + " .penawaran-action-save-upload").on("click", function(e) {
      e.preventDefault();

      var form = $("#" + _form_upload)[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('penawaran/ajax_save_upload/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            $("#" + _modal_upload).modal("hide");
            $("#" + _table_list).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle upload
    $(".penawaran-file").change(function() {
      readUploadInlineDocURL(this);
    });

    // Handle approval role
    $(document).on("change", ".penawaran-approval_role", function() {
      var role = $(this).val();
      var picId = $(".penawaran-approval_user_id");
      var picName = $(".penawaran-approval_name");
      var picList = "";

      // Empty list first
      picId.html("");
      picName.val("");

      $.ajax({
        type: "post",
        url: "<?php echo base_url('penawaran/ajax_get_approval/') ?>",
        data: {
          "role": role
        },
        dataType: "json",
        success: function(response) {
          if (response.length > 0) {
            picList += `<option disabled selected>Select &#8595;</option>`;

            response.map((item, index) => {
              picList += `<option value="${item.id}">${item.nama_lengkap}</option>`;
            });
          } else {
            picList += `<option disabled selected>(No data available)</option>`;
          };
          picId.append(picList);
        }
      });
    });

    // Handle approve role PIC changed
    $(document).on("change", ".penawaran-approval_user_id", function() {
      var value = $(".penawaran-approval_user_id option:selected").text();
      $(".penawaran-approval_name").val(value);
    });

    // Handle penawaran print
    $(document).on("click", "button.action-penawaran-print", function(e) {
      e.preventDefault();
      $(".penawaran-print-area").printThis();
    });

    // Handle penawaran print
    $(document).on("click", "button.action-penawaran-riwayat-print", function(e) {
      e.preventDefault();
      $(".penawaran-riwayat-print-area").printThis();
    });

    // Attachment
    // Handle add input
    $("#" + _section + " .attachment-add").on("click", function() {
      var key = (randomString(10) + Math.floor(Date.now() / 1000)).toString();
      var inputFile = '<div class="upload-inline-xs attachment-input-' + key + '" style="margin-top: 10px;"><div class="upload-button"><input type="file" name="file_name[]" class="upload-pure-button attachment-file" data-preview="' + key + '" accept="image/*,application/pdf" /></div><div class="upload-preview data-preview-' + key + '">No file chosen</div><div class="upload-action data-action-' + key + '"><a href="javascript:;" class="link-black attachment-delete" data-action="' + key + '"><small><i class="zmdi zmdi-close-circle"></i> Delete</small></a></div></div>';
      $(".attachment-input-wrapper").append(inputFile).children(':last').hide().fadeIn("fast");
    });

    // Handle delete input
    $(document.body).on("click", ".attachment-delete", function() {
      var key = $(this).attr("data-action");
      $(".attachment-input-" + key).fadeOut("fast", function() {
        $(this).remove();
      });
    });

    // Handle delete existing
    $(document.body).on("click", "a.attachment-delete-existing", function(e) {
      e.preventDefault();
      var id = $(this).attr("data-id");

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('penawaran/ajax_delete_attachment/') ?>" + id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $(".attachment-data-item-" + id).fadeOut("fast", function() {
                  $(this).remove();
                });
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle upload
    $(document.body).on("change", ".attachment-file", function() {
      readUploadMultipleDocURLXs(this);
    });
    // END ## Attachment

    // Import Items
    // Handle data submit import
    $(document).on("click", "#modal-import-item .item-action-save-import", function(e) {
      e.preventDefault();

      var form = $("#form-item-import")[0];
      var data = new FormData(form);

      $.ajax({
        type: "post",
        url: "<?php echo base_url('penawaran/ajax_import_item/') ?>",
        data: data,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        success: function(response) {
          if (response.status === true) {
            $("#modal-import-item").modal("hide");
            notify(response.data, "success");

            setTimeout(function() {
              location.href = "";
            }, 500);
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle upload
    $(document).on("change", ".item-file", function() {
      readUploadInlineDocURL(this);
    });
    // END ## Import Items
  });

  function getStatus(status) {
    switch (status.toString()) {
      case '0':
        return 'Draft';
        break;
      case '1':
        return 'Menunggu Persetujuan SPV';
        break;
      case '2':
        return 'Disetujui';
        break;
      case '3':
        return 'Ditolak';
        break;
      case '4':
        return 'Disetujui';
        break;
      case '5':
        return 'Dibatalkan';
        break;
      case '6':
        return 'Menunggu Persetujuan Manager';
        break;
      case '7':
        return 'Menunggu Persetujuan GM';
        break;
      case '8':
        return 'Menunggu Persetujuan Deputy. Dir';
        break;
      case '9':
        return 'Menunggu Persetujuan Direktur';
        break;
      default:
        return 'Undefined';
        break;
    };
  };

  function randomString(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;

    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    };

    return result;
  };
</script>