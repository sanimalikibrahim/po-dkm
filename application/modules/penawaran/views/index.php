<?php require_once('main.css.php') ?>
<?php include_once('_view_modal.php') ?>
<?php include_once('_view_riwayat_modal.php') ?>
<?php include_once('_approval_form.php') ?>
<?php include_once('_upload_form.php') ?>

<section id="penawaran">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <?php if ($module === 'draft') : ?>
                <div class="table-action">
                    <div class="buttons">
                        <a href="<?= base_url('penawaran/wizard') ?>" class="btn btn--raised btn-primary btn--icon-text">
                            <i class="zmdi zmdi-plus"></i> Add New
                        </a>
                    </div>
                </div>
            <?php endif ?>

            <div class="table-responsive">
                <table id="table-list" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Tanggal</th>
                            <th>Nomor</th>
                            <th>Project Name</th>
                            <th>Customer</th>
                            <!-- <th>Grand Total</th> -->
                            <th>Status</th>
                            <th>Created At</th>
                            <th width="100" style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>