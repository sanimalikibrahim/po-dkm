<div class="modal fade" id="modal-form-upload" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Upload File</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <div class="alert alert-info">
          Mengunggah berkas akan merubah status menjadi "Selesai"!
        </div>

        <form id="form-penawaran-upload">
          <!-- Temporary field -->
          <input type="hidden" name="penawaran_id" class="penawaran-id" readonly />

          <div class="form-group">
            <label required>File</label>
            <div class="upload-inline">
              <div class="upload-button">
                <input type="file" name="file" class="upload-pure-button penawaran-file" />
              </div>
              <div class="upload-preview">
                No file chosen
              </div>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required. <br />
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text penawaran-action-save-upload">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text penawaran-action-cancel-upload" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>