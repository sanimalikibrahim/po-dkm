<?php require_once('main.css.php') ?>
<?php $revisiNomor = null ?>
<?php $revisiNomorLabel = null ?>

<section id="penawaran">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute;">
                        <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                            <?php $revisiNomor = (!is_null($penawaran->revisi_nomor)) ? (int) $penawaran->revisi_nomor + 1 : 1; ?>
                            REV
                        <?php else : ?>
                            <?php $revisiNomorLabel = (isset($penawaran->revisi_nomor) && !is_null($penawaran->revisi_nomor)) ? ' : Rev. ' . $penawaran->revisi_nomor : null; ?>
                            <?= (isset($penawaran->id)) ? 'EDIT' : 'NEW' ?>
                        <?php endif ?>
                    </span>
                    <div style="margin-left: 60px; margin-top: 5px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title . $revisiNomorLabel : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <!-- Penawaran -->
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Department :</b></div>
                        <div class="col-xs-12 col-auto"><?= isset($penawaran->department) ? $penawaran->department : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Nomor :</b></div>
                        <div class="col-xs-12 col-auto"><?= isset($penawaran->nomor) ? $penawaran->nomor : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Date :</b></div>
                        <div class="col-xs-12 col-auto"><?= isset($penawaran->tanggal) ? $penawaran->city . ', ' . $penawaran->tanggal : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Project Name :</b></div>
                        <div class="col-xs-12 col-auto"><?= isset($penawaran->project_name) ? $penawaran->project_name : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Customer :</b></div>
                        <div class="col-xs-12 col-auto"><?= isset($penawaran->customer) ? $penawaran->customer : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Attachment :</b></div>
                        <div class="col-xs-12 col-auto">
                            <div style="display: flex;">
                                <div class="form-group" style="margin-bottom: 0px; flex: 1;">
                                    <div class="attachment" style="margin-top: 0px;">
                                        <?php if (count($penawaran_attachment) > 0) : ?>
                                            <?php foreach ($penawaran_attachment as $key => $item) : ?>
                                                <?php $isImage = (strpos($item->file_type, 'image') !== false) ? true : false ?>
                                                <div class="attachment-item attachment-data-item-<?php echo $item->id ?>">
                                                    <div class="row">
                                                        <div class="col-auto">
                                                            <?php if ($isImage) : ?>
                                                                <img src="<?php echo base_url($item->file_name_thumb) ?>" class="attachment-preview attachment-preview-img" />
                                                            <?php else : ?>
                                                                <div class="attachment-preview attachment-preview-file">
                                                                    <i class="zmdi zmdi-file-text"></i>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-auto" style="padding-left: 0;">
                                                            <div style="<?php echo ($is_mobile) ? 'text-overflow: ellipsis; overflow: hidden; width: 150px; height: 1.2em; white-space: nowrap;' : '' ?>">
                                                                <a href="<?php echo base_url($item->file_name) ?>" target="_blank">
                                                                    <span><?php echo $item->file_raw_name ?></span>
                                                                </a>
                                                            </div>
                                                            <span class="attachment-size">
                                                                <?php echo $item->file_size ?> KB
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php else : ?>
                                            <label>No data available</label>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <!-- END ## Penawaran -->

            <div class="mb-4"></div>
            <div id="table-penawaran-item">
                <?php include_once("_table_item.php"); ?>
            </div>

            <small class="form-text text-muted">
                Fields with red stars (<label required></label>) are required.
            </small>

            <div class="row">
                <div class="col">
                    <div class="buttons-container">
                        <div class="row">
                            <div class="col">
                                <a href="<?= base_url('penawaran/wizard/' . $penawaran->id . '/1') ?>" class="btn btn--raised btn-light btn--icon-text btn-custom mr-2">
                                    <i class="zmdi zmdi-long-arrow-left"></i>
                                    Back
                                </a>
                            </div>
                            <div class="col text-right">
                                <a href="<?= base_url('penawaran/draft') ?>" class="btn btn--raised btn-primary btn--icon-text btn-custom spinner-action-button">
                                    <i class="zmdi zmdi-save"></i>
                                    Save as Draft
                                </a>
                                <button class="btn btn--raised btn-success btn--icon-text btn-custom penawaran-action-sent spinner-action-button" data-id="<?= $penawaran->id ?>">
                                    Submit
                                    <i class="zmdi zmdi-long-arrow-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>