<div class="modal fade" id="modal-import-item" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Import Item</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">Penting! Data item akan dikosongkan terlebih dahulu.</div>

        <form id="form-item-import">

          <!-- Hidden temp -->
          <input type="hidden" name="penawaran_id" value="<?= $penawaran->id ?>" readonly />

          <div class="form-group">
            <label required>File</label>
            <a href="javascript:;" data-toggle="popover" data-trigger="focus" data-content="Use excel (.xlsx) format">
              <span class="zmdi zmdi-help"></span>
            </a>
            <div class="upload-inline">
              <div class="upload-button">
                <input type="file" name="file" class="upload-pure-button item-file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
              </div>
              <div class="upload-preview">
                No file chosen
              </div>
            </div>
            <div class="margin-top-10">
              <a href="<?php echo base_url('directory/templates/po-dkm-penawaran.xlsx') ?>" download="template-po-dkm-penawaran.xlsx">
                <i class="zmdi zmdi-download"></i> Download Template
              </a>
              <br>
              <a href="<?php echo base_url('directory/templates/po-dkm-penawaran-sample.xlsx') ?>" download="sample-po-dkm-penawaran.xlsx">
                <i class="zmdi zmdi-download"></i> Download Sample Data
              </a>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required. <br />
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text item-action-save-import">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text item-action-cancel-import" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>