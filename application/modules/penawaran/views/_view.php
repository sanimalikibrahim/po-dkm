<?php require_once('main.css.php') ?>

<div class="hidden-print">
  <div id="penawaran-other-wrapper">
    <!-- Persetujuan -->
    <?php if (in_array($penawaran->status, array(2, 3, 4))) : ?>
      <div class="card m-0">
        <div class="card-header p-1" id="penawaran-persetjuan">
          <h5 class="mb-0">
            <button class="btn btn-link" data-toggle="collapse" data-target="#persetjuan-list" aria-expanded="true" aria-controls="persetjuan-list">
              <i class="zmdi zmdi-account mr-1" style="width: 10px; text-align: left;"></i>
              Persetujuan
            </button>
          </h5>
        </div>
        <div id="persetjuan-list" class="collapse show" aria-labelledby="penawaran-persetjuan" data-parent="#penawaran-other-wrapper">
          <div class="card-body p-3">
            <table class="m-0 p-0">
              <tr>
                <td width="60" valign="top">Status</td>
                <td width="10" valign="top">:</td>
                <td valign="top">
                  <?= (isset($penawaran->status)) ? $controller->get_status($penawaran->status) : null ?>
                  <?= (isset($penawaran->approval_date)) ? '(' . $penawaran->approval_date . ')' : null ?>
                </td>
              </tr>
              <tr>
                <td width="60" valign="top">PIC</td>
                <td width="10" valign="top">:</td>
                <td valign="top">
                  <?= (isset($penawaran->approval_name)) ? $penawaran->approval_name : null ?>
                  <?= (isset($penawaran->approval_role)) ? '/ ' . $penawaran->approval_role : null ?>
                </td>
              </tr>
              <tr>
                <td width="60" valign="top">Catatan</td>
                <td width="10" valign="top">:</td>
                <td valign="top"><?= (isset($penawaran->approval_note)) ? str_replace("\r\n", '<br/>', htmlspecialchars_decode($penawaran->approval_note)) : null ?></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    <?php endif ?>
    <!-- Pembatalan -->
    <?php if (in_array($penawaran->status, array(5))) : ?>
      <div class="card m-0">
        <div class="card-header p-1" id="penawaran-persetjuan">
          <h5 class="mb-0">
            <button class="btn btn-link" data-toggle="collapse" data-target="#persetjuan-list" aria-expanded="true" aria-controls="persetjuan-list">
              <i class="zmdi zmdi-account mr-1" style="width: 10px; text-align: left;"></i>
              Pembatalan
            </button>
          </h5>
        </div>
        <div id="persetjuan-list" class="collapse show" aria-labelledby="penawaran-persetjuan" data-parent="#penawaran-other-wrapper">
          <div class="card-body p-3">
            <table class="m-0 p-0">
              <tr>
                <td width="60" valign="top">Tanggal</td>
                <td width="10" valign="top">:</td>
                <td valign="top">
                  <?= (isset($penawaran->cancel_at)) ? $penawaran->cancel_at : null ?>
                </td>
              </tr>
              <tr>
                <td width="60" valign="top">PIC</td>
                <td width="10" valign="top">:</td>
                <td valign="top">
                  <?= (isset($penawaran->cancel_by)) ? $penawaran->cancel_by : null ?>
                </td>
              </tr>
              <tr>
                <td width="60" valign="top">Catatan</td>
                <td width="10" valign="top">:</td>
                <td valign="top"><?= (isset($penawaran->cancel_note)) ? str_replace("\r\n", '<br/>', htmlspecialchars_decode($penawaran->cancel_note)) : null ?></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    <?php endif ?>
    <!-- Riwayat Revisi -->
    <?php if (count($penawaran_arsip) > 0) : ?>
      <div class="card m-0">
        <div class="card-header p-1" id="penawaran-riwayat">
          <h5 class="mb-0">
            <button class="btn btn-link" data-toggle="collapse" data-target="#riwayat-list" aria-expanded="true" aria-controls="riwayat-list">
              <i class="zmdi zmdi-file-text mr-1" style="width: 10px; text-align: left;"></i> Riwayat Revisi
            </button>
          </h5>
        </div>
        <div id="riwayat-list" class="collapse" aria-labelledby="penawaran-riwayat" data-parent="#penawaran-other-wrapper">
          <div class="card-body p-3">
            <ul class="pl-4 m-0">
              <?php foreach ($penawaran_arsip as $index => $item) : ?>
                <li>
                  <a href="javascript:;" data-id="<?= $item->id ?>" data-status="<?= $item->status ?>" class="action-penawaran-riwayat" data-toggle="modal" data-target="#modal-penawaran-view-riwayat">
                    Rev. <?= $item->revisi_nomor ?> (<?= $item->created_at ?>)
                  </a>
                </li>
              <?php endforeach ?>
            </ul>
          </div>
        </div>
      </div>
    <?php endif ?>
    <!-- Attachment -->
    <?php if (isset($is_attachment_visible) && $is_attachment_visible === true) : ?>
      <div class="card m-0">
        <div class="card-header p-1" id="penawaran-attachment">
          <h5 class="mb-0">
            <button class="btn btn-link" data-toggle="collapse" data-target="#attachment-list" aria-expanded="true" aria-controls="attachment-list">
              <i class="zmdi zmdi-attachment mr-1" style="width: 10px; text-align: left;"></i>
              Attachment (<?= count($penawaran_attachment) ?>)
            </button>
          </h5>
        </div>
        <div id="attachment-list" class="collapse" aria-labelledby="penawaran-attachment" data-parent="#penawaran-other-wrapper">
          <div class="card-body p-3">
            <div style="display: flex;">
              <div class="form-group" style="margin-bottom: 0px; flex: 1;">
                <div class="attachment p-0" style="margin-top: 0px; background: #fff;">
                  <?php if (count($penawaran_attachment) > 0) : ?>
                    <?php foreach ($penawaran_attachment as $key => $item) : ?>
                      <?php $isImage = (strpos($item->file_type, 'image') !== false) ? true : false ?>
                      <div class="attachment-item attachment-data-item-<?php echo $item->id ?>">
                        <div class="row">
                          <div class="col-auto">
                            <?php if ($isImage) : ?>
                              <img src="<?php echo base_url($item->file_name_thumb) ?>" class="attachment-preview attachment-preview-img" />
                            <?php else : ?>
                              <div class="attachment-preview attachment-preview-file">
                                <i class="zmdi zmdi-file-text"></i>
                              </div>
                            <?php endif; ?>
                          </div>
                          <div class="col-auto" style="padding-left: 0;">
                            <div style="<?php echo ($is_mobile) ? 'text-overflow: ellipsis; overflow: hidden; width: 150px; height: 1.2em; white-space: nowrap;' : '' ?>">
                              <a href="<?php echo base_url($item->file_name) ?>" target="_blank">
                                <span><?php echo $item->file_raw_name ?></span>
                              </a>
                            </div>
                            <span class="attachment-size">
                              <?php echo $item->file_size ?> KB
                            </span>
                          </div>
                        </div>
                      </div>
                    <?php endforeach; ?>
                  <?php else : ?>
                    <label>No data available</label>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php endif ?>
  </div>
  <?php if (in_array($penawaran->status, array(2, 3, 4)) || count($penawaran_arsip) > 0 || (isset($is_attachment_visible) && $is_attachment_visible === true)) : ?>
    <hr>
  <?php endif ?>
</div>

<div class="preview-po" style="border: 0; padding: 0;">
  <div class="preview-header">
    <table class="table-header">
      <!-- Kop Surat -->
      <tr>
        <td colspan="2">
          <table class="table-header">
            <td style="width: 80px;">
              <img src="<?php echo base_url('themes/_public/img/logo/dkm-black.png') ?>" class="logo" />
            </td>
            <td style="padding-left: 20px; padding-right: 20px;">
              <div class="kop-surat">
                <h5>PT. DHARMA KARYATAMA MULIA</h5>
                <span>Jl. Raya Bogor Km 29, Gandaria, Ps. Rebo - Jakarta 13710, Indonesia</span> <br />
                <span>Phone : +62 21 8719964-5, Fax : +62 21 8727018</span> <br />
                <span>Email : pt_dkm@dkm.co.id, Web : www.dkm.co.id</span>
              </div>
            </td>
            <td style="width: 80px; text-align: right;">
              <img src="<?php echo base_url('themes/_public/img/logo/sics.png') ?>" class="logo" />
            </td>
          </table>
          <hr class="double-line">
        </td>
      </tr>
      <!-- END ## Kop Surat -->
      <tr>
        <td colspan="2" align="right">
          <span style="font-size: 12px;"><?= (isset($penawaran->city)) ? $penawaran->city : null ?>, <?= (isset($penawaran->tanggal)) ? $controller->localizeDate($penawaran->tanggal) : null ?></span>
        </td>
      </tr>
      <tr>
        <td valign="top">
          <table class="table-header">
            <tr>
              <td width="65" style="font-size: 12px;">No</td>
              <td width="10" style="font-size: 12px;">:</td>
              <td style="font-size: 12px;"><?= (isset($penawaran->nomor)) ? $penawaran->nomor : null ?></td>
            </tr>
            <tr>
              <td width="65" style="font-size: 12px;">Perihal</td>
              <td width="10" style="font-size: 12px;">:</td>
              <td style="font-size: 12px;">Penawaran Harga</td>
            </tr>
            <tr>
              <td width="65" style="font-size: 12px;">Proyek</td>
              <td width="10" style="font-size: 12px;">:</td>
              <td style="font-size: 12px;"><?= (isset($penawaran->project_name)) ? $penawaran->project_name : null ?></td>
            </tr>
          </table>
          <div style="margin-bottom: 15px;"></div>
        </td>
      </tr>
      <tr>
        <td valign="top">
          <div class="kop-surat">
            <span style="font-size: 12px;"><b><?= (isset($penawaran->department)) ? $penawaran->department : null ?></b></span> <br>
            <span style="font-size: 12px;"><?= (isset($penawaran->customer)) ? $penawaran->customer : null ?></span> <br>
            <span style="font-size: 12px;"><?= (isset($penawaran->customer_address1)) ? $penawaran->customer_address1 : null ?></span> <br>
            <span style="font-size: 12px;"><b><?= (isset($penawaran->customer_address2)) ? $penawaran->customer_address2 : null ?></b></span>
          </div>
        </td>
      </tr>
    </table>
    <div style="margin-bottom: 15px;"></div>
  </div>
  <div class="preview-body">
    <table style="width: 100%; margin-bottom: 15px;">
      <tr>
        <td style="font-size: 12px;">
          Kepada Yth. <br>
          <?= (isset($penawaran->customer_attn)) ? $penawaran->customer_attn : null ?>
        </td>
        <!-- Revisi Nomor -->
        <?php if (isset($penawaran->revisi_nomor) && !is_null($penawaran->revisi_nomor) && $penawaran->revisi_nomor != 0) : ?>
          <td style="font-size: 12px; width: 80px; text-align: center; background: yellow; border: 1px solid #8c8b8b;">
            <b>Revisi <?= $penawaran->revisi_nomor ?></b>
          </td>
        <?php else : ?>
          <td>&nbsp;</td>
        <?php endif ?>
        <!-- END ## Revisi Nomor -->
      </tr>
      <tr>
        <td colspan="2">
          <div style="margin-bottom: 15px;"></div>
          <span style="font-size: 12px;">
            Dengan hormat, <br>
            Bersama dengan ini kami mengajukan surat penawaran harga kepada Bapak/Ibu atas <br>
            produk-produk kami sebagai uraian dibawah ini :
          </span>
        </td>
      </tr>
    </table>

    <table class="table-order-item" style="border-collapse: collapse;">
      <thead>
        <tr>
          <th width="50">NO</th>
          <th>DESCRIPTION</th>
          <th width="130">QTY</th>
          <th width="130">PRICE LIST (IDR)</th>
          <th width="130">AMOUNT (IDR)</th>
        </tr>
      </thead>
      <tbody>
        <?php // $total = 0 
        ?>
        <?php if (isset($penawaran_item) && count($penawaran_item) > 0) : ?>
          <?php $no = 1 ?>
          <?php foreach ($penawaran_item as $key => $item) : ?>
            <?php
            // $total = (!is_null($item->total) && !empty($item->total)) ? (float) $item->total + $total : 0 + $total;
            $isBoldStyle = ($item->is_bold == 1) ? 'font-weight: bold;' : '';
            ?>
            <?php if ($item->is_header == 1) : ?>
              <tr>
                <td colspan="6" style="height: 15px;"></td>
              </tr>
            <?php endif ?>
            <tr>
              <td valign="top" align="center" style="<?= $isBoldStyle ?>">
                <?= ($item->is_header == 1) ? '<b style="font-weight: bold;">' . $item->nomor . '</b>' : $item->nomor ?>
              </td>
              <td valign="top" colspan="<?= ($item->is_header == 1) ? 5 : 0 ?>" style="<?= $isBoldStyle ?>">
                <?= ($item->is_header == 1) ? '<b style="font-weight: bold;">' . $item->uraian . '</b>' : $item->uraian ?>
              </td>
              <?php if ($item->is_header == 0) : ?>
                <td valign="top" style="text-align: center; <?= $isBoldStyle ?>">
                  <?= (!is_null($item->volume)) ? number_format($item->volume, 2) : null ?> &nbsp;
                  <?= $item->satuan ?>
                </td>
                <td valign="top" style="text-align: right; <?= $isBoldStyle ?>"><?= (!is_null($item->unit_price)) ? number_format($item->unit_price, 0) : null ?></td>
                <td valign="top" style="text-align: right; <?= $isBoldStyle ?>"><?= (!is_null($item->total)) ? number_format($item->total, 0) : null ?></td>
              <?php endif ?>
            </tr>
          <?php endforeach; ?>
        <?php else : ?>
          <tr>
            <td colspan="5" style="padding: 15px;">
              No data available in table
            </td>
          </tr>
        <?php endif; ?>
      </tbody>
      <!-- <tfoot>
        <tr>
          <td colspan="7" class="p-0" style="height: 15px;"></td>
        </tr>
        <tr>
          <th colspan="5" style="text-align: right;">Total</th>
          <th style="text-align: right;"><?= number_format($total) ?></th>
        </tr>
        <tr>
          <th colspan="5" style="text-align: right;">Construction Fee 10%</th>
          <th style="text-align: right;"><?= number_format(($total / 100) * 10) ?></th>
        </tr>
        <tr>
          <th colspan="5" style="text-align: right;">Grand Total</th>
          <th style="text-align: right;"><?= number_format($total + (($total / 100) * 10)) ?></th>
        </tr>
      </tfoot> -->
    </table>
    <div style="margin-bottom: 15px;"></div>

    <?php if (!is_null($penawaran->note) && !empty($penawaran->note)) : ?>
      <span style="font-size: 12px;"><b><u><i>Keterangan :</i></u></b></span>
      <div style="font-size: 12px;"><?= (isset($penawaran->note)) ? str_replace("\r\n", '<br/>', htmlspecialchars_decode($penawaran->note)) : null ?></div>
      <div style="margin-bottom: 15px;"></div>
    <?php endif ?>

    <span style="font-size: 12px;">
      Besar harapan kami untuk menjalin kerjasama dengan perusahaan Bapak/Ibu, atas perhatian <br>
      dan kerjasama yang baik kami ucapkan terima kasih.
    </span>
    <div style="margin-bottom: 30px;"></div>

    <!-- TTD -->
    <?php if ($penawaran->status == 3) : ?>
      <table class="table-order-item mb-0" style="width: 300px;">
        <tbody>
          <tr>
            <td valign="center" align="center" class="ttd">
              Penawaran Ditolak
            </td>
          </tr>
        </tbody>
      </table>
    <?php elseif ($penawaran->status == 5) : ?>
      <table class="table-order-item mb-0" style="width: 300px;">
        <tbody>
          <tr>
            <td valign="center" align="center" class="ttd">
              Penawaran Dibatalkan
            </td>
          </tr>
        </tbody>
      </table>
    <?php else : ?>
      <table class="table-order-item mb-0" style="width: 300px;">
        <thead>
          <tr>
            <th><?= $controller->get_status($penawaran->status) ?></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td valign="center" align="center" class="ttd">
              <?php
              if (in_array($penawaran->status, array(2, 3, 4))) {
                echo (isset($penawaran->approval_name)) ? $penawaran->approval_name : null;
              } else {
                echo '-';
              };
              ?>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <th align="center"><?= (isset($penawaran->approval_role)) ? $penawaran->approval_role : null ?></th>
          </tr>
        </tfoot>
      </table>
      <p style="margin: 0; margin-top: 20px; font-size: 11px;">
        *Penawaran elektronik ini sah, berlaku dibuat secara otomatis oleh sistem sehingga tidak memerlukan tanda tangan langsung.
      </p>
    <?php endif ?>
    <!-- END ## TTD -->

    <!-- <span style="font-size: 12px;"><b>PT. DHARMA KARYATAMA MULIA</b></span>
    <div style="margin-bottom: 50px;"></div>
    <span style="font-size: 12px;"><b><u><?= (isset($penawaran->approval_name)) ? $penawaran->approval_name : null ?></u></b></span> <br>
    <span style="font-size: 12px;"><?= (isset($penawaran->approval_role)) ? $penawaran->approval_role : null ?></span> -->
  </div>
</div>