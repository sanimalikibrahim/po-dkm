<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Penawaran extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'PenawaranModel',
      'PenawaranitemModel',
      'PenawaranarsipModel',
      'PenawaranarsipitemModel',
      'DocumentModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    redirect(base_url('penawaran/approvalresult'));
  }

  public function draft()
  {
    $this->_tableList('draft', 'Draft');
  }

  public function approval()
  {
    $this->_tableList('approval', 'Persetujuan');
  }

  public function approvalresult()
  {
    $this->_tableList('approvalresult', 'Hasil Persetujuan');
  }

  public function approved()
  {
    $agent = new Mobile_Detect;
    $data = array(
      'app' => $this->app(),
      'is_mobile' => $agent->isMobile(),
      'main_js' => $this->load_main_js('penawaran'),
      'card_title' => 'Penawaran › Rekap',
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('grid_approved', $data, TRUE);
    $this->template->render();
  }

  private function _tableList($module = 'approvalresult', $pageTitle = 'Hasil Persetujuan')
  {
    $agent = new Mobile_Detect;
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('penawaran', false, array(
        'controller' => $this,
        'module' => $module
      )),
      'card_title' => 'Penawaran › ' . $pageTitle,
      'is_mobile' => $agent->isMobile(),
      'module' => $module
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function wizard($id = null, $wizardId = 1)
  {
    $src = $this->input->get('src');

    if ($src === 'revisi') {
      $isRevisi = true;
    } else {
      $isRevisi = false;
    };

    switch ($wizardId) {
      case '1':
        $this->_wizardForm_1($id, $isRevisi);
        break;
      case '2':
        $this->_wizardForm_2($id, $isRevisi);
        break;
      default:
        $this->_wizardForm_1($id, $isRevisi);
        break;
    };
  }

  private function _wizardForm_1($id = null, $isRevisi = false)
  {
    $agent = new Mobile_Detect;
    $penawaran = $this->PenawaranModel->getDetail(array('id' => $id));
    $approvalRole = array(
      array('id' => 'Manager', 'name' => 'Manager'),
      array('id' => 'G. Manager', 'name' => 'G. Manager'),
      array('id' => 'Deputy. Dir', 'name' => 'Deputy. Dir'),
      array('id' => 'Direktur', 'name' => 'Direktur'),
    );

    if (!is_null($id) && is_null($penawaran)) {
      show_404();
    } else {
      $approvalRole_temp = (!is_null($penawaran) && !empty($penawaran->approval_role)) ? $penawaran->approval_role : null;
      $approvalPicId_temp = (!is_null($penawaran) && !empty($penawaran->approval_user_id)) ? $penawaran->approval_user_id : null;
      $approvalPic_temp = array();

      if (!is_null($approvalRole_temp)) {
        $approvalPic_temp = $this->PenawaranModel->getApprovalPic($approvalRole_temp);
      };

      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('penawaran', false, array(
          'controller' => $this,
          'penawaran_id' => $id,
          'is_revisi' => $isRevisi,
        )),
        'card_title' => 'Penawaran',
        'is_mobile' => $agent->isMobile(),
        'penawaran' => $penawaran,
        'approval_role_list' => $this->init_list($approvalRole, 'id', 'name', $approvalRole_temp),
        'approval_pic_list' => $this->init_list($approvalPic_temp, 'id', 'nama_lengkap', $approvalPicId_temp),
        'is_revisi' => $isRevisi,
        'penawaran_attachment' => $this->DocumentModel->getAll(['ref' => 'penawaran', 'ref_id' => $id]),
        'data_auto_nomor' => $this->PenawaranModel->generateNomor(),
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_form', $data, TRUE);
      $this->template->render();
    };
  }

  private function _wizardForm_2($id = null)
  {
    $agent = new Mobile_Detect;
    $penawaran = $this->PenawaranModel->getDetail(array('id' => $id));

    if (!is_null($penawaran)) {
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('penawaran', false, array(
          'controller' => $this,
          'penawaran_id' => $id
        )),
        'card_title' => 'Penawaran › Item',
        'is_mobile' => $agent->isMobile(),
        'penawaran' => $penawaran,
        'penawaran_item' => $this->PenawaranitemModel->getAll_sorted($id),
        'penawaran_attachment' => $this->DocumentModel->getAll(['ref' => 'penawaran', 'ref_id' => $id]),
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_item_form', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }

  private function _save_attachment($id)
  {
    if (!empty($_FILES['file_name'])) {
      $cpUpload = new CpUpload();
      $files = $cpUpload->re_arrange($_FILES['file_name']);
      $post = array();
      $error = '';
      $directory = 'penawaran';

      foreach ($files as $item) {
        if (!empty($item['name'])) {
          $upload = $cpUpload->run($item, $directory, true, true, 'jpg|jpeg|png|gif|pdf', true);

          if ($upload->status === true) {
            $post[] = array(
              'ref' => $directory,
              'ref_id' => $id,
              'description' => $_POST['nomor'],
              'file_raw_name' => $upload->data->raw_name . $upload->data->file_ext,
              'file_raw_name_thumb' => ($upload->data->is_image) ? $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
              'file_name' => $upload->data->base_path,
              'file_name_thumb' => ($upload->data->is_image) ? 'directory/' . $directory . '/' . $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
              'file_size' => $upload->data->file_size,
              'file_type' => $upload->data->file_type,
              'file_ext' => $upload->data->file_ext,
              'created_by' =>  $this->session->userdata('user')['id']
            );
          } else {
            $error .= $upload->data;
          };
        };
      };

      if (empty($error) && count($post) > 0) {
        return $this->DocumentModel->insertBatch($post);
      } else {
        return array('status' => false, 'data' => $error);
      };
    } else {
      return array('status' => true, 'data' => 'Skip, no data to upload.');
    };
  }

  public function ajax_get_list()
  {
    $this->handle_ajax_request();
    $module = $this->input->get('src');
    $role = strtolower($this->session->userdata('user')['role']);
    $specialRole = array('supervisor', 'manager', 'g. manager', 'direktur', 'deputy. dir');

    if ($role === 'admin invoicing') {
      $condApprovalResult = array('is_archive' => 0, 'status' => 4);
    } else {
      $condApprovalResult = (in_array($role, $specialRole)) ? array('is_archive' => 0) : array('created_by' => $this->session->userdata('user')['id'], 'is_archive' => 0);
    };

    // Get approval by status
    switch ($role) {
      case 'supervisor':
        $approvalStatus = array(1);
        break;
      case 'manager':
        $approvalStatus = array(6);
        break;
      case 'g. manager':
        $approvalStatus = array(7);
        break;
      case 'deputy. dir':
        $approvalStatus = array(8);
        break;
      case 'direktur':
        $approvalStatus = array(9);
        break;
      default:
        $approvalStatus = array();
        break;
    };
    // END ## Get approval by status

    switch ($module) {
      case 'draft':
        $status = array(0);
        $pic = array('created_by' => $this->session->userdata('user')['id']);
        break;
      case 'approval':
        $status = $approvalStatus;
        $pic = array('is_archive' => 0);
        break;
      case 'approvalresult':
        $status = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
        $pic = $condApprovalResult;
        break;
      default:
        $status = array(-1);
        $pic = array('created_by' => $this->session->userdata('user')['id'], 'is_archive' => 0);
        break;
    };

    $dtAjax_config = array(
      'table_name' => 'view_penawaran',
      'order_column' => 7,
      'order_column_dir' => 'desc',
      'static_conditional' => $pic,
      'static_conditional_in_key' => 'status',
      'static_conditional_in' => $status,
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);

    echo json_encode($response);
  }

  public function ajax_get_approved($month = null, $year = null)
  {
    $this->handle_ajax_request();
    $month = (is_null($month)) ? date('m') : $month;
    $year = (is_null($year)) ? date('Y') : $year;
    $dtAjax_config = array(
      'table_name' => 'view_penawaran_rekap',
      'static_conditional' => array(
        'MONTH(tanggal)' => $month,
        'YEAR(tanggal)' => $year,
        'status' => 4
      )
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->PenawaranModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        $transaction = $this->PenawaranModel->insert();
      } else {
        $isRevisi = $this->input->post('is_revisi');

        if (!is_null($isRevisi)) {
          $transaction = $this->PenawaranModel->revisi($id);
        } else {
          $transaction = $this->PenawaranModel->update($id);
        };
      };

      if ($transaction['status'] === true) {
        $tempId = $transaction['data_id'];

        // Handle attachment upload
        $attachment = $this->_save_attachment($tempId);

        if ($attachment['status'] === false) {
          $transaction = $attachment;
        };
        // END ## Handle attachment upload
      };

      echo json_encode($transaction);
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_save_bypass($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->PenawaranModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        $transaction = $this->PenawaranModel->insert();
      } else {
        $isRevisi = $this->input->post('is_revisi');

        if (!is_null($isRevisi)) {
          $transaction = $this->PenawaranModel->revisi($id);
        } else {
          $transaction = $this->PenawaranModel->update($id);
        };
      };

      if ($transaction['status'] === true) {
        $tempId = $transaction['data_id'];

        // Handle attachment upload
        $attachment = $this->_save_attachment($tempId);

        if ($attachment['status'] === false) {
          $transaction = $attachment;
        };
        // END ## Handle attachment upload

        // Handle set status
        $transaction_setStatus = $this->PenawaranModel->setStatus($tempId, 1);

        if ($transaction_setStatus['status'] === true) {
          $myFullName = $this->session->userdata('user')['nama_lengkap'];

          // Notification
          $notification_data = array(
            'user_to' => $this->input->post('approval_user_id'),
            'ref' => 'penawaran',
            'ref_id' => $id,
            'description' => '<span style="color: #2196F3;">' . $myFullName . '</span> mengajukan penawaran.',
            'link' => 'penawaran/approval/?ref=' . $this->input->post('nomor')
          );
          $this->set_notification($notification_data);
          // END ## Notification
        };
        // END ## Handle set status
      };

      echo json_encode($transaction);
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_save_upload()
  {
    $this->handle_ajax_request();

    $cpUpload = new CpUpload();
    $upload = $cpUpload->run('file', 'penawaran', true, true, 'pdf|jpeg|jpg|png|gif|doc|docx|xls|xlsx');
    $penawaranId = $this->input->post('penawaran_id');

    if ($upload->status === true) {
      $_POST['berkas_path'] = $upload->data->base_path;
      $_POST['berkas_name'] = $upload->data->file_name;

      $transaction = $this->PenawaranModel->uploadPenyelesaian();

      if ($transaction['status'] === true) {
        $penawaran = $this->PenawaranModel->getDetail(array('id' => $penawaranId));
        $myFullName = $this->session->userdata('user')['nama_lengkap'];

        if (!is_null($penawaran)) {
          // Notification
          $notification_data = array(
            'ref' => 'penawaran',
            'ref_id' => $penawaranId,
            'description' => '<span style="color: #2196F3;">' . $myFullName . '</span> menyelesaikan penawaran.',
            'link' => 'penawaran/approvalresult/?ref=' . $penawaran->nomor
          );
          $this->set_notification($notification_data, 'Admin Invoicing');
          // END ## Notification
        };
      };

      echo json_encode($transaction);
    } else {
      echo json_encode(array('status' => false, 'data' => $upload->data));
    };
  }

  public function ajax_set_status($id = null, $status = null)
  {
    $this->handle_ajax_request();
    $transaction = $this->PenawaranModel->setStatus($id, $status);

    if ($transaction['status'] === true) {
      $penawaran = $this->PenawaranModel->getDetail(array('id' => $id));
      $userName = $this->session->userdata('user')['nama_lengkap'];

      // Notification
      $notification_data = array(
        'ref' => 'penawaran',
        'ref_id' => $id,
        'description' => '<span style="color: #2196F3;">' . $userName . '</span> mengajukan penawaran.',
        'link' => 'penawaran/approval/?ref=' . $penawaran->nomor
      );
      $this->set_notification($notification_data, 'Supervisor');
      // // END ## Notification
    };

    echo json_encode($transaction);
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PenawaranModel->delete($id));
  }

  public function ajax_cancel($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PenawaranModel->cancel($id));
  }

  public function ajax_delete_attachment($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->DocumentModel->delete($id));
  }

  public function ajax_get_approval()
  {
    $this->handle_ajax_request();
    $role = $this->input->post('role');

    echo json_encode($this->PenawaranModel->getApprovalPic($role));
  }

  public function ajax_get_preview($id)
  {
    $this->handle_ajax_request();

    $agent = new Mobile_Detect;
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('penawaran'),
      'card_title' => 'Penawaran › View',
      'controller' => $this,
      'is_mobile' => $agent->isMobile(),
      'data_id' => $id,
      'penawaran' => $this->PenawaranModel->getDetail(array('id' => $id)),
      'penawaran_item' => $this->PenawaranitemModel->getAll_sorted($id),
      'penawaran_arsip' => $this->PenawaranarsipModel->getAll(array('revisi_parent_id' => $id)),
      'penawaran_attachment' => $this->DocumentModel->getAll(['ref' => 'penawaran', 'ref_id' => $id]),
      'is_attachment_visible' => true,
    );

    $agent = new Mobile_Detect;
    if ($agent->isMobile()) {
      // $this->load->view('_view_mobile', $data);
      $this->load->view('_view', $data);
    } else {
      $this->load->view('_view', $data);
    };
  }

  public function ajax_get_preview_riwayat($id)
  {
    $this->handle_ajax_request();

    $agent = new Mobile_Detect;
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('penawaran'),
      'card_title' => 'Penawaran › View',
      'controller' => $this,
      'is_mobile' => $agent->isMobile(),
      'data_id' => $id,
      'penawaran' => $this->PenawaranarsipModel->getDetail(array('id' => $id)),
      'penawaran_item' => $this->PenawaranarsipitemModel->getAll_sorted($id),
      'penawaran_arsip' => array(),
      'penawaran_attachment' => $this->DocumentModel->getAll(['ref' => 'penawaran', 'ref_id' => $id]),
      'is_attachment_visible' => false,
    );

    $agent = new Mobile_Detect;
    if ($agent->isMobile()) {
      // $this->load->view('_view_mobile', $data);
      $this->load->view('_view', $data);
    } else {
      $this->load->view('_view', $data);
    };
  }

  public function ajax_save_approval($status = null, $id = null)
  {
    $this->handle_ajax_request();
    $penawaran = $this->PenawaranModel->getDetail(array('id' => $id));
    $penawaranRole = strtolower($penawaran->approval_role);
    $userRole = strtolower($this->session->userdata('user')['role']);
    $userName = $this->session->userdata('user')['nama_lengkap'];

    if ((int) $status === 3) {
      $notifStatus = 2;
    } else {
      if ($userRole === $penawaranRole) {
        $status = 4;
      } else {
        if ($userRole === strtolower('supervisor')) {
          $status = 6;
        } else {
          $status = (int) $penawaran->status + 1;
        };
      };
      $notifStatus = 1;
    };

    switch ($notifStatus) {
      case 1:
        $notifMessage = 'menyetujui penawaran.';
        break;
      case 2:
        $notifMessage = 'menolak penawaran.';
        break;
      default:
        $notifMessage = 'undefined.';
        break;
    };

    $transaction = $this->PenawaranModel->setApproval($id, $status);

    if ($transaction['status'] === true) {
      // Notification to creator
      $notification_data = array(
        'user_to' => $penawaran->created_by,
        'ref' => 'penawaran',
        'ref_id' => $id,
        'description' => '<span style="color: #2196F3;">' . $userName . '</span> ' . $notifMessage,
        'link' => 'penawaran/approvalresult/?ref=' . $penawaran->nomor
      );
      $this->set_notification($notification_data);
      // END ## Notification to creator

      // To other user
      if ($status == 4) {
        $roleToReceive = 'Admin Invoicing';
        $notifLinkNext = 'penawaran/approvalresult/?ref=' . $penawaran->nomor;
      } else if ($status == 6) {
        $roleToReceive = 'Manager';
        $notifLinkNext = 'penawaran/approval/?ref=' . $penawaran->nomor;
      } else if ($status == 7) {
        $roleToReceive = 'G. Manager';
        $notifLinkNext = 'penawaran/approval/?ref=' . $penawaran->nomor;
      } else if ($status == 8) {
        $roleToReceive = 'Deputy. Dir';
        $notifLinkNext = 'penawaran/approval/?ref=' . $penawaran->nomor;
      } else if ($status == 9) {
        $roleToReceive = 'Direktur';
        $notifLinkNext = 'penawaran/approval/?ref=' . $penawaran->nomor;
      };

      // Notification by roles
      $notification_data = array(
        'ref' => 'penawaran',
        'ref_id' => $id,
        'description' => '<span style="color: #2196F3;">' . $userName . '</span> ' . $notifMessage,
        'link' => $notifLinkNext
      );
      $this->set_notification($notification_data, $roleToReceive);
      // END ## Notification by roles
    };

    echo json_encode($transaction);
  }

  // Penawaran Item
  public function ajax_get_penawaran_item_table($id = null)
  {
    $this->handle_ajax_request();
    echo $this->load->view('_table_item', array(
      'penawaran' => $this->PenawaranModel->getDetail(array('id' => $id)),
      'penawaran_item' => $this->PenawaranitemModel->getAll_sorted($id),
    ), TRUE);
  }

  public function ajax_save_item($id = null)
  {
    $this->handle_ajax_request();
    $isHeader = $this->input->post('is_header');

    if ($isHeader == "1") {
      $this->form_validation->set_rules($this->PenawaranitemModel->rules_header());
    } else {
      $this->form_validation->set_rules($this->PenawaranitemModel->rules_item());
    };

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->PenawaranitemModel->insert());
      } else {
        echo json_encode($this->PenawaranitemModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete_item($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PenawaranitemModel->delete($id));
  }

  public function ajax_bold_item($id, $isBold = 1)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PenawaranitemModel->setBold($id, $isBold));
  }

  public function ajax_import_item()
  {
    $this->handle_ajax_request();

    try {
      $penawaranId = $this->input->post('penawaran_id');

      if (!is_null($penawaranId)) {
        // Delete current item
        $this->PenawaranitemModel->deleteByPenawaran($penawaranId);

        $this->file = json_decode(json_encode($_FILES['file']));

        if (isset($this->file) && !empty($this->file->name)) {
          $inputFileTemp = $this->file->tmp_name;

          $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileTemp);
          $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

          $reader->setReadDataOnly(true);

          $spreadsheet = $reader->load($inputFileTemp);
          $activeSheet = $spreadsheet->getActiveSheet();
          $worksheetInfo = $reader->listWorksheetInfo($inputFileTemp);
          $worksheetInfo = $worksheetInfo[0];

          $colStart = 1;
          $rowStart = 2;
          $columnEnd = $worksheetInfo['totalColumns'];
          $rowEnd = $worksheetInfo['totalRows'];

          for ($row = $rowStart; $row <= $rowEnd; $row++) {
            for ($col = $colStart; $col <= $columnEnd; $col++) {
              $colName = $this->PenawaranitemModel->getColumnName($col);
              $value = $activeSheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
              $value = (!empty(trim($value))) ? $value : null;

              // Set align right
              $toRightAlign = array('Sub Total', 'Total', 'Construction Fee 10%', 'Construction Fee 11%', 'Grand Total', 'Rounded');
              $value = ($colName === 'uraian' && in_array(trim($value), $toRightAlign)) ? '<div style="text-align: right;">' . $value . '</div>' : $value;

              $worksheetData[$row][$colName] = $value;
            };

            $worksheetData[$row]['penawaran_id'] = $penawaranId;
            $worksheetData[$row]['created_by'] = $this->session->userdata('user')['id'];
          };

          $worksheetData = array_values($worksheetData);

          echo json_encode($this->PenawaranitemModel->insertBatch($worksheetData));
        } else {
          $errors = '<div>The File field is required.</div>';
          echo json_encode(array('status' => false, 'data' => $errors));
        };
      } else {
        echo json_encode(array('status' => false, 'data' => 'The Penawaran ID field is required.'));
      };
    } catch (\Throwable $th) {
      $errors = '<div>Failed to get information data.</div>';
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }
  // END ## Penawaran Item

  public function get_status($status)
  {
    switch ($status) {
      case '0':
        return 'Draft';
        break;
      case '1':
        return 'Menunggu Persetujuan SPV';
        break;
      case '2':
        return 'Disetujui';
        break;
      case '3':
        return 'Ditolak';
        break;
      case '4':
        return 'Disetujui';
        break;
      case '5':
        return 'Dibatalkan';
        break;
      case '6':
        return 'Menunggu Persetujuan Manager';
        break;
      case '7':
        return 'Menunggu Persetujuan GM';
        break;
      case '8':
        return 'Menunggu Persetujuan Deputy. Dir';
        break;
      case '9':
        return 'Menunggu Persetujuan Direktur';
        break;
      default:
        return 'Undefined';
        break;
    };
  }
}
