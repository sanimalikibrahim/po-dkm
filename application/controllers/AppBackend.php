<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(FCPATH . 'vendor/autoload.php');

class AppBackend extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->handle_access();
        $this->load->model(['SettingModel', 'MenuModel', 'UserModel', 'NotificationModel']);
        $this->load->library('form_validation');
        $this->template->set_template($this->app()->template_backend);
    }

    public function app()
    {
        $agent = new Mobile_Detect;
        $appData = $this->SettingModel->getAll();
        $config = array();

        if (count($appData) > 0) {
            foreach ($appData as $index => $item) {
                $config[$item->data] = $item->content;
            };
        };

        $config['is_mobile'] = $agent->isMobile();

        return (object) $config;
    }

    public function load_main_js($moduleName, $isSpecificPath = false, $variables = null)
    {
        if (!is_null($variables)) {
            extract($variables, EXTR_SKIP);
        };

        ob_start();

        if ($isSpecificPath === true) {
            @include FCPATH . '/application/modules/' . $moduleName;
        } else {
            @include FCPATH . '/application/modules/' . $moduleName . '/views/main.js.php';
        };

        $sourcePath = ob_get_clean();
        // $minifier = new Minify\JS($sourcePath);

        // return $minifier->minify();
        return $sourcePath;
        ob_end_clean();
    }

    public function handle_access()
    {
        $session = $this->session->userdata('user');
        $isLogin = (!is_null($session) && $session['is_login'] === true) ? true : false;

        if ($isLogin === false) {
            if ($this->router->fetch_class() != 'login') {
                redirect(base_url('login'));
            };
        } else {
            if ($this->router->fetch_class() == 'login') {
                redirect(base_url());
            };
        };
    }

    public function handle_ajax_request()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        };
    }

    public function get_month($bulan)
    {
        switch ($bulan) {
            case '01':
                return 'Januari';
                break;
            case '02':
                return 'Februari';
                break;
            case '03':
                return 'Maret';
                break;
            case '04':
                return 'April';
                break;
            case '05':
                return 'Mei';
                break;
            case '06':
                return 'Juni';
                break;
            case '07':
                return 'Juli';
                break;
            case '08':
                return 'Agustus';
                break;
            case '09':
                return 'September';
                break;
            case '10':
                return 'Oktober';
                break;
            case '11':
                return 'November';
                break;
            case '12':
                return 'Desember';
                break;
            default:
                return 'Undefined';
                break;
        };
    }

    public function set_notification($post, $role = null)
    {
        $payload = [];

        if (!is_null($role)) {
            $users = $this->UserModel->getAll(['role' => $role]);

            if (count($users) > 0) {
                foreach ($users as $key => $item) {
                    $payload[] = array(
                        'user_from' => $this->session->userdata('user')['id'],
                        'user_to' => $item->id,
                        'ref' => $post['ref'],
                        'ref_id' => $post['ref_id'],
                        'description' => $post['description'],
                        'link' => $post['link']
                    );
                };
            };
        } else {
            $payload[] = array(
                'user_from' => $this->session->userdata('user')['id'],
                'user_to' => $post['user_to'],
                'ref' => $post['ref'],
                'ref_id' => $post['ref_id'],
                'description' => $post['description'],
                'link' => $post['link']
            );
        };

        return $this->NotificationModel->insertBatch($payload);
    }

    public function init_list($data, $value, $text, $default_value = null, $static = null)
    {
        $lists = '<option disabled selected>(No data available)</option>';

        if (count($data) > 0) {
            $is_selected_ph = (is_null($default_value)) ? 'selected' : '';
            $lists = '<option disabled ' . $is_selected_ph . '>Select &#8595;</option>';

            if (!is_null($static)) {
                $lists .= $static;
            };

            foreach ($data as $key => $item) {
                $item = (is_object($item) === false) ? (object) $item : $item;
                $is_selected = (!is_null($default_value) && ($item->{$value} === $default_value)) ? 'selected' : '';
                $lists .= '<option value="' . $item->{$value} . '" ' . $is_selected . '>' . $item->{$text} . '</option>';
            };
        };

        return $lists;
    }

    public function localizeDate($date = null)
    {
        $date = (is_null($date)) ? date('Y-m-d') : $date;
        $dateSplit = explode('-', $date);
        $year = $dateSplit[0];
        $month = $dateSplit[1];
        $day = $dateSplit[2];
        $monthName = $this->get_month($month);

        return $day . ' ' . $monthName . ' ' . $year;
    }
}
