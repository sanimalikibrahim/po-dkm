<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Search extends AppBackend
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(['SearchModel']);
    }

    public function index()
    {
        $keyword = (isset($_GET['q'])) ? $_GET['q'] : '';
        $data = $this->SearchModel->packingOrder($keyword);
        $data = array(
            'app' => $this->app(),
            'card_title' => 'Search',
            'card_subTitle' => count($data) . ' data found for "' . $keyword . '".',
            'controller' => $this,
            'data' => $data
        );
        $this->template->set('title', $data['app']->app_name, TRUE);
        $this->template->load_view('search', $data, TRUE);
        $this->template->render();
    }

    public function get_status($status)
    {
        switch ($status) {
            case 0:
                return 'Draft';
                break;
            case 1:
                return 'Menunggu Persetujuan Manager';
                break;
            case 2:
                return 'Menunggu Persetujuan Finance';
                break;
            case 3:
                return 'Ditolak Oleh Manager';
                break;
            case 4:
                return 'Menunggu Persetujuan G. Manager';
                break;
            case 5:
                return 'Ditolak Oleh Finance';
                break;
            case 6:
                return 'Menunggu Persetujuan Deputy. Dir';
                break;
            case 7:
                return 'Ditolak Oleh G. Manager';
                break;
            case 8:
                return 'Menunggu Persetujuan Direktur';
                break;
            case 9:
                return 'Ditolak Oleh Deputy. Dir';
                break;
            case 10:
                return 'Disetujui Direktur';
                break;
            case 11:
                return 'Ditolak Oleh Direktur';
                break;
            default:
                return 'Draft';
                break;
        }
    }
}
