<aside class="sidebar">
    <div class="scrollbar-inner">
        <div class="user">
            <div class="user__info" data-toggle="dropdown">
                <img class="user__img" src="<?php echo base_url('themes/material_admin/') ?>demo/img/profile-pics/8.jpg" alt="">
                <div>
                    <div class="user__name"><?php echo $this->session->userdata('user')['nama_lengkap'] ?></div>
                    <div class="user__email"><?php echo $this->session->userdata('user')['role'] ?></div>
                </div>
            </div>

            <div class="dropdown-menu">
                <a class="dropdown-item" href="<?php echo base_url('setting/account') ?>">Account</a>
                <a class="dropdown-item" href="<?php echo base_url('logout') ?>">Logout</a>
            </div>
        </div>

        <ul class="navigation">

            <?php
            if (isset($menus) && count($menus) > 0) {
                $menu = '';
                foreach ($menus as $key => $item) {
                    if (count($item->childs) === 0) {
                        // Has no childs
                        $isNewTab = ($item->is_newtab) ? 'target="_blank"' : '';
                        $activeLink = ($item->link_tobase) ? base_url($item->link) : $item->link;
                        $class = ($this->router->fetch_class() === $item->link) ? 'navigation__active' : '';
                        $menu .= '<li class="' . $class . '">';
                        $menu .= '<a href="' . $activeLink . '" ' . $isNewTab . '><i class="' . $item->icon . '"></i> ' . $item->name . '</a>';
                        $menu .= '</li>';
                    } else {
                        // Have a childs
                        $links = $item->link;
                        $links = rtrim($links, ',');
                        $links = preg_replace('/\s+/', '', $links);
                        $links = explode(',', $links);

                        if (count($links) == 0) {
                            $links = $item->link;
                        };

                        $class = (in_array($this->router->fetch_class(), $links)) ? 'navigation__sub--active navigation__sub--toggled' : '';
                        $menu .= '<li class="navigation__sub ' . $class . '">';
                        $menu .= '<a href="#"><i class="' . $item->icon . '"></i> ' . $item->name . '</a>';
                        $menu .= '<ul>';

                        foreach ($item->childs as $index => $child) {
                            $routerClass = $this->router->fetch_class();
                            $routerMethod = $this->router->fetch_method();
                            $isNewTab = ($child->is_newtab) ? 'target="_blank"' : '';
                            $activeLink = ($child->link_tobase) ? base_url($child->link) : $child->link;
                            $classChild = (($routerClass === $child->link) || ($routerClass . '/' . $routerMethod === $child->link)) ? 'navigation__active' : '';
                            $menu .= '<li class="' . $classChild . '">';
                            $menu .= '<a href="' . $activeLink . '" ' . $isNewTab . '>' . $child->name . '</a>';
                            $menu .= '</li>';
                        };

                        $menu .= '</ul>';
                        $menu .= '</li>';
                    };
                };
                echo $menu;
            };
            ?>

        </ul>
    </div>
</aside>