</main>

<!-- Javascript -->
<!-- Vendors -->
<script src="<?php echo base_url('themes/material_admin/vendors/jquery/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/popper.js/popper.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/bootstrap/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/jquery-scrollbar/jquery.scrollbar.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/jquery-scrollLock/jquery-scrollLock.min.js') ?>"></script>

<script src="<?php echo base_url('themes/material_admin/vendors/flot/jquery.flot.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/flot/jquery.flot.resize.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/flot.curvedlines/curvedLines.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/jqvmap/jquery.vmap.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/jqvmap/maps/jquery.vmap.world.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/easy-pie-chart/jquery.easypiechart.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/salvattore/salvattore.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/sparkline/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/moment/moment.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/fullcalendar/fullcalendar.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/bootstrap-notify/bootstrap-notify.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/sweetalert2/sweetalert2.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/tinymce/tinymce.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/autosize/autosize.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/jquery-text-counter/textcounter.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/flatpickr/flatpickr.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/jquery-mask-plugin/jquery.mask.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/select2/js/select2.full.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/nouislider/nouislider.min.js') ?>"></script>

<!-- Vendors: Data tables -->
<script src="<?php echo base_url('themes/material_admin/vendors/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="https://cdn.datatables.net/rowgroup/1.1.1/js/dataTables.rowGroup.js"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/datatables-buttons/dataTables.buttons.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/datatables-buttons/buttons.print.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/jszip/jszip.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/datatables-buttons/buttons.html5.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/datatables/dataTables.rowReorder.min.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/vendors/datatables/dataTables.responsive.min.js') ?>"></script>

<!-- Charts and maps-->
<script src="<?php echo base_url('themes/material_admin/demo/js/flot-charts/curved-line.js') ?>"></script>
<!-- <script src="<?php echo base_url('themes/material_admin/demo/js/flot-charts/dynamic.js') ?>"></script> -->
<script src="<?php echo base_url('themes/material_admin/demo/js/flot-charts/line.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/demo/js/flot-charts/chart-tooltips.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/demo/js/other-charts.js') ?>"></script>
<script src="<?php echo base_url('themes/material_admin/demo/js/jqvmap.js') ?>"></script>

<!-- PrintThis -->
<script src="<?php echo base_url('themes/_public/js/printThis.js') ?>"></script>

<!-- App functions and actions -->
<script src="<?php echo base_url('themes/material_admin/js/app.min.js') ?>"></script>
<script src="<?php echo base_url('themes/_public/js/public.main.js') ?>"></script>

<?php echo (isset($main_js)) ?  $main_js : '' ?>


<script type="text/javascript">
	function getTimeAgo(time) {
		return moment(time).fromNow();
	};

	// Notification
	$(".notif-time-ago").each(function(i, obj) {
		var val = $(obj).html();
		$(obj).html(getTimeAgo(val));
	});

	var checkNotification = function() {
		$.ajax({
			type: "get",
			url: "<?php echo base_url('notification/last/1') ?>",
			success: function(response) {
				var response = JSON.parse(response);
				var lists = '';

				if (response.count_unread > 0) {
					$("#app-notification-flag").addClass("top-nav__notify");
				} else {
					$("#app-notification-flag").removeClass("top-nav__notify");
				};

				if (response.count > 0) {
					jQuery.each(response.data, function(index, item) {
						var isRead = (item.is_read == "0") ? 'font-weight: bold;' : '';
						var timeAgo = getTimeAgo(item.created_date);
						lists += '<a href="<?php echo base_url('notification/read/') ?>' + item.id + '" class="listview__item" style="padding: .50rem 1rem;">' +
							'<img src="<?php echo base_url('/themes/material_admin/demo/img/profile-pics/8.jpg') ?>" class="listview__img">' +
							'<div class="listview__content">' +
							'<p style="white-space: normal; line-break: anywhere; ' + isRead + '">' + item.description + '</p>' +
							'<small style="color: #777;">' + timeAgo + '</small>' +
							'</div>' +
							'</a>';
					});
					$("#app-notification-data").html(lists);
				} else {
					$("#app-notification-data").html("<div style='padding: 1rem;'>You have no notification</div>");
				};
			}
		});
	};
	checkNotification();
	// var refInterval = window.setInterval("checkNotification()", 60000); // 60 seconds
	// END ## Notification

	// Search
	$("#form-app-search").on("submit", function(e) {
		e.preventDefault();
		var keyword = $(".app-search-keyword").val();
		if (keyword.trim() != "") {
			window.location = "<?php echo base_url('search/?q=') ?>" + keyword;
		};
	});
	// END ## Search
</script>
</body>

</html>