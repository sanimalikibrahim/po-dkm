<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>{title}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('themes/_public/') ?>img/favicon-black/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('themes/_public/') ?>img/favicon-black/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('themes/_public/') ?>img/favicon-black/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('themes/_public/') ?>img/favicon-black/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('themes/_public/') ?>img/favicon-black/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('themes/_public/') ?>img/favicon-black/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('themes/_public/') ?>img/favicon-black/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('themes/_public/') ?>img/favicon-black/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('themes/_public/') ?>img/favicon-black/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="i<?php echo base_url('themes/_public/') ?>mg/favicon-black/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('themes/_public/') ?>img/favicon-black/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('themes/_public/') ?>img/favicon-black/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('themes/_public/') ?>img/favicon-black/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url('themes/_public/') ?>img/favicon-black/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url('themes/_public/') ?>img/favicon-black/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/material-design-iconic-font/css/material-design-iconic-font.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/animate.css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/jquery-scrollbar/jquery.scrollbar.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/fullcalendar/fullcalendar.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/sweetalert2/sweetalert2.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/flatpickr/flatpickr.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/select2/css/select2.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/nouislider/nouislider.min.css') ?>">
    <link href="https://cdn.datatables.net/rowgroup/1.1.1/css/rowGroup.dataTables.css" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/datatables/dataTables.bootstrap.min.css') ?>" /> -->
    <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/vendors/datatables/responsive.bootstrap.min.css') ?>" />

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url('themes/material_admin/css/app.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('themes/_public/css/public.main.css') ?>">
</head>

<body data-ma-theme="<?php echo $app->theme_color ?>">
    <main class="main">
        <div class="page-loader">
            <div class="page-loader__spinner">
                <svg viewBox="25 25 50 50">
                    <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                </svg>
            </div>
        </div>

        <header class="header">
            <div class="navigation-trigger hidden-xl-up" data-ma-action="aside-open" data-ma-target=".sidebar">
                <div class="navigation-trigger__inner">
                    <i class="navigation-trigger__line"></i>
                    <i class="navigation-trigger__line"></i>
                    <i class="navigation-trigger__line"></i>
                </div>
            </div>

            <div class="header__logo">
                <h1>
                    <a href="<?php echo base_url() ?>">
                        <?php echo (!$app->is_mobile) ? $app->app_name  : 'PO | DKM' ?>
                    </a>
                </h1>
            </div>

            <form class="search" id="form-app-search">
                <div class="search__inner">
                    <?php $currentKey = ($this->router->fetch_class() == 'search' && isset($_GET['q'])) ? $_GET['q'] : '' ?>
                    <input type="text" class="search__text app-search-keyword" placeholder="Search for packing order..." value="<?php echo $currentKey ?>">
                    <i class="zmdi zmdi-search search__helper" data-ma-action="search-close"></i>
                </div>
            </form>

            <ul class="top-nav">
                <li class="hidden-xl-up"><a href="" data-ma-action="search-open"><i class="zmdi zmdi-search"></i></a></li>

                <li class="dropdown top-nav__notifications">
                    <a href="" data-toggle="dropdown" id="app-notification-flag">
                        <i class="zmdi zmdi-notifications"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                        <div class="listview listview--hover">
                            <div class="listview__header">
                                Notifications
                            </div>
                            <div class="listview__scroll scrollbar-inner">
                                <!-- Looping here -->
                                <div id="app-notification-data"></div>
                                <!-- END ## Looping here -->
                            </div>
                            <a href="<?php echo base_url('notification') ?>" class="view-more">View All</a>
                            <div class="p-1"></div>
                        </div>
                    </div>
                </li>

                <?php if (!$app->is_mobile) : ?>
                    <li class="dropdown hidden-xs-down">
                        <a href="" data-toggle="dropdown"><i class="zmdi zmdi-format-color-fill"></i></a>

                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-item theme-switch">
                                Theme Color

                                <div class="btn-group btn-group-toggle btn-group--colors" data-toggle="buttons">
                                    <label class="btn bg-green <?php echo ($app->theme_color == 'green') ? 'active' : '' ?>"><input type="radio" value="green" autocomplete="off" checked></label>
                                    <label class="btn bg-blue <?php echo ($app->theme_color == 'blue') ? 'active' : '' ?>"><input type="radio" value="blue" autocomplete="off"></label>
                                    <label class="btn bg-red <?php echo ($app->theme_color == 'red') ? 'active' : '' ?>"><input type="radio" value="red" autocomplete="off"></label>
                                    <label class="btn bg-orange <?php echo ($app->theme_color == 'orange') ? 'active' : '' ?>"><input type="radio" value="orange" autocomplete="off"></label>
                                    <label class="btn bg-teal <?php echo ($app->theme_color == 'teal') ? 'active' : '' ?>"><input type="radio" value="teal" autocomplete="off"></label>

                                    <div class="clearfix mt-2"></div>

                                    <label class="btn bg-cyan <?php echo ($app->theme_color == 'cyan') ? 'active' : '' ?>"><input type="radio" value="cyan" autocomplete="off"></label>
                                    <label class="btn bg-blue-grey <?php echo ($app->theme_color == 'blue-grey') ? 'active' : '' ?>"><input type="radio" value="blue-grey" autocomplete="off"></label>
                                    <label class="btn bg-purple <?php echo ($app->theme_color == 'purple') ? 'active' : '' ?>"><input type="radio" value="purple" autocomplete="off"></label>
                                    <label class="btn bg-indigo <?php echo ($app->theme_color == 'indigo') ? 'active' : '' ?>"><input type="radio" value="indigo" autocomplete="off"></label>
                                    <label class="btn bg-brown <?php echo ($app->theme_color == 'bworn') ? 'active' : '' ?>"><input type="radio" value="brown" autocomplete="off"></label>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endif; ?>

                <?php if ($app->is_mobile) : ?>
                    <li class="dropdown">
                        <a href=""><i class="zmdi zmdi-refresh"></i></a>
                    </li>
                <?php endif; ?>
            </ul>
        </header>

        <?php include_once('sidebar.php') ?>