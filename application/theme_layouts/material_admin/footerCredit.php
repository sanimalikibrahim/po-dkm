<footer class="footer hidden-xs-down">
  <p>© <?php echo $app->app_name ?>. Version <?php echo $app->app_version ?></p>

  <ul class="nav footer__nav">
    <a class="nav-link" href="<?php echo base_url('developercontact/') ?>" target="_blank">Developer Contact</a>
  </ul>
</footer>
