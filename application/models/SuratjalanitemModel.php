<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SuratjalanitemModel extends CI_Model
{
  private $_table = 'surat_jalan_item';
  private $_tableView = '';
  private $_columns = array(
    'surat_jalan_id',
    'supplier_barang_id',
    'kode_barang',
    'nama_barang',
    'quantity',
    'quantity_unit',
    'keterangan',
    'pengiriman_ke',
  ); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'surat_jalan_id',
        'label' => 'Surat Jalan ID',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'supplier_barang_id',
        'label' => 'Barang ID',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'kode_barang',
        'label' => 'Kode Barang',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nama_barang',
        'label' => 'Nama Barang',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'quantity',
        'label' => 'Quantity',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'quantity_unit',
        'label' => 'Quantity Unit',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'keterangan',
        'label' => 'Keterangan',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'pengiriman_ke',
        'label' => 'Pengiriman Ke',
        'rules' => 'required|trim'
      ],
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function getTotalDikirim($suratJalanId = null)
  {
    $this->db->select_sum('quantity');
    $this->db->where(['surat_jalan_id' => $suratJalanId]);
    return $this->db->get($this->_table)->row();
  }

  public function getAllByPermintaan($permintaanBarangId = null)
  {
    $query = "
      SELECT
        x.permintaan_barang_id,
        x.supplier_barang_id,
        IFNULL(x.kode_barang, '') AS kode_barang,
        x.nama_barang,
        x.quantity_unit,
        x.permintaan,
        SUM(x.sudah_dikirim) AS sudah_dikirim,
        (x.permintaan - IFNULL(SUM(x.sudah_dikirim), 0)) AS belum_dikirim
      FROM (
        SELECT
          t.permintaan_barang_id,
          t.supplier_barang_id,
          t.kode_barang,
          t.nama_barang,
          t.quantity_unit,
          t.quantity AS permintaan,
          (
            SELECT SUM(quantity)
            FROM surat_jalan_item
            WHERE surat_jalan_id = s.id AND supplier_barang_id = t.supplier_barang_id
          ) AS sudah_dikirim
        FROM permintaan_barang_item t
        LEFT JOIN surat_jalan s ON s.permintaan_barang_id = t.permintaan_barang_id
        WHERE t.permintaan_barang_id = '$permintaanBarangId'
      ) x
      GROUP BY supplier_barang_id
    ";
    return $this->db->query($query)->result();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->surat_jalan_id = $this->clean_number($this->input->post('surat_jalan_id'));
      $this->supplier_barang_id = $this->clean_number($this->input->post('supplier_barang_id'));
      $this->kode_barang = $this->input->post('kode_barang');
      $this->nama_barang = $this->input->post('nama_barang');
      $this->quantity = $this->clean_number($this->input->post('quantity'));
      $this->quantity_unit = $this->input->post('quantity_unit');
      $this->keterangan = $this->input->post('keterangan');
      $this->pengiriman_ke = $this->clean_number($this->input->post('pengiriman_ke'));
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->surat_jalan_id = $this->clean_number($this->input->post('surat_jalan_id'));
      $this->supplier_barang_id = $this->clean_number($this->input->post('supplier_barang_id'));
      $this->kode_barang = $this->input->post('kode_barang');
      $this->nama_barang = $this->input->post('nama_barang');
      $this->quantity = $this->clean_number($this->input->post('quantity'));
      $this->quantity_unit = $this->input->post('quantity_unit');
      $this->keterangan = $this->input->post('keterangan');
      $this->pengiriman_ke = $this->clean_number($this->input->post('pengiriman_ke'));
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
