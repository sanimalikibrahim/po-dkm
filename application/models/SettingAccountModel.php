<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SettingAccountModel extends CI_Model
{
  private $_table = 'user';

  public function rules()
  {
    return array(
      [
        'field' => 'email',
        'label' => 'Email',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nama_lengkap',
        'label' => 'Nama Lengkap',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function update()
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $id = $this->session->userdata('user')['id'];

    try {
      $temp = $this->getDetail(['id' => $id]);
      $post = $this->input->post();
      $post['password'] = (isset($post['password']) && !empty($post['password'])) ? md5($post['password']) : $temp->password;

      $this->password = $post['password'];
      $this->email = $post['email'];
      $this->nama_lengkap = $post['nama_lengkap'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }
}
