<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PackingorderitemModel extends CI_Model
{
  private $_table = 'packing_order_item';
  private $_tableView = '';
  private $_columns = array(
    'packing_order_id',
    'description',
    'note',
    'quantity',
    'quantity_unit',
    'unit_price',
    'amount'
  ); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'packing_order_id',
        'label' => 'Packing Order',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'description',
        'label' => 'Description',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'note',
        'label' => 'Note',
        'rules' => 'trim'
      ],
      [
        'field' => 'quantity',
        'label' => 'Quantity',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'quantity_unit',
        'label' => 'Quantity Unit',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'unit_price',
        'label' => 'Unit Price',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'amount',
        'label' => 'Amount',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $post = $this->input->post();

      $this->packing_order_id = $post['packing_order_id'];
      $this->description = $this->br2nl($post['description']);
      $this->note = $this->br2nl($post['note']);
      $this->quantity = $this->clean_number($post['quantity']);
      $this->quantity_unit = $post['quantity_unit'];
      $this->unit_price = $this->clean_number($post['unit_price']);
      $this->amount = $this->clean_number($post['amount']);
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $post = $this->input->post();

      $this->packing_order_id = $post['packing_order_id'];
      $this->description = $this->br2nl($post['description']);
      $this->note = $this->br2nl($post['note']);
      $this->quantity = $this->clean_number($post['quantity']);
      $this->quantity_unit = $post['quantity_unit'];
      $this->unit_price = $this->clean_number($post['unit_price']);
      $this->amount = $this->clean_number($post['amount']);
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
