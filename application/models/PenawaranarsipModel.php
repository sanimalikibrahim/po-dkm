<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PenawaranarsipModel extends CI_Model
{
  private $_table = 'penawaran_arsip';
  private $_tableView = '';
  private $_columns = array(); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'department',
        'label' => 'Department',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nomor',
        'label' => 'Nomor',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'city',
        'label' => 'City',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'project_name',
        'label' => 'Project Name',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'customer',
        'label' => 'Customer Name',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'customer_attn',
        'label' => 'Customer Attn',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'customer_address1',
        'label' => 'Customer Address 1',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'approval_role',
        'label' => 'Approval Role',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'approval_user_id',
        'label' => 'Approval PIC',
        'rules' => 'required|trim'
      ],
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $nomor = $this->input->post('nomor');
      $validateNomor = $this->getDetail(['nomor' => $nomor]);

      if (count($validateNomor) > 0) {
        $response = array('status' => false, 'data' => 'Nomor already exist.', 'data_id' => null);
      } else {
        $this->tanggal = $this->input->post('tanggal');
        $this->nomor = $nomor;
        $this->city = $this->input->post('city');
        $this->department = $this->input->post('department');
        $this->customer = $this->input->post('customer');
        $this->customer_address1 = $this->input->post('customer_address1');
        $this->customer_address2 = $this->input->post('customer_address2');
        $this->customer_attn = $this->input->post('customer_attn');
        $this->project_name = $this->input->post('project_name');
        $this->note = $this->input->post('note');
        $this->approval_role = $this->input->post('approval_role');
        $this->approval_user_id = $this->input->post('approval_user_id');
        $this->approval_name = $this->input->post('approval_name');
        $this->revisi_parent_id = $this->input->post('revisi_parent_id');
        $this->revisi_nomor = $this->input->post('revisi_nomor');
        $this->revisi_date = $this->input->post('revisi_date');
        $this->created_by = $this->session->userdata('user')['id'];
        $this->db->insert($this->_table, $this);

        $this->temp_id = $this->db->insert_id();

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'data_id' => null);
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $temp = $this->getDetail(['id' => $id]);
    $existNomor = false;

    try {
      $nomor = $this->input->post('nomor');

      if ($nomor != $temp->nomor) {
        $validateNomor = $this->getDetail(['nomor' => $nomor]);

        if (count($validateNomor) > 0) {
          $existNomor = true;
        };
      };

      if ($existNomor === true) {
        $response = array('status' => false, 'data' => 'Nomor already exist.');
      } else {
        $this->tanggal = $this->input->post('tanggal');
        $this->nomor = $nomor;
        $this->city = $this->input->post('city');
        $this->department = $this->input->post('department');
        $this->customer = $this->input->post('customer');
        $this->customer_address1 = $this->input->post('customer_address1');
        $this->customer_address2 = $this->input->post('customer_address2');
        $this->customer_attn = $this->input->post('customer_attn');
        $this->project_name = $this->input->post('project_name');
        $this->note = $this->input->post('note');
        $this->approval_role = $this->input->post('approval_role');
        $this->approval_user_id = $this->input->post('approval_user_id');
        $this->approval_name = $this->input->post('approval_name');
        $this->revisi_parent_id = $this->input->post('revisi_parent_id');
        $this->revisi_nomor = $this->input->post('revisi_nomor');
        $this->revisi_date = $this->input->post('revisi_date');
        $this->updated_at = date('Y-m-d H:i:s');
        $this->updated_by = $this->session->userdata('user')['id'];
        $this->db->update($this->_table, $this, ['id' => $id]);

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete('penawaran_item', ['penawaran_id' => $id]);
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);
      $this->db->truncate('penawaran_item');

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }
}
