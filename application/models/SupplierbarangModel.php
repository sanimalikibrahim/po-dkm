<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SupplierbarangModel extends CI_Model
{
  private $_table = 'supplier_barang';
  private $_tableView = '';
  private $_columns = array(
    'supplier_id',
    'departemen',
    'code_part',
    'nama_part',
    'harga',
    'satuan'
  ); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex = null)
  {
    if (!is_null($columnIndex)) {
      $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
      $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : '0'; // Get value
    } else {
      $result = $this->_columns;
    };

    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'supplier_id',
        'label' => 'Supplier',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'departemen',
        'label' => 'Departemen',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'code_part',
        'label' => 'Kode Part',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nama_part',
        'label' => 'Nama Part',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'harga',
        'label' => 'Harga',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'satuan',
        'label' => 'Satuan',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $post = $this->input->post();

      $this->supplier_id = $post['supplier_id'];
      $this->departemen = $post['departemen'];
      $this->code_part = $post['code_part'];
      $this->nama_part = $this->br2nl($post['nama_part']);
      $this->harga = $this->clean_number($post['harga']);
      $this->satuan = $post['satuan'];
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $temp = $this->getDetail(['id' => $id]);
      $post = $this->input->post();

      $this->supplier_id = $post['supplier_id'];
      $this->departemen = $post['departemen'];
      $this->code_part = $post['code_part'];
      $this->nama_part = $this->br2nl($post['nama_part']);
      $this->harga = $this->clean_number($post['harga']);
      $this->satuan = $post['satuan'];
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
