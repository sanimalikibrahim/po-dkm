<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PackingorderModel extends CI_Model
{
  private $_table = 'packing_order';
  private $_tableView = 'view_packing_order';
  private $_columns = array(
    'supplier_id',
    'nomor',
    'tanggal',
    'term_of_delivery',
    'term_of_payment',
    'term_of_payment_satuan'
  ); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'supplier_id',
        'label' => 'Supplier',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nomor',
        'label' => 'Nomor',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'term_of_delivery',
        'label' => 'Term Of Delivery',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'term_of_payment',
        'label' => 'Term Of Payment',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'term_of_payment_satuan',
        'label' => 'Term Of Payment (Satuan)',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'ppn',
        'label' => 'Pajak Pertambahan Nilai (PPN)',
        'rules' => 'required|trim'
      ]
    );
  }

  public function generateNomor()
  {
    $query = "SELECT LPAD(LEFT(nomor, 4) + 1, 4, '0') as auto_nomor FROM packing_order ORDER BY LEFT(nomor, 4) DESC LIMIT 1";
    $result = $this->db->query($query)->row();

    if (count($result) > 0) {
      $nomor = $result->auto_nomor;
    } else {
      $nomor = '0001';
    };

    $roman_month = array(1 => 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII');
    $month = $roman_month[date('n')];
    $year = date('y');
    $result = $nomor . '-PO/TRD-DKM/' . $month . '/' . $year;

    return $result;
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function checkTtd($id, $status, $role)
  {
    $query = "SELECT t.*
              FROM view_packing_order_history t
              JOIN packing_order p ON p.id = t.packing_order_id and p.revisi = t.revisi
              WHERE t.packing_order_id = '" . $id . "' AND t.action = '" . $status . "' AND t.role = '" . $role . "'
              ORDER BY t.id DESC
              LIMIT 1";
    $result = $this->db->query($query)->row();

    if (count($result) > 0) {
      return $result->nama_lengkap;
    } else {
      return null;
    };
  }

  public function getWaitApprovalCount()
  {
    $query = "SELECT *
              FROM (
                (SELECT COUNT(t.id) AS wait_supervisor FROM packing_order t WHERE status = '1') AS wait_supervisor,
                (SELECT COUNT(t.id) AS wait_manager FROM packing_order t WHERE status = '2') AS wait_manager,
                (SELECT COUNT(t.id) AS wait_finance FROM packing_order t WHERE status = '3') AS wait_finance,
                (SELECT COUNT(t.id) AS wait_g_manager FROM packing_order t WHERE status = '4') AS wait_g_manager,
                (SELECT COUNT(t.id) AS wait_deputy_dir FROM packing_order t WHERE status = '5') AS wait_deputy_dir,
                (SELECT COUNT(t.id) AS wait_direktur FROM packing_order t WHERE status = '6') AS wait_direktur
              )";
    return $this->db->query($query)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $post = $this->input->post();
      $nomor = $post['nomor'];
      $validateNomor = $this->getDetail(['nomor' => $nomor]);

      if (count($validateNomor) > 0) {
        $response = array('status' => false, 'data' => 'Nomor already exist.', 'data_id' => null);
      } else {
        $this->supplier_id = $post['supplier_id'];
        $this->nomor = $nomor;
        $this->tanggal = $post['tanggal'];
        $this->term_of_delivery = $post['term_of_delivery'];
        $this->term_of_payment = $post['term_of_payment'];
        $this->term_of_payment_satuan = $post['term_of_payment_satuan'];
        $this->ppn = $post['ppn'];
        $this->status = 0;
        $this->created_by = $this->session->userdata('user')['id'];
        $this->db->insert($this->_table, $this);

        $this->temp_id = $this->db->insert_id();

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'data_id' => null);
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $temp = $this->getDetail(['id' => $id]);
    $existNomor = false;

    try {
      $post = $this->input->post();
      $nomor = $post['nomor'];

      if ($nomor != $temp->nomor) {
        $validateNomor = $this->getDetail(['nomor' => $nomor]);

        if (count($validateNomor) > 0) {
          $existNomor = true;
        };
      };

      if ($existNomor === true) {
        $response = array('status' => false, 'data' => 'Nomor already exist.');
      } else {
        $this->supplier_id = $post['supplier_id'];
        $this->nomor = $nomor;
        $this->tanggal = $post['tanggal'];
        $this->term_of_delivery = $post['term_of_delivery'];
        $this->term_of_payment = $post['term_of_payment'];
        $this->term_of_payment_satuan = $post['term_of_payment_satuan'];
        $this->ppn = $post['ppn'];
        $this->updated_at = date('Y-m-d H:i:s');
        $this->updated_by = $this->session->userdata('user')['id'];
        $this->db->update($this->_table, $this, ['id' => $id]);

        $response = array('status' => true, 'data' => 'Data has been saved.');
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete('packing_order_history', ['packing_order_id' => $id]);
      $this->db->delete('packing_order_item', ['packing_order_id' => $id]);
      $this->db->delete('document', ['ref' => 'packingorder', 'ref_id' => $id]);
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function set_status($id, $status)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $temp = $this->getDetail(['id' => $id]);
    $revisi = ($status === 1) ? (int) $temp->revisi + 1 : (int) $temp->revisi;

    try {
      $this->status = $status;
      $this->revisi = $revisi;
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Status has been saved.', 'revisi' => $revisi);
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to update status.', 'revisi' => $revisi);
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }
}
