<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PermintaanbarangitemModel extends CI_Model
{
  private $_table = 'permintaan_barang_item';
  private $_tableView = '';
  private $_columns = array(
    'permintaan_barang_id',
    'supplier_barang_id',
    'nama_barang',
    'quantity',
    'quantity_unit',
  ); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'permintaan_barang_id',
        'label' => 'Permintaan Barang',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'supplier_barang_id',
        'label' => 'Supplier Barang ID',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nama_barang',
        'label' => 'Nama Barang',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'quantity',
        'label' => 'Quantity',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'quantity_unit',
        'label' => 'Quantity Unit',
        'rules' => 'required|trim'
      ],
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->permintaan_barang_id = $this->clean_number($this->input->post('permintaan_barang_id'));
      $this->supplier_barang_id = $this->clean_number($this->input->post('supplier_barang_id'));
      $this->kode_barang = $this->input->post('kode_barang');
      $this->nama_barang = $this->input->post('nama_barang');
      $this->quantity = $this->clean_number($this->input->post('quantity'));
      $this->quantity_unit = $this->input->post('quantity_unit');
      $this->unit_price = $this->clean_number($this->input->post('unit_price'));
      $this->amount = $this->clean_number($this->input->post('amount'));
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->permintaan_barang_id = $this->clean_number($this->input->post('permintaan_barang_id'));
      $this->supplier_barang_id = $this->clean_number($this->input->post('supplier_barang_id'));
      $this->kode_barang = $this->input->post('kode_barang');
      $this->nama_barang = $this->input->post('nama_barang');
      $this->quantity = $this->clean_number($this->input->post('quantity'));
      $this->quantity_unit = $this->input->post('quantity_unit');
      $this->unit_price = $this->clean_number($this->input->post('unit_price'));
      $this->amount = $this->clean_number($this->input->post('amount'));
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function updatePenawaran($permintaanBarangId, $id, $payload)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      // Permintaan Barang
      $this->db->update('permintaan_barang', array(
        'tanggal_penawaran' => date('Y-m-d H:i:s')
      ), array(
        'id' => $permintaanBarangId
      ));

      // Permintaan Barang Item
      $this->harga_beli = $this->clean_number($payload['harga_beli']);
      $this->harga_beli_total = $this->clean_number($payload['harga_beli_total']);
      $this->selisih = $this->clean_number($payload['selisih']);
      $this->selisih_persen = $this->clean_number($payload['selisih_persen']);
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
