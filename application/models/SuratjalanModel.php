<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SuratjalanModel extends CI_Model
{
  private $_table = 'surat_jalan';
  private $_tableView = '';
  private $_columns = array(
    'permintaan_barang_id',
    'penerima',
    'nomor',
    'tanggal',
    'status_pengiriman',
    'pengiriman_ke',
  ); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'permintaan_barang_id',
        'label' => 'Permintaan Barang ID',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'penerima',
        'label' => 'Penerima',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nomor',
        'label' => 'Nomor',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'pengiriman_ke',
        'label' => 'Pengiriman Ke',
        'rules' => 'required|trim'
      ],
    );
  }

  public function generateNomor()
  {
    $query = "SELECT LPAD(LEFT(nomor, 4) + 1, 4, '0') as auto_nomor FROM $this->_table ORDER BY LEFT(nomor, 4) DESC LIMIT 1";
    $result = $this->db->query($query)->row();

    if (!is_null($result)) {
      $nomor = $result->auto_nomor;
    } else {
      $nomor = '0001';
    };

    $roman_month = array(1 => 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII');
    $month = $roman_month[date('n')];
    $year = date('y');
    $result = $nomor . '/TRD-DKM/' . $month . '/' . $year;

    return $result;
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function getHeadInfo($permintaanBarangId = null)
  {
    $query = "
      SELECT
        t.id AS permintaan_barang_id,
        t.dari,
        t.penerima,
        t.tanggal AS tanggal_permintaan,
        s.nomor,
        IFNULL(s.pengiriman_ke + 1, 1) AS next_pengiriman_ke
      FROM permintaan_barang t
      LEFT JOIN surat_jalan s ON s.permintaan_barang_id = t.id
      WHERE t.id = '$permintaanBarangId'
      ORDER BY s.id DESC
      LIMIT 1
    ";
    return $this->db->query($query)->row();
  }

  public function getTotalInfo($permintaanBarangId = null)
  {
    $query = "
      SELECT
        x.*,
        (x.total_permintaan - IFNULL(x.total_dikirim, 0)) AS selisih
      FROM (
        SELECT
          t.id AS permintaan_barang_id,
          (
            SELECT SUM(s1.quantity)
            FROM permintaan_barang_item s1
            WHERE s1.permintaan_barang_id = t.id
          ) AS total_permintaan,
          (
            SELECT SUM(s1.quantity)
            FROM surat_jalan_item s1
            JOIN surat_jalan s2 ON s2.id = s1.surat_jalan_id
            WHERE s2.permintaan_barang_id = t.id
          ) AS total_dikirim
        FROM permintaan_barang t
        WHERE t.id = '$permintaanBarangId'
      ) x
    ";
    return $this->db->query($query)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->permintaan_barang_id = $this->input->post('permintaan_barang_id');
      $this->penerima = $this->input->post('penerima');
      $this->nomor = $this->input->post('nomor');
      $this->tanggal = $this->input->post('tanggal');
      $this->status_pengiriman = 0;
      $this->pengiriman_ke = $this->input->post('pengiriman_ke');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $this->temp_id = $this->db->insert_id();

      $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'data_id' => null);
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->permintaan_barang_id = $this->input->post('permintaan_barang_id');
      $this->penerima = $this->input->post('penerima');
      $this->nomor = $this->input->post('nomor');
      $this->tanggal = $this->input->post('tanggal');
      $this->status_pengiriman = $this->input->post('status_pengiriman');
      $this->pengiriman_ke = $this->input->post('pengiriman_ke');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete('surat_jalan_item', ['surat_jalan_id' => $id]);
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function set_status($id, $status)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      if ((int) $status == 1) {
        $this->pic_sender = $this->session->userdata('user')['nama_lengkap'];
      };

      $this->status_pengiriman = $status;
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Status has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to update status.');
    };

    return $response;
  }

  public function set_statusPembayaran($id, $status)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->status_pembayaran = $status;
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Status has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to update status.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }
}
