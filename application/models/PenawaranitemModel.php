<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PenawaranitemModel extends CI_Model
{
  private $_table = 'penawaran_item';
  private $_tableView = '';
  private $_columns = array(
    'nomor', 'uraian', 'volume', 'satuan', 'unit_price', 'total'
  ); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'penawaran_id',
        'label' => 'Penawaran ID',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'uraian',
        'label' => 'Uraian',
        'rules' => 'required|trim'
      ],
    );
  }

  public function rules_header()
  {
    return array(
      [
        'field' => 'penawaran_id',
        'label' => 'Penawaran ID',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nomor',
        'label' => 'Nomor',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'uraian',
        'label' => 'Uraian Pekerjaan',
        'rules' => 'required|trim'
      ],
    );
  }

  public function rules_item()
  {
    return array(
      [
        'field' => 'penawaran_id',
        'label' => 'Penawaran ID',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'penawaran_item_parent_id',
        'label' => 'Penawaran Item Parent ID',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'uraian',
        'label' => 'Uraian Pekerjaan',
        'rules' => 'required|trim'
      ],
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getAll_sorted($penawaranId = null, $parentId = null)
  {
    $items = $this->db
      ->where(array(
        'penawaran_id' => $penawaranId,
        'penawaran_item_parent_id' => $parentId
      ))
      // ->order_by('CAST(nomor AS UNSIGNED)', 'asc')
      ->get($this->_table)
      ->result();

    $result = array();

    if (count($items) > 0) {
      foreach ($items as $index => $item) {
        $child = $this->getAll_sorted($penawaranId, $item->id);
        $result[] = $item;

        if (count($child) > 0) {
          $result[] = $child;
        };
      };
    };

    $result = $this->nestedToSingle($result);
    return $result;
  }

  public function nestedToSingle(array $array)
  {
    $singleDimArray = [];

    foreach ($array as $item) {

      if (is_array($item)) {
        $singleDimArray = array_merge($singleDimArray, $this->nestedToSingle($item));
      } else {
        $singleDimArray[] = $item;
      }
    }

    return $singleDimArray;
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $parentId = $this->input->post('penawaran_item_parent_id');
      $volume = $this->input->post('volume');
      $unitPrice = $this->input->post('unit_price');
      $total = $this->input->post('total');

      $this->penawaran_id = $this->input->post('penawaran_id');
      $this->penawaran_item_parent_id = (!is_null($parentId) && !empty($parentId)) ? $this->clean_number($parentId) : null;
      $this->nomor = $this->input->post('nomor');
      $this->uraian = $this->input->post('uraian');
      $this->volume = (!is_null($volume) && !empty($volume)) ? $this->clean_number($volume) : null;
      $this->satuan = $this->input->post('satuan');
      $this->unit_price = (!is_null($unitPrice) && !empty($unitPrice)) ? $this->clean_number($unitPrice) : null;
      $this->total = (!is_null($total) && !empty($total)) ? $this->clean_number($total) : null;
      $this->is_header = $this->input->post('is_header');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $this->temp_id = $this->db->insert_id();

      $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'data_id' => null);
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $parentId = $this->input->post('penawaran_item_parent_id');
      $volume = $this->input->post('volume');
      $unitPrice = $this->input->post('unit_price');
      $total = $this->input->post('total');

      $this->penawaran_id = $this->input->post('penawaran_id');
      $this->penawaran_item_parent_id = (!is_null($parentId) && !empty($parentId)) ? $this->clean_number($parentId) : null;
      $this->nomor = $this->input->post('nomor');
      $this->uraian = $this->input->post('uraian');
      $this->volume = (!is_null($volume) && !empty($volume)) ? $this->clean_number($volume) : null;
      $this->satuan = $this->input->post('satuan');
      $this->unit_price = (!is_null($unitPrice) && !empty($unitPrice)) ? $this->clean_number($unitPrice) : null;
      $this->total = (!is_null($total) && !empty($total)) ? $this->clean_number($total) : null;
      $this->is_header = $this->input->post('is_header');
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setBold($id, $isBold = 1)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->is_bold = $isBold;
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function deleteByPenawaran($penawaranId)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['penawaran_id' => $penawaranId]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9.]/', '', $number);
  }
}
