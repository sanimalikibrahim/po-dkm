<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model
{
  private $_table = 'user';
  private $_tableView = '';
  private $_columns = array(
    'nik',
    'username',
    'email',
    'nama_lengkap',
    'jabatan',
    'cabang',
    'departemen'
  ); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)
  private $_permittedChars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+';
  
	public function getColumnName($columnIndex) {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
		return $result;
  }

  public function rules() {
    return array(
      [
        'field' => 'role',
        'label' => 'Role',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nik',
        'label' => 'NIK',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'username',
        'label' => 'Username',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'email',
        'label' => 'Email',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nama_lengkap',
        'label' => 'Nama Lengkap',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'jabatan',
        'label' => 'Jabatan',
        'rules' => 'trim'
      ],
      [
        'field' => 'abang',
        'label' => 'Cabang',
        'rules' => 'trim'
      ],
      [
        'field' => 'departemen',
        'label' => 'Departemen',
        'rules' => 'trim'
      ]
    );
  }

  public function getAll($params = []) {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = []) {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function getBackup($params = []) {
    $temp = $this->getAll($params);
    $data = [];

    foreach ($temp as $key => $item) {
      $data[] = array(
        'nik' => $item->nik,
        'username' => $item->username,
        'password' => $item->password,
        'email' => $item->email,
        'nama_lengkap' => $item->nama_lengkap,
        'jabatan' => $item->jabatan,
        'cabang' => $item->cabang,
        'departemen' => $item->departemen,
        'role' => $item->role,
        'created_by' => $item->created_by,
        'created_date' => $item->created_date,
        'updated_by' => $item->updated_by,
        'updated_date' => $item->updated_date
      );
    };

    return $data;
  }

  public function insert() {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $post = $this->input->post();
      $post['password'] = (isset($post['password']) && !empty($post['password'])) ? md5($post['password']) : null;
  
      $this->nik = $post['nik'];
      $this->username = $post['username'];
      $this->password = $post['password'];
      $this->email = $post['email'];
      $this->nama_lengkap = $post['nama_lengkap'];
      $this->jabatan = $post['jabatan'];
      $this->cabang = $post['cabang'];
      $this->departemen = $post['departemen'];
      $this->role = $post['role'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data) {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id) {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $temp = $this->getDetail(['id' => $id]);
      $post = $this->input->post();
      $post['password'] = (isset($post['password']) && !empty($post['password'])) ? md5($post['password']) : $temp->password;
  
      $this->nik = $post['nik'];
      $this->username = $post['username'];
      $this->password = $post['password'];
      $this->email = $post['email'];
      $this->nama_lengkap = $post['nama_lengkap'];
      $this->jabatan = $post['jabatan'];
      $this->cabang = $post['cabang'];
      $this->departemen = $post['departemen'];
      $this->role = $post['role'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setPassword($nik, $data) {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->where(['nik' => $nik]);
      $this->db->set('password', $data);
      $this->db->update($this->_table);

      $response = array('status' => true, 'data' => 'Password has been changed.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to change password.');
    };

    return $response;
  }

  public function generateToken($params = []) {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $token = $this->generateRandom($this->_permittedChars, 255);

      $this->db->where($params);
      $this->db->set('token', $token);
      $this->db->update($this->_table);

      $response = array('status' => true, 'data' => 'Token has been generated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to generate token.');
    };

    return $response;
  }

  public function generatePassword($params = []) {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->where($params);
      $this->db->set('password', 'md5(concat(id, nik))', false);
      $this->db->update($this->_table);

      $response = array('status' => true, 'data' => 'Password has been generated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to generate password.');
    };

    return $response;
  }

  public function delete($id) {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate() {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function generateRandom($input, $strength = 16) {
    $input_length = strlen($input);
    $random_string = '';

    for($i = 0; $i < $strength; $i++) {
      $random_character = $input[mt_rand(0, $input_length - 1)];
      $random_string .= $random_character;
    };
 
    return $random_string;
  }
}
