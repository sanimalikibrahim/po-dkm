<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PermintaanbarangModel extends CI_Model
{
  private $_table = 'permintaan_barang';
  private $_tableView = 'view_permintaan_barang';
  private $_columns = array(
    'dari',
    'tanggal',
  ); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'dari',
        'label' => 'Dari',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'penerima',
        'label' => 'Penerima',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ],
    );
  }

  public function generateNomor()
  {
    $query = "SELECT LPAD(LEFT(nomor, 4) + 1, 4, '0') as auto_nomor FROM $this->_table ORDER BY LEFT(nomor, 4) DESC LIMIT 1";
    $result = $this->db->query($query)->row();

    if (count($result) > 0) {
      $nomor = $result->auto_nomor;
    } else {
      $nomor = '0001';
    };

    $roman_month = array(1 => 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII');
    $month = $roman_month[date('n')];
    $year = date('y');
    $result = $nomor . '-PO/TRD-DKM/' . $month . '/' . $year;

    return $result;
  }

  public function checkTtd($id, $status, $role)
  {
    $query = "SELECT t.*
              FROM view_permintaan_barang_history t
              JOIN permintaan_barang p ON p.id = t.permintaan_barang_id and p.revisi = t.revisi
              WHERE t.permintaan_barang_id = '" . $id . "' AND t.action = '" . $status . "' AND t.role = '" . $role . "'
              ORDER BY t.id DESC
              LIMIT 1";
    $result = $this->db->query($query)->row();

    if (count($result) > 0) {
      return $result->nama_lengkap;
    } else {
      return null;
    };
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->dari = $this->input->post('dari');
      $this->penerima = $this->input->post('penerima');
      $this->tanggal = $this->input->post('tanggal');
      $this->status = 0;
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $this->temp_id = $this->db->insert_id();

      $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'data_id' => null);
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->dari = $this->input->post('dari');
      $this->penerima = $this->input->post('penerima');
      $this->tanggal = $this->input->post('tanggal');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setAsPenawaran($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->tanggal_penawaran = date('Y-m-d H:i:s');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete('permintaan_barang_item', ['permintaan_barang_id' => $id]);
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function set_status($id, $status)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $temp = $this->getDetail(['id' => $id]);
    $revisi = ($status === 1) ? (int) $temp->revisi + 1 : (int) $temp->revisi;

    try {
      $this->status = $status;
      $this->revisi = $revisi;
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Status has been saved.', 'revisi' => $revisi);
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to update status.', 'revisi' => $revisi);
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }
}
