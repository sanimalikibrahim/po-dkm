<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PenawaranModel extends CI_Model
{
  private $_table = 'penawaran';
  private $_tableView = 'view_penawaran';
  private $_columns = array(); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'department',
        'label' => 'Department',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nomor',
        'label' => 'Nomor',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'city',
        'label' => 'City',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'project_name',
        'label' => 'Project Name',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'customer',
        'label' => 'Customer Name',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'customer_attn',
        'label' => 'Customer Attn',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'customer_address1',
        'label' => 'Customer Address 1',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'approval_role',
        'label' => 'Approval Role',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'approval_user_id',
        'label' => 'Approval PIC',
        'rules' => 'required|trim'
      ],
    );
  }

  public function generateNomor()
  {
    $query = "SELECT LPAD(LEFT(nomor, 4) + 1, 4, '0') as auto_nomor FROM " . $this->_table . " ORDER BY LEFT(nomor, 4) DESC LIMIT 1";
    $result = $this->db->query($query)->row();

    if (count($result) > 0) {
      $nomor = $result->auto_nomor;
    } else {
      $nomor = '0001';
    };

    $roman_month = array(1 => 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII');
    $month = $roman_month[date('n')];
    $year = date('Y');
    $result = $nomor . '/PEN/HO/PD-PT.DKM/' . $month . '/' . $year;

    return $result;
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function getApprovalPic($role = null)
  {
    $user = $this->db->where(array('role' => $role))->order_by('nama_lengkap', 'asc')->get('user')->result();
    $result = [];

    if (count($user) > 0) {
      foreach ($user as $index => $item) {
        $result[] = array(
          'id' => $item->id,
          'nama_lengkap' => $item->nama_lengkap,
          'departemen' => $item->departemen,
          'cabang' => $item->cabang,
          'jabatan' => $item->jabatan
        );
      };
    };

    return $result;
  }

  public function getWaitApprovalCount()
  {
    $query = "SELECT *
              FROM (
                (SELECT COUNT(id) AS menunggu_persetujuan FROM $this->_table WHERE status = '1' AND is_archive = '0') AS menunggu_persetujuan,
                (SELECT COUNT(id) AS disetujui FROM $this->_table WHERE status = '2' AND is_archive = '0') AS disetujui,
                (SELECT COUNT(id) AS ditolak FROM $this->_table WHERE status = '3' AND is_archive = '0') AS ditolak,
                (SELECT COUNT(id) AS selesai FROM $this->_table WHERE status = '4' AND is_archive = '0') AS selesai,
                (SELECT COUNT(id) AS dibatalkan FROM $this->_table WHERE status = '5' AND is_archive = '0') AS dibatalkan
              )";
    return $this->db->query($query)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $nomor = $this->input->post('nomor');
      $validateNomor = $this->getDetail(['nomor' => $nomor]);

      if (count($validateNomor) > 0) {
        $response = array('status' => false, 'data' => 'Nomor already exist.', 'data_id' => null);
      } else {
        $this->tanggal = $this->input->post('tanggal');
        $this->nomor = $nomor;
        $this->city = $this->input->post('city');
        $this->department = $this->input->post('department');
        $this->customer = $this->input->post('customer');
        $this->customer_address1 = $this->input->post('customer_address1');
        $this->customer_address2 = $this->input->post('customer_address2');
        $this->customer_attn = $this->input->post('customer_attn');
        $this->project_name = $this->input->post('project_name');
        $this->note = $this->input->post('note');
        $this->approval_role = $this->input->post('approval_role');
        $this->approval_user_id = $this->input->post('approval_user_id');
        $this->approval_name = $this->input->post('approval_name');
        $this->revisi_parent_id = $this->input->post('revisi_parent_id');
        $this->revisi_nomor = $this->input->post('revisi_nomor');
        $this->revisi_date = $this->input->post('revisi_date');
        $this->created_by = $this->session->userdata('user')['id'];
        $this->db->insert($this->_table, $this);

        $this->temp_id = $this->db->insert_id();

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'data_id' => null);
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $temp = $this->getDetail(['id' => $id]);
    $existNomor = false;

    try {
      $nomor = $this->input->post('nomor');

      if ($nomor != $temp->nomor) {
        $validateNomor = $this->getDetail(['nomor' => $nomor]);

        if (count($validateNomor) > 0) {
          $existNomor = true;
        };
      };

      if ($existNomor === true) {
        $response = array('status' => false, 'data' => 'Nomor already exist.');
      } else {
        $this->tanggal = $this->input->post('tanggal');
        $this->nomor = $nomor;
        $this->city = $this->input->post('city');
        $this->department = $this->input->post('department');
        $this->customer = $this->input->post('customer');
        $this->customer_address1 = $this->input->post('customer_address1');
        $this->customer_address2 = $this->input->post('customer_address2');
        $this->customer_attn = $this->input->post('customer_attn');
        $this->project_name = $this->input->post('project_name');
        $this->note = $this->input->post('note');
        $this->approval_role = $this->input->post('approval_role');
        $this->approval_user_id = $this->input->post('approval_user_id');
        $this->approval_name = $this->input->post('approval_name');
        $this->updated_at = date('Y-m-d H:i:s');
        $this->updated_by = $this->session->userdata('user')['id'];
        $this->db->update($this->_table, $this, ['id' => $id]);

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function revisi($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $existNomor = false;

    try {
      $nomor = $this->input->post('nomor');
      $validateNomor = $this->getDetail(['nomor' => $nomor]);
      $existNomor = (count($validateNomor) > 0) ? true : false;

      if ($existNomor === true) {
        $response = array('status' => false, 'data' => 'Nomor already exist.');
      } else {
        // Create archive
        $oldPenawaran = $this->getDetail(array('id' => $id));
        $newPenawaran = array();

        if (!is_null($oldPenawaran)) {
          foreach ($oldPenawaran as $index => $item) {
            $newPenawaran[$index] = $item;
            $newPenawaran['revisi_parent_id'] = $id;
            $newPenawaran['revisi_nomor'] = (int) $this->input->post('revisi_nomor') - 1;
            $newPenawaran['revisi_date'] = date('Y-m-d');
            $newPenawaran['is_archive'] = 1;

            unset($newPenawaran['id']);
            unset($newPenawaran['total']);
            unset($newPenawaran['construction_fee']);
            unset($newPenawaran['grand_total']);
          };

          $this->db->insert('penawaran_arsip', $newPenawaran);
          $penawaranArsip_id = $this->db->insert_id();

          // Insert items
          $oldPenawaranItems = $this->db->where(array('penawaran_id' => $id))->get('penawaran_item')->result();
          $newPenawaranItems = array();

          if (count($oldPenawaranItems) > 0) {
            foreach ($oldPenawaranItems as $index => $item) {
              $newPenawaranItems[$index] = $item;
              $newPenawaranItems[$index]->penawaran_id = $penawaranArsip_id;
            };

            if (count($newPenawaranItems) > 0) {
              $this->db->insert_batch('penawaran_arsip_item', $newPenawaranItems);
            };
          };
        };
        // END ## Create archive

        // Update for revisi
        $this->tanggal = $this->input->post('tanggal');
        $this->nomor = $nomor;
        $this->city = $this->input->post('city');
        $this->department = $this->input->post('department');
        $this->customer = $this->input->post('customer');
        $this->customer_address1 = $this->input->post('customer_address1');
        $this->customer_address2 = $this->input->post('customer_address2');
        $this->customer_attn = $this->input->post('customer_attn');
        $this->project_name = $this->input->post('project_name');
        $this->note = $this->input->post('note');
        $this->approval_role = $this->input->post('approval_role');
        $this->approval_user_id = $this->input->post('approval_user_id');
        $this->approval_name = $this->input->post('approval_name');
        $this->approval_note = null;
        $this->approval_date = null;
        $this->revisi_parent_id = $this->input->post('revisi_parent_id');
        $this->revisi_nomor = $this->input->post('revisi_nomor');
        $this->revisi_date = date('Y-m-d');
        $this->status = 0;
        $this->is_archive = 0;
        $this->created_by = $this->session->userdata('user')['id'];
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = null;
        $this->updated_by = null;
        $this->db->update($this->_table, $this, ['id' => $id]);
        // END ## Update for revisi

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setStatus($id = null, $status = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->update($this->_table, array('status' => $status), ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setApproval($id = null, $status = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->update($this->_table, array(
        'status' => $status,
        'approval_note' => $this->input->post('note'),
        'approval_date' => date('Y-m-d')
      ), ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function uploadPenyelesaian()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $id = $this->input->post('penawaran_id');

      $this->db->update($this->_table, array(
        'status' => 4,
        'berkas_path' => $this->input->post('berkas_path'),
        'berkas_name' => $this->input->post('berkas_name'),
      ), ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function cancel($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->update($this->_table, array(
        'status' => 5,
        'cancel_by' => $this->session->userdata('user')['nama_lengkap'],
        'cancel_at' => date('Y-m-d H:i:s'),
        'cancel_note' => $this->input->post('cancel_note')
      ), ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been cancelled.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete('penawaran_item', ['penawaran_id' => $id]);
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);
      $this->db->truncate('penawaran_item');

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }
}
