<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SearchModel extends CI_Model
{
  public function packingOrder($keyword)
  {
    $role = strtolower($this->session->userdata('user')['role']);

    $this->db->like('nomor', $keyword, 'both');

    if ($role != 'administrasi') {
      $this->db->where('status !=', 0);
    };

    $this->db->order_by('id', 'desc');
    $result = $this->db->get('view_packing_order')->result();

    return $result;
  }
}
