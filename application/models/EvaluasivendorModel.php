<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EvaluasivendorModel extends CI_Model
{
  private $_table = 'evaluasi_vendor';
  private $_tableView = 'view_evaluasi_vendor';

  public function rules()
  {
    return array(
      [
        'field' => 'supplier_id',
        'label' => 'Supplier',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'harga',
        'label' => 'Harga / Biaya',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'kualitas_barang',
        'label' => 'Kualitas Barang / Pekerjaan',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'waktu_pengiriman',
        'label' => 'Tepat Waktu Pengiriman / Pelaksanaan Pekerjaan',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'cara_pembayaran',
        'label' => 'Cara Pembayaran',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $post = $this->input->post();

      $this->supplier_id = $post['supplier_id'];
      $this->harga = $this->clean_number($post['harga']);
      $this->kualitas_barang = $this->br2nl($post['kualitas_barang']);
      $this->waktu_pengiriman = $this->br2nl($post['waktu_pengiriman']);
      $this->cara_pembayaran = $this->br2nl($post['cara_pembayaran']);
      $this->lain_lain = $this->br2nl($post['lain_lain']);
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $temp = $this->getDetail(['id' => $id]);
      $post = $this->input->post();

      $this->supplier_id = $post['supplier_id'];
      $this->harga = $this->clean_number($post['harga']);
      $this->kualitas_barang = $this->br2nl($post['kualitas_barang']);
      $this->waktu_pengiriman = $this->br2nl($post['waktu_pengiriman']);
      $this->cara_pembayaran = $this->br2nl($post['cara_pembayaran']);
      $this->lain_lain = $this->br2nl($post['lain_lain']);
      $this->created_by = $this->session->userdata('user')['id'];
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
