<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PackingorderhistoryModel extends CI_Model
{
  private $_table = 'packing_order_history';
  private $_tableView = 'view_packing_order_history';
  private $_columns = array(
    'packing_order_id',
    'user_pic',
    'action',
    'description'
  ); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'packing_order_id',
        'label' => 'Packing Order',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'user_pic',
        'label' => 'User Pic',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'action',
        'label' => 'Action',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'description',
        'label' => 'Description',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert($post)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->packing_order_id = $post['packing_order_id'];
      $this->user_pic = $post['user_pic'];
      $this->action = $post['action'];
      $this->description = $post['description'];
      $this->revisi = $post['revisi'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id, $post)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->packing_order_id = $post['packing_order_id'];
      $this->user_pic = $post['user_pic'];
      $this->action = $post['action'];
      $this->description = $post['description'];
      $this->revisi = $post['revisi'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }
}
