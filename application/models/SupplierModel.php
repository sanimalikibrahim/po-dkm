<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SupplierModel extends CI_Model
{
  private $_table = 'supplier';
  private $_tableView = '';
  private $_columns = array(
    'nama',
    'alamat',
    'nama_kontak',
    'telepon',
    'fax'
  ); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'nama',
        'label' => 'Nama',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'alamat',
        'label' => 'Alamat',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nama_kontak',
        'label' => 'Nama Kontak',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'telepon',
        'label' => 'Telepon',
        'rules' => 'trim'
      ],
      [
        'field' => 'fax',
        'label' => 'Fax',
        'rules' => 'trim'
      ]
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $post = $this->input->post();

      $this->nama = $post['nama'];
      $this->alamat = $this->br2nl($post['alamat']);
      $this->nama_kontak = $post['nama_kontak'];
      $this->telepon = $post['telepon'];
      $this->fax = $post['fax'];
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      // Set kode
      $this->temp_id = $this->db->insert_id();
      $set_kode = $this->updateKode($this->temp_id);

      if ($set_kode['status'] === true) {
        $response = array('status' => true, 'data' => 'Data has been saved.');
      } else {
        $response = $set_kode;
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function updateKode($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $kode = date('Y') . '.' . $id;
      $this->db->update($this->_table, ['kode' => $kode], ['id' => $id]);

      $response = array('status' => true, 'data' => 'Kode has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to update kode.');
    };

    return $response;
  }

  public function generateKode()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->set('kode', "CONCAT(YEAR(NOW()), '.', id)", false);
      $this->db->where('kode IS NULL', null, false);
      $this->db->update($this->_table);

      $response = array('status' => true, 'data' => 'Kode has been generated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to generate kode.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $temp = $this->getDetail(['id' => $id]);
      $post = $this->input->post();

      $this->nama = $post['nama'];
      $this->alamat = $this->br2nl($post['alamat']);
      $this->nama_kontak = $post['nama_kontak'];
      $this->telepon = $post['telepon'];
      $this->fax = $post['fax'];
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete('supplier_barang', ['supplier_id' => $id]);
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }
}
