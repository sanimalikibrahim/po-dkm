<html>

<head>
  <style type="text/css">
    /* Preview */
    @media(print) {

      table {
        border-collapse: collapse;
      }

      .float-right {
        float: right;
      }

      .preview-po {
        background: #fff;
      }

      .logo {
        height: 80px;
      }

      .table-header {
        width: 100%;
      }

      .kop-surat-h5 {
        font-size: 14px;
        margin-bottom: .3rem;
      }

      .kop-surat-span {
        font-size: 11px;
        color: #333;
      }

      .double-line {
        color: #8c8b8b;
        border-top: 3px double #8c8b8b;
        margin-top: 20px;
        margin-bottom: 30px;
        height: 2px;
      }

      .table-body {
        border: 1px solid #8c8b8b;
        font-size: 11px;
      }

      .table-body .th {
        background: #f9f9f9;
        padding: 4px 8px;
        border-bottom: 1px solid #8c8b8b;
      }

      .table-body .td {
        background: #fff;
        padding: 4px 8px;
        border-bottom: 1px solid #8c8b8b;
      }

      .table-body-right {
        font-size: 11px;
      }

      .table-body-right .th {
        padding: 4px 8px;
      }

      .table-body-right .td {
        padding: 4px 8px;
      }

      .table-order-item {
        width: 100%;
        margin-bottom: 20px;
        font-size: 11px;
        margin: 0px;
      }

      .table-order-item th {
        border: 1px solid #8c8b8b;
        text-align: center;
        padding: 4px 8px;
        margin: 0px;
      }

      .table-order-item td {
        border: 1px solid #8c8b8b;
        padding: 4px 8px;
        margin: 0px;
      }

      .table-order-item .no-border {
        border: 0;
      }

      .table-order-item .ttd {
        height: 60px;
        width: 14.285%;
        vertical-align: middle;
      }

      .table-information {
        font-size: 10px;
      }

      .table-information td {
        padding: 0px 10px 0px 0px;
      }

      .clear {
        height: 20px;
      }

      .clear-sm {
        height: 10px;
      }
    }

    /* END ## Preview */
  </style>
</head>

<body>
  <div class="preview-po">
    <div class="preview-header">
      <table class="table-header">
        <tr>
          <td style="width: 80px;">
            <img src="%img_logo_1%" class="logo" />
          </td>
          <td style="padding-left: 20px; padding-right: 20px;">
            <div class="kop-surat">
              <h5 class="kop-surat-h5">PT. DHARMA KARYATAMA MULIA</h5>
              <span class="kop-surat-span">Jl. Raya Bogor Km 29, Gandaria, Ps. Rebo - Jakarta 13710, Indonesia</span> <br />
              <span class="kop-surat-span">Phone : +62 21 8719964-5, Fax : +62 21 8727018</span> <br />
              <span class="kop-surat-span">Email : pt_dkm@dkm.co.id, Web : www.dkm.co.id</span>
            </div>
          </td>
          <td style="width: 80px; text-align: right;">
            <img src="%img_logo_2%" class="logo" />
          </td>
        </tr>
      </table>
      <hr class="double-line" />
    </div>
    <div class="preview-body">
      <table style="width: 100%; margin-bottom: 10px;">
        <tr>
          <td>&nbsp;</td>
          <td style="border: 1px solid #8c8b8b; width: 250px; padding: 4px 8px; text-align: left; font-size: 11px;">
            FM-PCH-01-01 <br>
            Revisi-1-
          </td>
        </tr>
      </table>
      <div style="text-align: center; margin-bottom: 10px;">
        <h5>PURCHASE ORDER</h5>
      </div>
      <table style="width: 100%; margin-bottom: 20px;">
        <tr>
          <td valign="top">
            <table class="table-body" style="overflow: wrap;">
              <tr>
                <td valign="top" class="th">Nama Supplier</td>
                <td valign="top" align="center" class="td">%supplier_nama%</td>
              </tr>
              <tr>
                <td valign="top" class="th">Alamat Supplier</td>
                <td valign="top" class="td">%supplier_alamat%</td>
              </tr>
              <tr>
                <td valign="top" class="th">
                  Contact Person <br />
                  Telpon No. / Fax
                </td>
                <td valign="top" class="td">
                  %supplier_nama_kontak% <br />
                  %supplier_telepon_fax%
                </td>
              </tr>
            </table>
          </td>
          <td valign="top">
            <table class="table-body-right">
              <tr>
                <td valign="top" class="th">Tanggal</td>
                <td valign="top" class="td">: %tanggal%</td>
              </tr>
              <tr>
                <td valign="top" class="th">PO No.</td>
                <td valign="top" class="td">: %nomor%</td>
              </tr>
              <tr>
                <td valign="top" class="th">Term Of Delivery</td>
                <td valign="top" class="td">: %term_of_delivery%</td>
              </tr>
              <tr>
                <td valign="top" class="th">Term Of Payment</td>
                <td valign="top" class="td">
                  : %term_of_payment% <br />
                  &nbsp; Setelah invoice diterima.
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <table class="table-order-item">
        <thead>
          <tr>
            <th width="50">NO.</th>
            <th>DESCRIPTION OF GOOD</th>
            <th>QTY</th>
            <th>UNIT PRICE</th>
            <th>AMOUNT</th>
          </tr>
        </thead>
        <tbody>
          %order_item_list%
        </tbody>
        <tfoot>
          <tr>
            <th colspan="3" class="no-border">&nbsp;</th>
            <th valign="top" style="text-align: left;">TOTAL</th>
            <td>
              <table style="width: 100%;">
                <tr>
                  <td style="border: none; padding: 0; font-weight: bold;">Rp</td>
                  <td style="border: none; padding: 0; font-weight: bold; text-align: right;">%total%</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <th colspan="3" class="no-border">&nbsp;</th>
            <th valign="top" style="text-align: left;">SUB TOTAL</th>
            <td>
              <table style="width: 100%;">
                <tr>
                  <td style="border: none; padding: 0; font-weight: bold;">Rp</td>
                  <td style="border: none; padding: 0; font-weight: bold; text-align: right;">%sub_total%</td>
                </tr>
              </table>
            </td>
          </tr>
          %start_wrap_ppn%
          <tr>
            <th colspan="3" class="no-border">&nbsp;</th>
            <th valign="top" style="text-align: left;">PPN 11%</th>
            <td>
              <table style="width: 100%;">
                <tr>
                  <td style="border: none; padding: 0; font-weight: bold;">Rp</td>
                  <td style="border: none; padding: 0; font-weight: bold; text-align: right;">%ppn%</td>
                </tr>
              </table>
            </td>
          </tr>
          %end_wrap_ppn%
        </tfoot>
      </table>
      <div class="clear"></div>
      <table class="table-order-item">
        <thead>
          <tr>
            <th colspan="3">Diajukan</th>
            <th>Diperika</th>
            <th colspan="3">Disetujui</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td valign="center" align="center" class="ttd">%ttd_administrasi%</td>
            <td valign="center" align="center" class="ttd">%ttd_supervisor%</td>
            <td valign="center" align="center" class="ttd">%ttd_manager%</td>
            <td valign="center" align="center" class="ttd">%ttd_finance%</td>
            <td valign="center" align="center" class="ttd">%ttg_g_manager%</td>
            <td valign="center" align="center" class="ttd">%ttd_deputy_dir%</td>
            <td valign="center" align="center" class="ttd">%ttd_direktur%</td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <th align="center">Administrasi</th>
            <th align="center">Supervisor</th>
            <th align="center">Manager</th>
            <th align="center">Finance</th>
            <th align="center">G. Manager</th>
            <th align="center">Deputy. Dir</th>
            <th align="center">Direktur</th>
          </tr>
        </tfoot>
      </table>
      <p style="margin: 0; margin-top: 20px; font-size: 11px;">
        *PO ini tidak memerlukan tanda tangan langsung dikarenakan sudah dilakukan tanda tangan elektronik.
      </p>
    </div>
  </div>
</body>

</html>