<html>
  <head>
    <style type="text/css">
      * {
        font-family:Arial, Helvetica, sans-serif;
      }
      body {
        padding: 15px;
        font-size: 14px;
      }
      table {
        font-size: 14px;
      }
      .logo {
        width: 60px;
      }
      .title {
        font-size: 17px;
        font-weight: bold;
      }
      .sub-title {
        font-size: 14px;
        font-weight: bold;
      }
      .item {
        margin-top: 30px;
        margin-bottom: 30px;
      }
      .item-data td {
        font-size: 12px;
        padding-bottom: 8px;
      }
      .text-bold {
        font-weight: bold;
      }
      .titik-dua {
        width: 30px;
        text-align: center;
      }
      .penerima {
        padding-top: 30px;
      }
      .take-homepay {
        width: 100%;
        margin-top: 20px;
        padding-top: 10px;
        padding-bottom: 10px;
        border-top: 1px dotted #999;
        border-bottom: 1px dotted #999;
      }
      .barcode {
          padding: 1.5mm;
          margin: 0;
          vertical-align: top;
          color: #000044;
      }
      .barcodecell {
          text-align: center;
          vertical-align: middle;
      }
    </style>
  </head>
  <body>
    <table>
      <tr>
        <td valign="center">
          <img class="logo" src="%img_logo%" />
        </td>
        <td valign="center" style="padding-top: 15px;">
          <div class="title">PT. Dharma Karyatama Mulia</div>
          <div class="sub-title">Pay Slip</div>
        </td>
      </tr>
    </table>

    <table class="item">
      <tr>
        <td align="right">Bulan</td>
        <td class="titik-dua">:</td>
        <td>%tahun_bulan%</td>
      </tr>
      <tr>
        <td align="right">Nama</td>
        <td class="titik-dua">:</td>
        <td><b>%nama_lengkap%</b></td>
      </tr>
      <tr>
        <td align="right">Departemen</td>
        <td class="titik-dua">:</td>
        <td>%departemen%</td>
      </tr>
      <tr>
        <td align="right">Lokasi</td>
        <td class="titik-dua">:</td>
        <td>%cabang%</td>
      </tr>
    </table>

    <table class="item-data" width="100%">
      <tr>
        <td width="50%" valign="top">
          <div class="pendapatan">
            <span class="sub-title">Penerimaan :</span>
            <table>
              %p1_gaji_pokok_divRow%
                <tr>
                  <td width="200">Gaji Pokok</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_gaji_pokok%</td>
                </tr>
              %p1_gaji_pokok_divRowEnd%
              %p1_tj_tetap_divRow%
              <tr>
                <td>Tunjangan Tetap</td>
                <td class="titik-dua">:</td>
                <td>%p1_tj_tetap%</td>
              </tr>
              %p1_tj_tetap_divRowEnd%
              %p1_tj_jabatan_divRow%
                <tr>
                  <td>Tunjangan Jabatan</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_tj_jabatan%</td>
                </tr>
              %p1_tj_jabatan_divRowEnd%
              %p1_overtime_divRow%
                <tr>
                  <td>Lembur</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_overtime%</td>
                </tr>
              %p1_overtime_divRowEnd%
              %p1_dinas_luar_kota_divRow%
                <tr>
                  <td>Dinas Luar Kota</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_dinas_luar_kota%</td>
                </tr>
              %p1_dinas_luar_kota_divRowEnd%
              %p1_u_makan_trans_divRow%
                <tr>
                  <td>Uang Makan & Transportasi</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_u_makan_trans%</td>
                </tr>
              %p1_u_makan_trans_divRowEnd%
              %p1_tj_hp_lainnya_divRow%
                <tr>
                  <td>Tunjangan HP & Lainnya</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_tj_hp_lainnya%</td>
                </tr>
              %p1_tj_hp_lainnya_divRowEnd%
              %p1_insentif_thr_pulsa_divRow%
                <tr>
                  <td>Insentif THR & Pulsa</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_insentif_thr_pulsa%</td>
                </tr>
              %p1_insentif_thr_pulsa_divRowEnd%
              %p1_tj_kehadiran_transport_divRow%
                <tr>
                  <td>Tunjangan Kehadiran / Transport</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_tj_kehadiran_transport%</td>
                </tr>
              %p1_tj_kehadiran_transport_divRowEnd%
              %p1_makan_divRow%
                <tr>
                  <td>Tunjangan Makan</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_makan%</td>
                </tr>
              %p1_makan_divRowEnd%
              %p1_late_meal_divRow%
                <tr>
                  <td>Late Meal</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_late_meal%</td>
                </tr>
              %p1_late_meal_divRowEnd%
              %p1_bpjs_p_divRow%
                <tr>
                  <td>BPJS Perusahaan</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_bpjs_p%</td>
                </tr>
              %p1_bpjs_p_divRowEnd%
              %p1_astek_p_divRow%
                <tr>
                  <td>Astek Perusahaan</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_astek_p%</td>
                </tr>
              %p1_astek_p_divRowEnd%
              %p1_iuran_pensiun_p_divRow%
                <tr>
                  <td>BPJS TK Pensiun Perusahaan</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_iuran_pensiun_p%</td>
                </tr>
              %p1_iuran_pensiun_p_divRowEnd%
              %p1_akdhk_p_divRow%
                <tr>
                  <td>AKDHK Perusahaan</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_akdhk_p%</td>
                </tr>
              %p1_akdhk_p_divRowEnd%
              %p1_meal_private_tj_dll_divRow%
                <tr>
                  <td>Meal Private & Tunjangan Lainnya</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_meal_private_tj_dll%</td>
                </tr>
              %p1_meal_private_tj_dll_divRowEnd%
              %p1_out_of_town_rapel_insentif_divRow%
                <tr>
                  <td>Out Of Town, Rapel & Insentif</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_out_of_town_rapel_insentif%</td>
                </tr>
              %p1_out_of_town_rapel_insentif_divRowEnd%
              %p1_tj_pph21_divRow%
                <tr>
                  <td>PPh 21</td>
                  <td class="titik-dua">:</td>
                  <td>%p1_tj_pph21%</td>
                </tr>
              %p1_tj_pph21_divRowEnd%
              <tr>
                <td class="text-bold">TOTAL</td>
                <td class="text-bold titik-dua">:</td>
                <td class="text-bold">%p1_total_bruto%</td>
              </tr>
            </table>
          </div>
        </td>
        <td width="50%" valign="top">
          <div class="potongan">
            <span class="sub-title">Potongan :</span>
            <table>
              %p2_astek_p_deduction_divRow%
                <tr>
                  <td width="200">Astek Perusahaan</td>
                  <td class="titik-dua">:</td>
                  <td>%p2_astek_p_deduction%</td>
                </tr>
              %p2_astek_p_deduction_divRowEnd%
              %p2_jht2_divRow%
                <tr>
                  <td>JHT 2</td>
                  <td class="titik-dua">:</td>
                  <td>%p2_jht2%</td>
                </tr>
              %p2_jht2_divRowEnd%
              %p2_iuran_pensiun_p_divRow%
                <tr>
                  <td>BPJS TK Pensiun Perusahaan</td>
                  <td class="titik-dua">:</td>
                  <td>%p2_iuran_pensiun_p%</td>
                </tr>
              %p2_iuran_pensiun_p_divRowEnd%
              %p2_iuran_pensiun_k_divRow%
                <tr>
                  <td>BPJS TK Pensiun Karyawan</td>
                  <td class="titik-dua">:</td>
                  <td>%p2_iuran_pensiun_k%</td>
                </tr>
              %p2_iuran_pensiun_k_divRowEnd%
              %p2_bpjs_p_divRow%
                <tr>
                  <td>BPJS Perusahaan</td>
                  <td class="titik-dua">:</td>
                  <td>%p2_bpjs_p%</td>
                </tr>
              %p2_bpjs_p_divRowEnd%
              %p2_bpjs_k_divRow%
                <tr>
                  <td>BPJS Karyawan</td>
                  <td class="titik-dua">:</td>
                  <td>%p2_bpjs_k%</td>
                </tr>
              %p2_bpjs_k_divRowEnd%
              %p2_akdhk_d_divRow%
                <tr>
                  <td>AKDHK Perusahaan</td>
                  <td class="titik-dua">:</td>
                  <td>%p2_akdhk_d%</td>
                </tr>
              %p2_akdhk_d_divRowEnd%
              %p2_pinjaman_koperasi_divRow%
                <tr>
                  <td>Pinjaman Koperasi</td>
                  <td class="titik-dua">:</td>
                  <td>%p2_pinjaman_koperasi%</td>
                </tr>
              %p2_pinjaman_koperasi_divRowEnd%
              %p2_pinjaman_perusahaan_divRow%
                <tr>
                  <td>Pinjaman Perusahaan</td>
                  <td class="titik-dua">:</td>
                  <td>%p2_pinjaman_perusahaan%</td>
                </tr>
              %p2_pinjaman_perusahaan_divRowEnd%
              %p2_simpanan_koperasi_divRow%
              <tr>
                <td>Simpanan Wajib Koperasi</td>
                <td class="titik-dua">:</td>
                <td>%p2_simpanan_koperasi%</td>
              </tr>
              %p2_simpanan_koperasi_divRowEnd%
              %p2_tj_pph21_divRow%
                <tr>
                  <td>PPh 21</td>
                  <td class="titik-dua">:</td>
                  <td>%p2_tj_pph21%</td>
                </tr>
              %p2_tj_pph21_divRowEnd%
              <tr>
                <td class="text-bold">TOTAL</td>
                <td class="text-bold titik-dua">:</td>
                <td class="text-bold">%p2_total_potongan%</td>
              </tr>
            </table>
          </div>
        </td>
      </tr>
    </table>

    <table class="take-homepay">
      <tr>
        <td class="text-bold" width="130" valign="top">TAKE HOME PAY</td>
        <td class="text-bold titik-dua" valign="top">:</td>
        <td class="text-bold">
          %take_home_pay%
          <div style="font-weight: normal; font-style: italic; font-size: 13px;">(%terbilang_take_home_pay%)</div>
        </td>
      </tr>
    </table>

    <table width="100%">
      <tr>
        <td class="penerima">
          Penerima
          <div class="ttd">
            <br/><br/><br/><br/>
          </div>
          %nama_lengkap%
        </td>
        <td align="right" valign="bottom">
          %barcode%
        </td>
      </tr>
    </table>
  </body>
</html>
